﻿Shader "Custom/OutlineAndShadow"
{
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_ColorOutline("OutlineColor", Color) = (1,1,1,1)
		[PerRendererData]_MainTex("Sprite Texture", 2D) = "white" {}
		_Cutoff("Shadow alpha cutoff", Range(0,1)) = 0.5
		_OutlineSize("Outline", int) = 0
		_OuterGlow("Outer", int) = 1 //decide between inner or outer glow
	}
		SubShader{
			Tags
			{
				"Queue" = "Geometry"
				"RenderType" = "TransparentCutout"
			}
			LOD 200
			Cull Off
			Blend One OneMinusSrcAlpha

			Pass{
				CGPROGRAM
				#pragma vertex vertexFunc
				#pragma fragment fragmentFunc
				#include "UnityCG.cginc"

				sampler2D _MainTex;

				struct v2f
				{
					float4 pos : SV_POSITION; //var name, what we grabbing
					half2 uv : TEXCOORD0; //only 1 texture
				};

				v2f vertexFunc(appdata_base v) //2d specific variable, type of data
				{
					v2f output;
					output.pos = UnityObjectToClipPos(v.vertex); //give a position, convert from mesh space to camera space, put vertices correctly
					output.uv = v.texcoord;

					return output;
				}

				//write frament function to render on screen

				fixed4 _ColorOutline;
				int _OutlineSize;
				float4 _MainTex_TexelSize; //pixel size
				int _OuterGlow; //check whether inner or outer glow

				fixed4 fragmentFunc(v2f i) : COLOR //type color
				{
					half4 c = tex2D(_MainTex, i.uv); //grab the color of the pixel
					c.rgb *= c.a;
					half4 outlineC = _ColorOutline;

					outlineC.a *= ceil(c.a); //prevent floating point error, round up to get whole value
					outlineC.rgb *= outlineC.a; //outline 


					fixed upAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y) * _OutlineSize).a;  //get alpha of the pixel abv
					fixed downAlpha = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y) * _OutlineSize).a;
					fixed leftAlpha = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0) * _OutlineSize).a;
					fixed rightAlpha = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0) * _OutlineSize).a;

					if (c.a < 0.3 && _OuterGlow == 1) //if currently alpha is less than 0.3 , and want outer glow
					{
						if (upAlpha > 0.3 || downAlpha > 0.3 || leftAlpha > 0.3 || rightAlpha > 0.3) //check above it got color or not, if yes, draw outline
						{
							c.a = 1;
							c = _ColorOutline;
						}
					}

					if (c.a < 0.3)
						discard;

					if (_OuterGlow == 0) //if want inner glow
						return lerp(outlineC, c, ceil(upAlpha * downAlpha * rightAlpha * leftAlpha));

					return c;
				}
				ENDCG
			}


				CGPROGRAM
				// Lambert lighting model, and enable shadows on all light types
				#pragma surface surf Lambert addshadow fullforwardshadows

				// Use shader model 3.0 target, to get nicer looking lighting
				#pragma target 3.0

				sampler2D _MainTex;
				fixed4 _Color;
				fixed _Cutoff;

				struct Input
				{
					float2 uv_MainTex;
				};

				void surf(Input IN, inout SurfaceOutput o) {
					fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
					o.Albedo = c.rgb;
					o.Alpha = c.a;
					clip(o.Alpha - _Cutoff);
				}
				ENDCG
		}
		FallBack "Diffuse"
}