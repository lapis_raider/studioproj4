﻿Shader "Custom/CameraSanity"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_EffectAmount("Effect Amount", Range(0, 1)) = 1.0
		_BrightnessAmount("Brightness Amount", Range(0.0, 3)) = 1.0
	}

	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata //info we getting from mesh data
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0; 
			};

			struct v2f //info we passing to fragment shader
			{
				float2 uv : TEXCOORD0; //pass uv data to individual fucntion
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			fixed4 _Color;
			uniform float _EffectAmount;
			uniform float _BrightnessAmount;
			
			v2f vert (appdata v)
			{
				v2f o; //pos of vertex, init o and set vertex variables
				o.vertex = UnityObjectToClipPos(v.vertex); //take pt relative of obj to screen
				o.uv = v.uv; //pass vertex info
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target //turn pixels to colours, vertex locked the position already
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);

				float3 brtColor = col.rgb * _BrightnessAmount;
				col.rgb = lerp(brtColor, dot(brtColor, float3(0.3, 0.59, 0.11)), _EffectAmount); //for greyscale effect
				col *= _Color;

				return col;
			}
			ENDCG
		}
	}
}
