﻿Shader "Custom/Outline"
{
	Properties //what we need for shader
	{
		//main texture, treat sprite sheet as main texture
		_MainTex("Texture", 2D) = "white" {} //texture, 2D, default white
		_Color("Colour", Color) = (1,1,1,1) //color of outline
		_OutlineSize("Outline", int) = 0
		_OuterGlow("Outer", int) = 1 //decide between inner or outer glow
	}

		SubShader{
			Cull Off //not going to ignore any pixels, only 3D objs need cause backface
			Blend One OneMinusSrcAlpha

			Pass{
				CGPROGRAM
				#pragma vertex vertexFunc
				#pragma fragment fragmentFunc

				#include "UnityCG.cginc"

				sampler2D _MainTex;

			struct v2f
			{
				float4 pos : SV_POSITION; //var name, what we grabbing
				half2 uv : TEXCOORD0; //only 1 texture
			};

			v2f vertexFunc(appdata_base v) //2d specific variable, type of data
			{
				v2f output;
				output.pos = UnityObjectToClipPos(v.vertex); //give a position, convert from mesh space to camera space, put vertices correctly
				output.uv = v.texcoord;

				return output;
			}

			//write frament function to render on screen

			fixed4 _Color;
			int _OutlineSize;
			float4 _MainTex_TexelSize; //pixel size
			int _OuterGlow; //check whether inner or outer glow

			fixed4 fragmentFunc(v2f i) : COLOR //type color
			{
				half4 c = tex2D(_MainTex, i.uv); //grab the color of the pixel
				c.rgb *= c.a;
				half4 outlineC = _Color;

				outlineC.a *= ceil(c.a); //prevent floating point error, round up to get whole value
				outlineC.rgb *= outlineC.a; //outline 


				fixed upAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y) * _OutlineSize).a;  //get alpha of the pixel abv
				fixed downAlpha = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y) * _OutlineSize).a;
				fixed leftAlpha = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0) * _OutlineSize).a;
				fixed rightAlpha = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0) * _OutlineSize).a;

				if (c.a < 0.3 && _OuterGlow == 1) //if currently alpha is less than 0.3 , and want outer glow
				{
					if (upAlpha > 0.3 || downAlpha > 0.3 || leftAlpha > 0.3 || rightAlpha > 0.3) //check above it got color or not, if yes, draw outline
					{
						c.a = 1;
						c = _Color;
					}
				}

				if (c.a < 0.3)
					discard;

				if (_OuterGlow == 0) //if want inner glow
					return lerp(outlineC, c, ceil(upAlpha * downAlpha * rightAlpha * leftAlpha));

				return c;
			}


				ENDCG
			}
		}
}
