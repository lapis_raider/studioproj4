﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class Joystick : MonoBehaviour {

    public Image js;
    public Image smallJS;
    public RectTransform inputArea;

    public PlayerMovement player;

    Vector2 dir;

	// Update is called once per frame
	void Update () {

        player.Move(dir);

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                Vector3 touchPos = Input.GetTouch(i).position;
                if (RectTransformUtility.RectangleContainsScreenPoint(inputArea,  touchPos))
                {
                    smallJS.transform.localPosition = touchPos - js.rectTransform.position;
                    dir = smallJS.transform.position - js.transform.position;
                    dir = dir.normalized;
                    smallJS.transform.localPosition = (Vector3)dir * 50f;
                    break;
                }
                else
                {
                    smallJS.transform.localPosition = Vector3.zero;
                    dir = Vector2.zero;
                }
            }
        }
        else
        {
            smallJS.transform.localPosition = Vector3.zero;
            dir = Vector2.zero;
        }
	}
}
