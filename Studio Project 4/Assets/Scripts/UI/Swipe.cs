﻿using UnityEngine;

public class Swipe : MonoBehaviour {

    // Use this for initialization
    private bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
    private bool isDragging = false; //is finger on screen
    private Vector2 startTouch, swipeDelta;

    public float deadZone = 30; // the offset to stop swipe
    private bool deadZoneReached = false;
    public void Update()
    {
        tap = swipeLeft = swipeRight = swipeUp = swipeDown = false; //everytime we have a new frame, frame is back to false
        
        #region windows Inputs
        if (Input.GetMouseButtonDown(0)) //left click
        {
            tap = true;
            isDragging = true;
            startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0)) //if release
        {
            Reset();
        }
        #endregion

        #region MobileInputs
        if (Input.touches.Length > 0) //we holding down something
        {
            if (Input.touches[0].phase == TouchPhase.Began) //first touch, if we just pressing
            {
                tap = true;
                isDragging = true;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) //check if touch is let go or canceled
            {
                Reset();
            }
         }
        #endregion

        //calculate the distance
        swipeDelta = Vector2.zero;
        if (isDragging) //check if player is still on screen pressing
        {
            if (Input.touches.Length > 0) //get the offset
                swipeDelta = Input.touches[0].position - startTouch;
            else if (Input.GetMouseButton(0))
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
        }

        if (swipeDelta.magnitude > deadZone || deadZoneReached) //if swipe too much
        {
            //check which direction
            float x = swipeDelta.x;
           // float y = swipeDelta.y;

            //check which one is bigger to determind direction
           
           if (x < 0)
               swipeLeft = true;
           else
               swipeRight = true;
            

            Reset();
        }

    }

    private void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDragging = false;
        deadZoneReached = false;
        
    }

    public Vector2 SwipeDelta
    { get { return swipeDelta; }
    }

    public Vector2 StartTouch
    {
        get { return startTouch; }
    }

    public bool SwipeLeft
    { get { return swipeLeft; } }

    public bool SwipeRight
    { get { return swipeRight; }}

    public bool SwipeUp
    { get { return swipeUp; } }

    public bool SwipeDown
    { get { return swipeDown; } }

    public bool IsDraggin
    { get { return isDragging; } }

    public float DeadZone
    { get { return deadZone; } }

    public void SetDeadZone(float _deadZone)
    { deadZone = _deadZone; }

    public void SetDeadZone(bool _deadZone)
    { deadZoneReached = _deadZone; }



}
