﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisorCooldown : MonoBehaviour {

    public GameObject coolDownImage;

	// Use this for initialization
	void Start () {
        coolDownImage.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {

        //Update the fill amount all the time
        Image image = coolDownImage.GetComponent<Image>();

        float MAX_BATTERY = GameDataManager.Instance.playerData.visor.MAX_BATTERY;
        float CURR_BATTERY = GameDataManager.Instance.playerData.visor.batteryLife;

        // image.fillAmount = CURR_BATTERY / MAX_BATTERY;

        image.fillAmount = (MAX_BATTERY - CURR_BATTERY) / MAX_BATTERY;

	}
}
