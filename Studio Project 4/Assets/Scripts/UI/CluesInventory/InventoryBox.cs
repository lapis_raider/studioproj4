﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryBox : MonoBehaviour {
    public GameObject sprite;
    public InventoryInfoBox infoBox;
    Item item;

    public void Start()
    {
        item = null;
        sprite.SetActive(false);
    }

    public void PlaceItem(Item itemPlaced)
    {
        sprite.SetActive(true);
        sprite.GetComponent<Image>().sprite = itemPlaced.icon; //change sprite accordingly
        item = itemPlaced;
    }

    public void RemoveItem()
    {
        item = null;
        sprite.SetActive(false);
    }

    public void Clicked() //when clicked pass info to the box
    {
        if (!item) //if no item just ignore
            return;

        infoBox.Info(item);
    }

    public void TurnOff()
    {
        //change animation to fade out
        gameObject.GetComponent<Animator>().SetTrigger("StartAnimation");
        sprite.SetActive(false);
    }
}
