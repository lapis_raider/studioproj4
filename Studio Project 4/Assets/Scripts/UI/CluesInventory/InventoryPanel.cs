﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class InventoryPanel : MonoBehaviour {

    // Use this for initialization
    public GameObject bottom;
    public GameObject top;
    public GameObject middle;
    public GameObject textBox;

    public GameObject nextButton;
    public GameObject prevButton;

    public float animationSpeed;

    Vector2 originalBottomPos;
    Vector2 originalTopPos;
    InventoryBox[] boxes;

    bool open = false;

    int startIndex = 0;
    bool nextPage = false; //check whether next page already or not
    bool animationStart = false;

	void Awake () {
        originalBottomPos = bottom.transform.localPosition;
        originalTopPos = top.transform.localPosition;

        GameDataManager.Instance.playerData.inventoryData.onItemChange += UpdateInventory;
    }

    public void ToggleInventory()
    {
        AudioManager.instance.Play("bag");
        if (!open)
        {
            gameObject.SetActive(true);
            prevButton.gameObject.SetActive(false); //set the prev button to inactive first
            StartCoroutine(Open());
        }
        else
            StartCoroutine(Close());
    }

    public void Update()
    {
        RectTransform rect = middle.gameObject.GetComponent<RectTransform>();
        rect.offsetMax = new Vector2(rect.offsetMax.x, (top.gameObject.transform.localPosition.y - originalTopPos.y + top.gameObject.GetComponent<RectTransform>().rect.yMax));
        rect.offsetMin = new Vector2(rect.offsetMin.x, bottom.gameObject.transform.localPosition.y - originalBottomPos.y + top.gameObject.GetComponent<RectTransform>().rect.yMin);
    }

    public void UpdateInventory()
    {
        for (int i = 0; i < boxes.Length; ++i)
        {
            if (i + startIndex < GameDataManager.Instance.playerData.inventoryData.items.Count)
            {
                boxes[i].PlaceItem(GameDataManager.Instance.playerData.inventoryData.items[i + startIndex]);
            }
            else
                boxes[i].RemoveItem();
        }
    }

    IEnumerator Open()
    {
        //move to position 0,0, make the scroll open animation
        bottom.gameObject.transform.DOLocalMove(new Vector2(0,0), animationSpeed);
        top.gameObject.transform.DOLocalMove(new Vector2(0, 0), animationSpeed);
        yield return new WaitForSeconds(animationSpeed);

        middle.gameObject.transform.Find("Grid").gameObject.SetActive(true);
        nextButton.gameObject.SetActive(true);
        startIndex = 0;

        if (boxes == null)
            boxes = gameObject.GetComponentsInChildren<InventoryBox>();

        yield return new WaitForSeconds(0.1f);

        if (GameDataManager.Instance.playerData.inventoryData.itemChanged)
        {
            GameDataManager.Instance.playerData.inventoryData.onItemChange.Invoke();
            GameDataManager.Instance.playerData.inventoryData.itemChanged = false;
            GameDataManager.Instance.playerData.inventoryData.inventoryOpened = true;
        }

        open = true;
    }

    IEnumerator Close()
    {
        //close text box if it is on
        if (textBox.GetComponent<InventoryInfoBox>().Close()) //if the box exists to close, wait for a while
            yield return new WaitForSeconds(1.0f);

        //turn of box animation
        for (int i =0; i < boxes.Length; ++i)
        {
            boxes[i].TurnOff();
        }

        yield return new WaitForSeconds(0.7f);
        middle.gameObject.transform.Find("Grid").gameObject.SetActive(false);

        //off the button
        if (prevButton.activeSelf)
            prevButton.gameObject.GetComponent<Animator>().SetTrigger("Close");
        else if (nextButton.activeSelf)
            nextButton.gameObject.GetComponent<Animator>().SetTrigger("Close");

        //make scroll go down
        bottom.gameObject.transform.DOLocalMove(originalBottomPos, animationSpeed);
        top.gameObject.transform.DOLocalMove(originalTopPos, animationSpeed);

        yield return new WaitForSeconds(animationSpeed);

        nextButton.gameObject.SetActive(false);
        prevButton.gameObject.SetActive(false);

        GameDataManager.Instance.playerData.inventoryData.itemChanged = false;
        GameDataManager.Instance.playerData.inventoryData.inventoryOpened = false;

        open = false;
        gameObject.SetActive(false);
    }

    public void TurnPage()
    {
        if (animationStart)
            return;

        nextPage = !nextPage;
        animationStart = true;

        if (nextPage) //if its second page
        {
            startIndex = boxes.Length;
            StartCoroutine(TurningPageAnimation());
           
        }
        else
        {
            startIndex = 0;
            StartCoroutine(PrevPageAnimation());
        }

        UpdateInventory();
    }

    IEnumerator TurningPageAnimation()
    {
        prevButton.gameObject.SetActive(true);
        nextButton.gameObject.GetComponent<Animator>().SetTrigger("Close");
        yield return new WaitForSeconds(0.5f);

        nextButton.gameObject.SetActive(false);
        animationStart = false;
    }

    IEnumerator PrevPageAnimation()
    {
        nextButton.gameObject.SetActive(true);
        prevButton.gameObject.GetComponent<Animator>().SetTrigger("Close");
        yield return new WaitForSeconds(0.5f);

        prevButton.gameObject.SetActive(false);
        animationStart = false;
    }

}
