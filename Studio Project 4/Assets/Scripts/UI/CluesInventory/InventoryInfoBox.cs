﻿using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class InventoryInfoBox : MonoBehaviour {

    public TextMeshProUGUI description;
    public TextMeshProUGUI objName;
    public GameObject image;
    Item prevItem;

    bool boxDisplayed = false; //check whether box is displayed or not

    public void Info(Item itemInfo)
    {
        Debug.Log("displayed call, displayed = " + boxDisplayed);
        if (!boxDisplayed)
        {
            Debug.Log("displayed");
            gameObject.transform.DORotate(new Vector3(0, 0, 0), 1.0f);
            boxDisplayed = true;
        }
        else
        {
            if (prevItem.name == itemInfo.name)
            {
                Debug.Log("undisplayed");
                Close();
            }
        }

        prevItem = itemInfo;
        description.SetText(itemInfo.infomation);
        objName.SetText(itemInfo.name);
        image.GetComponent<Image>().sprite = itemInfo.icon;
    }

    public bool Close()
    {
        if (boxDisplayed)
        {
            gameObject.transform.DORotate(new Vector3(0, 90, 0), 1.0f);
            boxDisplayed = false;
            return true;
        }

        return false;
    }
}
