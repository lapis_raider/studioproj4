﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBillboarding : MonoBehaviour {

    [SerializeField]
    public Camera m_camera; // if you want to use a different camera to billboard

	// Use this for initialization
	void Start () {
        m_camera = Camera.main;
	}
	
	// Update is called once per frame
	void LateUpdate () {
       transform.LookAt(transform.position + m_camera.transform.rotation * Vector3.forward,
     m_camera.transform.rotation * Vector3.up);
    }
}
