﻿using UnityEngine;

public class VisorUI : MonoBehaviour {

	void Start () {
        GameDataManager.Instance.playerData.visor.OnVisorActivateCallBack += UpdateVisorBackground;
    }
	
	void UpdateVisorBackground()
    {
        if (GameDataManager.Instance.playerData.visor.visorActive)
            GetComponent<SpriteRenderer>().enabled = true;
        else
            GetComponent<SpriteRenderer>().enabled = false;
    }


}
