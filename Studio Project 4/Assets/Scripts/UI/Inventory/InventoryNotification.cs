﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryNotification : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameDataManager.Instance.playerData.inventoryData.onItemChange += ShowNotification;
    }

    private void ShowNotification()
    {
        if (GameDataManager.Instance.playerData.inventoryData.itemChanged && !GameDataManager.Instance.playerData.inventoryData.inventoryOpened)
        {
            gameObject.GetComponent<Image>().enabled = !gameObject.GetComponent<Image>().IsActive();
        }
    }
}
