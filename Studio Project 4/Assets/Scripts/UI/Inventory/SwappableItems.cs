﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwappableItems : MonoBehaviour {

    //Declare an array of sprites? 
    [SerializeField]
    private List<Sprite>imageList;

    [SerializeField]
    private List<Sprite>imageIndexList;

    private UsableItemInventoryData inventory;

    //Make the front and back stuff.
    [SerializeField]
    private SwappableObject backUI;
    [SerializeField]
    private SwappableObject frontUI;

    [SerializeField]
    private GameObject associatedObject;

    [SerializeField]
    private GameObject rotateLayer;

    [SerializeField]
    private List<GameObject> layers;

    private Quaternion toRotate;

    private bool initialiseValues = false;

    public float rotateSpeed;

    [SerializeField]
    private GameObject coolDownImage;

    // Use this for initialization
    void Start() {
        inventory = GameDataManager.Instance.playerData.usableItemInventory;
        //inventory.onItemSwapCallback += UpdateUI;
        foreach (ItemUsable item in inventory.items)
        {
            imageList.Add(item.prefabUIImage);
            imageIndexList.Add(item.prefabUIIndex);
        }

        frontUI.GetComponent<Image>().sprite = inventory.items[inventory.currentItem].prefabUIImage;
        backUI.GetComponent<Image>().sprite = inventory.items[inventory.nextItem].prefabUIImage;

        backUI.layerIndex = 0;
        frontUI.layerIndex = 1;
    }
	
	// Update is called once per frame
	void Update () {
        //Make both of them the same first, and only update the back.
        // inventory = GameDataManager.Instance.playerData.usableItemInventory;
        DoRotation();

        CheckSwipe();

        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    frontUI.transform.SetParent(layers[0].transform);
        //    backUI.transform.SetParent(layers[1].transform);
        //}

        if (inventory.items.Count > 0)
        {
            frontUI.GetComponent<Image>().sprite = inventory.items[inventory.currentItem].prefabUIImage;
            backUI.GetComponent<Image>().sprite = inventory.items[inventory.nextItem].prefabUIImage;
            associatedObject.GetComponent<Image>().sprite = inventory.items[inventory.nextItem].prefabUIIndex;
        }
        else
        {
            //else insert empty code here 
        }

        //for cool down
        if (inventory.items[inventory.currentItem].startCoolDown)
        {
            coolDownImage.SetActive(true);
            coolDownImage.GetComponent<Image>().fillAmount = (inventory.items[inventory.currentItem].coolDownTimer - inventory.items[inventory.currentItem].currTimer) / inventory.items[inventory.currentItem].coolDownTimer;
        }
        else
        {
            coolDownImage.SetActive(false);
        }
    }




    void CheckSwipe()
    {
        RectTransform rect = gameObject.transform as RectTransform;
        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        pos.x = pos.x * Screen.width;
        pos.y = pos.y * Screen.height;

        int offset = 50;

        if ((pos.x >= rect.rect.xMin + gameObject.transform.position.x - offset && pos.x <= rect.rect.xMax + gameObject.transform.position.x + offset)
            && (pos.y >= rect.rect.yMin +gameObject.transform.position.y - offset && pos.y <= rect.rect.yMax + gameObject.transform.position.y + offset))
            {

            if (gameObject.GetComponent<Swipe>().SwipeRight)
                GameDataManager.Instance.playerData.usableItemInventory.SwapCurrentItem(1);
            else if (gameObject.GetComponent<Swipe>().SwipeLeft)
                GameDataManager.Instance.playerData.usableItemInventory.SwapCurrentItem(-1);
        }
    }



    void DoRotation()
    {
        if(inventory.doRotation)
        {
            if(!initialiseValues)
            {
                toRotate.y += 180;
                frontUI.GetComponent<Image>().sprite = inventory.items[inventory.currentItem].prefabUIImage;
                backUI.GetComponent<Image>().sprite = inventory.items[inventory.nextItem].prefabUIImage;
                associatedObject.GetComponent<Image>().sprite = inventory.items[inventory.nextItem].prefabUIIndex;
                initialiseValues = true;
            }

            if(rotateLayer.transform.rotation.y != 1.0)
            {
                rotateLayer.transform.rotation = Quaternion.Lerp(rotateLayer.transform.rotation, toRotate, rotateSpeed * Time.deltaTime);
            } 

            if(rotateLayer.transform.rotation.y >= 0.97f)
            {
                inventory.currentItem = inventory.nextItem;
                rotateLayer.transform.rotation = toRotate;

                //Increase both layerIndexes;
                ++frontUI.layerIndex;
                ++backUI.layerIndex;

                int size = layers.Count - 1;
                if (frontUI.layerIndex > size)
                    frontUI.layerIndex = 0;
                else if (frontUI.layerIndex < 0)
                    frontUI.layerIndex = size;
                if (backUI.layerIndex > size)
                    backUI.layerIndex = 0;
                else if (backUI.layerIndex < 0)
                    backUI.layerIndex = size;

                frontUI.transform.SetParent(layers[frontUI.layerIndex].transform);
                backUI.transform.SetParent(layers[backUI.layerIndex].transform);

                Vector3 position = frontUI.transform.position;
                frontUI.transform.position = backUI.transform.position;
                backUI.transform.position = position;

                inventory.doRotation = false;
                initialiseValues = false;

                rotateLayer.transform.rotation = Quaternion.identity;
                toRotate.y = 0;
            }

        }
    }

}
