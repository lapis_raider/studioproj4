﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsableItemUI : MonoBehaviour {

    UsableItemInventoryData inventory;

    //Bad way to do it.
    public GameObject associatedObject;
    
	// Use this for initialization
	void Start () {
        inventory = GameDataManager.Instance.playerData.usableItemInventory;
        inventory.onItemSwapCallback += UpdateUI;
    }

    void UpdateUI()
    {
        // gameObject.GetComponent<Image>().sprite = inventory.items[inventory.currentItem].prefab.GetComponent<SpriteRenderer>().sprite;
        gameObject.GetComponent<Image>().sprite = inventory.items[inventory.currentItem].prefabUIImage;
        associatedObject.GetComponent<Image>().sprite = inventory.items[inventory.currentItem].prefabUIIndex;
    }
}
