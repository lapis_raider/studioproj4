﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwappableObject : MonoBehaviour {

    [SerializeField]
    private GameObject swappableObject;

    public int layerIndex;

    public void SwapLayers(GameObject layer, bool worldSpace = false)
    {
        transform.SetParent(layer.transform, worldSpace);
    }


}
