﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraSanity : MonoBehaviour {

    public Material effectMaterial;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, effectMaterial);
    }
}
