﻿using UnityEngine;

public class CreditScript : MonoBehaviour {

    public void DestroyGameManager()
    {
        AudioManager.instance.StopAllSounds();

        AudioManager.instance.Play("background");
        Destroy(GameDataManager.Instance); 
    }
}
