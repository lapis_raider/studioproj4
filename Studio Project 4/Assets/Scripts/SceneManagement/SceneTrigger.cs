﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class SceneTrigger : MonoBehaviour {

    public Animator transitionAnim;
    
    public Transform moveTo;
    public Transform positionIn = null; //position to lerp to for animation
    public Transform positionOut = null; //when go out lerp to which position for animation
    public float moveSpeed;
    Collider2D player = null;

    public GameObject currentLevel;
    public GameObject transitionLevel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameDataManager.Instance.isPaused)
            return;

        if (collision.gameObject.tag == "Player")
        {
            GameObject bigManSpawnObject = GameObject.Find("BigManSpawn");
            switch(transitionLevel.name)
            {
                case "Level 1":
                    {
                        Vector2 pos = new Vector2(-4.18f, 14.04f);
                        if (currentLevel.name == "Level 2")
                            pos = new Vector2(-4.18f, 14.04f);
                        else if(currentLevel.name == "Level Maze")
                            pos = new Vector2(-29.8f, 14.37f);
                        WayPointBigMan.instance.level = 1;
                        WayPointButler.instance.level = 1;
                        bigManSpawnObject.GetComponent<spawnBigMan>().SetNewSpawn(currentLevel.name, transitionLevel.name, pos);
                    }
                    break;
                case "Level 2":
                    {
                         WayPointBigMan.instance.level = 2;
                         WayPointButler.instance.level = 2;
                         Vector2 pos = new Vector2(-91.1f, -7.12f);
                         bigManSpawnObject.GetComponent<spawnBigMan>().SetNewSpawn(currentLevel.name, transitionLevel.name, pos);
                    }
                    break;
                case "Level Maze":
                    {
                        WayPointBigMan.instance.level = 3;
                        WayPointButler.instance.level = 3;
                        Vector2 pos = new Vector2(-31.2f, 45.3f);
                        bigManSpawnObject.GetComponent<spawnBigMan>().SetNewSpawn(currentLevel.name, transitionLevel.name, pos);
                    }
                    break;
                default:
                    break;
            }
            player = collision;
            StartCoroutine(LoadScene());
        }
    }

    IEnumerator LoadScene()
    {
        GameDataManager.Instance.isPaused = true; //pause the game

        //do quick animation to go to position
        if (positionIn)
            player.gameObject.transform.DOMove(new Vector3(positionIn.position.x, positionIn.position.y, player.gameObject.transform.localPosition.z), moveSpeed);

        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2.0f);

        transitionAnim.SetTrigger("start");
        player.gameObject.transform.localPosition = new Vector3(moveTo.position.x, moveTo.position.y, player.gameObject.transform.localPosition.z);
        if (transitionLevel.name == "Level 1")
        {
            GameDataManager.Instance.playerData.level = PlayerData.LEVEL.LEVEL1;
        }
        else if (transitionLevel.name == "Level 2")
        {
            GameDataManager.Instance.playerData.level = PlayerData.LEVEL.LEVEL2;
        }
        else if (transitionLevel.name == "Level Basement")
        {
            GameDataManager.Instance.playerData.level = PlayerData.LEVEL.LEVELBASEMENT;
        }
        else if (transitionLevel.name == "Level Maze")
        {
            GameDataManager.Instance.playerData.level = PlayerData.LEVEL.LEVELMAZE;
        }
        transitionLevel.SetActive(true);

        transitionAnim.SetTrigger("start");
        AudioManager.instance.Play("stairs");
        yield return new WaitForSeconds(0.5f);

        if (positionOut)
            player.gameObject.transform.DOMove(new Vector3(positionOut.position.x, positionOut.position.y, player.gameObject.transform.localPosition.z), moveSpeed);

        yield return new WaitForSeconds(moveSpeed);
        GameDataManager.Instance.isPaused = false; //unpause game
        currentLevel.SetActive(false);
    }
}
