﻿using System.Collections;
using System; // exceptions
using UnityEngine.SceneManagement; // scene management
using UnityEngine;


public class SceneManagement : MonoBehaviour
{
    //Load a standard scene to the screen
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadSceneAdditive(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        GameDataManager.Instance.doMovement = false;
    }

    public void UnloadScene(string sceneName)
    {
        try
        {
            SceneManager.UnloadSceneAsync(sceneName);
            GameDataManager.Instance.doMovement = true;
        }
        catch
        {
            Debug.Log("Scene isnt load, load scene to unload scene");
        }
    }

    //Load the scene asyncronously (For loading screens?)
    public void LoadSceneAsync(string sceneName)
    {
        StartCoroutine(LoadAsyncScene(sceneName));
    }

    //Merging two scenes together?
    public void MergeScenes(string sceneName, string sceneName2)
    {
        if(sceneName == sceneName2)
        {
            Debug.Log("The two scene names can not be identical");
            return;
        }

        //Else we get the scene by their names
        Scene sceneOne = SceneManager.GetSceneByName(sceneName);
        Scene sceneTwo = SceneManager.GetSceneByName(sceneName2);

        try
        {
            SceneManager.MergeScenes(sceneOne, sceneTwo);
        }
        catch (Exception exception)
        {
            Debug.Log(exception.ToString());
            Debug.Log("Scene Manager failed to merge " + sceneName + " with " + sceneName2);
        }

    }


    //The coroutine for loading a scene asyncronously
    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);

        if (!asyncOperation.isDone)
            yield return null;
    }
    
    //Quit the application (used for mobile and stuff)
    public void QuitGame()
    {
        Application.Quit();
    }


}
