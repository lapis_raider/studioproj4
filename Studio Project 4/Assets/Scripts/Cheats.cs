﻿using UnityEngine;

public class Cheats : MonoBehaviour {

    public Item weapon;
    public Item picture;
    public Item bible;
    public Item memento;
    public Item journal;
    public Item hand;
    public Item ms1;
    public Item ms2;
    public Item ms3;
    public Item ms4;

    // Update is called once per frame
    void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)) // Can't die
        {
            GameDataManager.Instance.playerData.PlayerSanityDecrease(-10000);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) // Give all but final clue
        {
            GameDataManager.Instance.playerData.inventoryData.Add(weapon);
            GameDataManager.Instance.playerData.inventoryData.Add(picture);
            GameDataManager.Instance.playerData.inventoryData.Add(bible);
            GameDataManager.Instance.playerData.inventoryData.Add(memento);
            GameDataManager.Instance.playerData.inventoryData.Add(journal);
            GameDataManager.Instance.playerData.inventoryData.Add(hand);
            GameDataManager.Instance.playerData.inventoryData.Add(ms1);
            GameDataManager.Instance.playerData.inventoryData.Add(ms2);
            GameDataManager.Instance.playerData.inventoryData.Add(ms3);
            GameDataManager.Instance.playerData.inventoryData.Add(ms4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GetComponent<BoxCollider2D>().enabled = !GetComponent<BoxCollider2D>().enabled;
        }
	}
}
