﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UsableItemInventoryData{

    // Use this for initialization
    //the list of items
    //number of items
    public List<ItemUsable> items = new List<ItemUsable>();
   
    //[HideInInspector]
    public int currentItem = 0;

    [HideInInspector]
    public int nextItem = 0;

    public delegate void OnItemSwap();
    public OnItemSwap onItemSwapCallback;

    public bool doRotation = false;
    

	// Update is called once per frame
	public void SwapCurrentItem(int currentOffset)
    {
        if (items == null)
            return;

        if (doRotation) // cant change during animation
            return;

        if (items.Count <= 0)
            return;

        //currentItem += currentOffset;
        //nextItem = currentItem;

        //if (currentItem < 0)
        //    currentItem = items.Count - 1;
        //else if (currentItem >= items.Count)
        //    currentItem = 0;

        nextItem += currentOffset;
        doRotation = true;

        if (nextItem < 0)
            nextItem = items.Count - 1;
        else if (nextItem >= items.Count)
            nextItem = 0;

        if (onItemSwapCallback != null)
             onItemSwapCallback.Invoke();
    }

    public void RemoveCurrentItem()
    {
        items.Remove(items[currentItem]);

        if (currentItem < 0)
            currentItem = items.Count - 1;
        else if (currentItem >= items.Count)
            currentItem = 0;

        if (nextItem < 0)
            nextItem = items.Count - 1;
        else if (nextItem >= items.Count)
            nextItem = 0;
    }
}
