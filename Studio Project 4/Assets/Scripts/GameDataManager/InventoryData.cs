﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryData {

    //Test
    public int inventorySize = 5;
    private string infoString;

    public List<Item> items = new List<Item>();

    public delegate void OnChangeItem();
    public OnChangeItem onItemChange;

    [HideInInspector]
    public bool inventoryOpened = false;

    [HideInInspector]
    public bool itemChanged = false;

    public void Add(Item item)
    {
        if (item.addToInventory) //if its an item that should be added into inventory
        {
            items.Add(item);

            if ((int)item.type < GameDataManager.Instance.playerData.clues.Length)
                GameDataManager.Instance.playerData.clues[(int)item.type] = true;
            else
                GameDataManager.Instance.gotCheese = true;

            itemChanged = true;
            onItemChange.Invoke();

            AudioManager.instance.Play("ItemPickup");
        }
    }

    public void Remove(Item item)
    {
        items.Remove(item);
        itemChanged = true;
        onItemChange.Invoke();
        
    }

    public string Print()
    {
        infoString = "\n---Inventory Data---\n";

        double duo = 2;
        string wasd = "kms";

        //The data to add to your textfile?
        //EDIT THIS PART ONLY
        DataFormatter.FormatData(ref infoString, "Inventory Size", inventorySize);
        DataFormatter.FormatData(ref infoString, "String", wasd);
        DataFormatter.FormatData(ref infoString, "duo", duo);
        DataFormatter.FormatData(ref infoString, "basic", Vector3.zero);

        if (items.Count > 0)
        {
            infoString += "---Inventory Items---\n";
            foreach (Item item in items)
            {
                DataFormatter.FormatData(ref infoString,"Item", item.name);
            }
        }
        //END OF EDIT
 
        infoString += "\n";

        return infoString;
    }
    
    //Probably a bad way to do it
    public void ReplaceData(InventoryData thatData)
    {
        this.inventorySize = thatData.inventorySize;
    }
    
    //Not the best way but yknow, until I find a better way
    public void FormatData(string dataName, string theString)
    {
        infoString += dataName + ": " + theString + "\n";
    }

    public void FormatData(string dataName, int theInt)
    {
        infoString += dataName + ": " + theInt + "\n";
    }

    public void FormatData(string dataName, float theFloat)
    {
        infoString += dataName + ": " + theFloat + "\n";
    }

    public void FormatData(string dataName, double theDouble)
    {
        infoString += dataName + ": " + theDouble + "\n";
    }

    public void FormatData(string dataName, Vector3 theVector3)
    {
        infoString += dataName + ": " + theVector3 + "\n";
    }

    public void FormatData(string dataName, Vector2 theVector2)
    {
        infoString += dataName + ": " + theVector2 + "\n";
    }

}
