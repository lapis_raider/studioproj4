﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class KeybindsData : MonoBehaviour {

    [SerializeField]
    private OptionsMenu optionsMenu;

    public Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();

    //Movement texts
    public Text moveUp, moveDown, moveLeft, moveRight;

    //Interaction texts
    public Text interactText, actionText;

    //Visor Text
    public Text visorText;

    //Minimap Text
    public Text minimapText;


    public GameObject changingText;

    //Const string for movements.
    const string up = "MoveUp";
    const string down = "MoveDown";
    const string left = "MoveLeft";
    const string right = "MoveRight";
    const string interact = "Interact";
    const string action = "Action";
    const string visor = "Visor";
    const string minimap = "Minimap";

    private GameObject currentKey;

    public bool inOptions = false;

    // Use this for initialization
    void Start () {
        keys = new Dictionary<string, KeyCode>();
        if (!inOptions)
        {
            //Movement keys
            keys.Add(up, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(up, "W")));
            keys.Add(down, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(down, "S")));
            keys.Add(left, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(left, "A")));
            keys.Add(right, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(right, "D")));

            //Movement text
            moveUp.text = keys[up].ToString();
            moveDown.text = keys[down].ToString();
            moveLeft.text = keys[left].ToString();
            moveRight.text = keys[right].ToString();

            //Add the keys to the keybinding
            keys.Add(interact, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(interact, "E")));
            keys.Add(action, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(action, "Space")));
            keys.Add(visor, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(visor, "V")));
            keys.Add(minimap, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(minimap, "M")));

            interactText.text = keys[interact].ToString();
            actionText.text = keys[action].ToString();
            visorText.text = keys[visor].ToString();
            minimapText.text = keys[minimap].ToString();


        }
        else
        {
            keys = GameDataManager.Instance.keys;

            //Movement text
            //moveUp.text = keys[up].ToString();
            //moveDown.text = keys[down].ToString();
            //moveLeft.text = keys[left].ToString();
            //moveRight.text = keys[right].ToString();
            //interactText.text = keys[interact].ToString();
            //actionText.text = keys[action].ToString();
            //visorText.text = keys[visor].ToString();
            //minimapText.text = keys[minimap].ToString();
        }

        //Save the defaults first?
        SaveKeyBinds();
    }

    public void SaveKeyBinds()
    {
        GameDataManager.Instance.keys = keys;
    }

    void OnGUI()
    {
        if(currentKey != null)
        {
            Event e = Event.current;
            if(e.isKey)
            {
                keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
                changingText.SetActive(false);
                optionsMenu.settingsChanged = true;
            }

        }
    }
    public void ChangeKey(GameObject clicked)
    {
        currentKey = clicked;
    }

    public void SaveKeys()
    {
        foreach(var key in keys)
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }

        PlayerPrefs.Save();
        SaveKeyBinds();
    }

    public void ChangingKey(string keyString)
    {
        changingText.SetActive(true);
        changingText.GetComponentInChildren<Text>().text = "Changing key bound in... " + keyString;
    }

    public void RevertKeybindings()
    {
        keys = new Dictionary<string, KeyCode>();
        keys.Add(up, KeyCode.W);
        keys.Add(down, KeyCode.S);
        keys.Add(left, KeyCode.A);
        keys.Add(right, KeyCode.D);
        keys.Add(visor, KeyCode.V);
        keys.Add(minimap, KeyCode.M);

        //Movement text
        moveUp.text = keys[up].ToString();
        moveDown.text = keys[down].ToString();
        moveLeft.text = keys[left].ToString();
        moveRight.text = keys[right].ToString();

        //Add the keys to the keybinding
        keys.Add(interact, KeyCode.E);
        keys.Add(action, KeyCode.Space);

        interactText.text = keys[interact].ToString();
        actionText.text = keys[action].ToString();
        visorText.text = keys[visor].ToString();
        minimapText.text = keys[minimap].ToString();
        optionsMenu.settingsChanged = true;
    }

}
