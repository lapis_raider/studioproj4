﻿using UnityEngine;

//This is the data to save
[System.Serializable]
public class PlayerData {
    //The inventory of the player
    public InventoryData inventoryData;
    public UsableItemInventoryData usableItemInventory;
    public PlayerVisor visor;

    //for player info
    public Material cameraMaterial;
    public int playerFullSanity;

    [HideInInspector]
    public int playerSanity;

    //The position of the character
    public Vector3 position;

    //Saving to text file
    private string infoString;

    public enum LEVEL
    {
        LEVEL1 = 0,
        LEVEL2,
        LEVELMAZE,
        LEVELBASEMENT,
    }
    public LEVEL level;

    public enum CLUES
    {
        WEAPON = 0,
        PICTURE,
        BIBLE,
        MEMENTO,
        JOURNAL,
        ARM,
        FAMILY,
        MUSICSHEET_1,
        MUSICSHEET_2,
        MUSICSHEET_3,
        MUSICSHEET_4,
        CLUES_TOTAL
    }
    [HideInInspector]
    public bool[] clues;

    public void Init()
    {
        playerSanity = playerFullSanity;
        cameraMaterial.SetFloat("_EffectAmount", 0.0f); //set camera to original amt
        visor.Init();
        clues = new bool[(int)CLUES.CLUES_TOTAL];
    }

    //Functions to save stuff
    public void SaveInventory(InventoryData thatData)
    {
        this.inventoryData = thatData;
    }

    public string PrintPlayerData()
    {
        infoString = "---Player Data---\n";
       DataFormatter.FormatData(ref infoString,"Position", position);

        //Call the inventory print func tion
        infoString += inventoryData.Print();

        return infoString;
    }

    public void PlayerSanityDecrease(int decrease)
    {
        playerSanity -= decrease;

        //edit the greyscale level of camera
        float percentage = (playerFullSanity - playerSanity) / (float)playerFullSanity;
        percentage = Mathf.Clamp01(percentage);

        cameraMaterial.SetFloat("_EffectAmount", percentage);
        AudioManager.instance.Play("Damage");
    }
}
