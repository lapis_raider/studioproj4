﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsData {
    //Save settings to a text file
    private string infoString;

    //All the settings to save are stored here

    //The storing of music value
    public float musicValue;
    //The storing of sfx value;
    public float sfxValue;

    public string PrintSettingsData()
    {
        infoString = "---Settings Data---\n";
        DataFormatter.FormatData(ref infoString, "Music", musicValue);
        DataFormatter.FormatData(ref infoString, "SFX", sfxValue);

        return infoString;
    }


}
