﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataFormatter : MonoBehaviour {

    //Not the best way but yknow, until I find a better way
    public static void FormatData(ref string infoString, string dataName, string theString)
    {
        infoString += dataName + ": " + theString + "\n";
    }

    public static void FormatData(ref string infoString, string dataName, int theInt)
    {
        infoString += dataName + ": " + theInt + "\n";
    }

    public static void FormatData(ref string infoString, string dataName, float theFloat)
    {
        infoString += dataName + ": " + theFloat + "\n";
    }

    public static void FormatData(ref string infoString, string dataName, double theDouble)
    {
        infoString += dataName + ": " + theDouble + "\n";
    }

    public static void FormatData(ref string infoString, string dataName, Vector3 theVector3)
    {
        infoString += dataName + ": " + theVector3 + "\n";
    }

    public static void FormatData(ref string infoString, string dataName, Vector2 theVector2)
    {
        infoString += dataName + ": " + theVector2 + "\n";
    }
    
}
