﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameDataManager : MonoBehaviour {

    //Set the singletonData
    public static GameDataManager Instance;

    //The values to save, dump all the classes here
    public PlayerData playerData;
    //Settings values
    public SettingsData settingsData;
    //Text editor
    TextFileEditor editor;
    //Password for password game
    public string password;
    public int passwordChar = 6;
  
    bool _doMovement; // if player should run playermovement update
    [HideInInspector]
    public bool bookgame; // if player beat book minigame
    [HideInInspector]
    public bool computerGame = false; // if player beat book computer game
    [HideInInspector]
    public bool musicGame = false; // if player beat music game
    [HideInInspector]
    public bool passwordGame = false; // if player beat password game

    [HideInInspector]
    public bool gotCheese = false;

    //KeyBinds
    [HideInInspector]
    public Dictionary<string, KeyCode> keys;

    //IsGamePaused
    public bool isPaused = false;

    private bool endGame = false;
    void Awake()
    {
        //Get the instance of this certain class
        if(Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    //To initialise playerData
    private void Start()
    {
        //Get the component of the editor
        editor = GetComponent<TextFileEditor>();

        playerData.Init();

        _doMovement = true;
    }

    //Function to save data
    public void SavePlayerData(PlayerData theData)
    {
       // Debug.Log("Player data has been saved!");
        this.playerData = theData;
    }

    //Function save another settings data?
    public void SaveSettingsData(SettingsData theData)
    {
       // Debug.Log("Settings data has been saved!");
        this.settingsData = theData;
    }


    //To print the playerData and its subsequent classes
    string PrintPlayerData()
    {
        return this.playerData.PrintPlayerData();
    }

    //To print the settingsData so that saving is easier.
    string PrintSettingsData()
    {
        return this.settingsData.PrintSettingsData();
    }

    void LoadSettingsData()
    {
        //Do file reading
    }

    //Update test function
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    editor.WriteText("playerInfo", PrintPlayerData(), false   );
        //}
        //if(Input.GetKeyDown(KeyCode.P))
        //{
        //    SaveOptions();
        //}
        if (!endGame)
        {
            if (playerData.playerSanity <= 0)
            {
                SceneManager.LoadScene("WinLoseScene");
                endGame = true;
            }
        }
    }
    
    //Function to check if the player can move
    public bool doMovement
    {   
        get { return _doMovement; }
        set {
            _doMovement = value;
            if (!_doMovement)
                BaseEnemyInfo.stopUpdate = true;
            else
                BaseEnemyInfo.stopUpdate = false;
        }
    }

    //To save the settings by writing into a text file.
    public void SaveOptions()
    {
        editor.WriteText("settings", PrintSettingsData(), false);
    }
}
