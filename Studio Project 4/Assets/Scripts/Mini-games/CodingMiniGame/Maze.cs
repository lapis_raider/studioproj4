﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Maze : MonoBehaviour
{
    [HideInInspector]
    public List<GameObject> pathways = new List<GameObject>();

    RectTransform rectTransform;
    Vector2 minPos;
    Vector2 maxPos;

    DIR[,] mazeSet;

    public enum DIR
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        NONE
    }

    DIR unavilable = DIR.NONE; //cannot go to this direction

    bool destroyAll = false;

    private void Awake()
    {
        mazeSet = new DIR[5, 7] { { DIR.UP, DIR.RIGHT, DIR.RIGHT, DIR.RIGHT, DIR.UP, DIR.LEFT, DIR.LEFT },
                                  { DIR.UP, DIR.RIGHT, DIR.DOWN, DIR.RIGHT, DIR.UP, DIR.RIGHT, DIR.DOWN },
                                  { DIR.UP, DIR.UP, DIR.UP, DIR.RIGHT, DIR.DOWN, DIR.RIGHT, DIR.DOWN },
                                  { DIR.RIGHT, DIR.UP, DIR.RIGHT, DIR.RIGHT, DIR.UP, DIR.LEFT, DIR.UP },
                                  { DIR.RIGHT, DIR.UP, DIR.LEFT, DIR.UP, DIR.RIGHT, DIR.RIGHT, DIR.DOWN } };
    }

    private void Start()
    {
        if (GameDataManager.Instance.computerGame)
            return;

        rectTransform = transform as RectTransform;
        minPos = new Vector2(rectTransform.position.x + rectTransform.rect.xMin, rectTransform.position.y + rectTransform.rect.yMin);
        maxPos = new Vector2(rectTransform.position.x + rectTransform.rect.xMax, rectTransform.position.y + rectTransform.rect.yMax);

        //initialize maze
        MazeGeneration();
    }
    public void MazeGeneration()
    {
        int chosen = Random.Range(0, 5);

        DrawMaze(mazeSet[chosen, 0], 0);
        DrawMaze(mazeSet[chosen, 1], 1);
        DrawMaze(mazeSet[chosen, 2], 2);
        DrawMaze(mazeSet[chosen, 3], 3);
        DrawMaze(mazeSet[chosen, 4], 4);
        DrawMaze(mazeSet[chosen, 5], 5);
        DrawMaze(mazeSet[chosen, 6], 6);
    }

    public void DestroyAll()
    {
        if (pathways.Count > 0) //rset
        {
            for (int i = 0; i < pathways.Count; ++i)
            {
                GameObject obj = pathways[i];
                Destroy(obj);
            }
            pathways.Clear();

            destroyAll = false;
            unavilable = DIR.NONE;
        }
    }

    public void Check(Maze.DIR type)
    {
        switch (type) //set the next unavialable direction
        {
            case DIR.UP:
                unavilable = DIR.DOWN;
                break;
            case DIR.DOWN:
                unavilable = DIR.UP;
                break;
            case DIR.RIGHT:
                unavilable = DIR.LEFT;
                break;
            case DIR.LEFT:
                unavilable = DIR.RIGHT;
                break;
        }
    }

    public bool DrawMaze(Maze.DIR type, int index)
    {
        //instantiate, start position
        GameObject obj;
        string fileName = "";

        switch (type)
        {
            case DIR.UP:
                fileName = "MiniGame/Computer/UpPath";
                break;
            case DIR.DOWN:
                fileName = "MiniGame/Computer/DownPath";
                break;
            case DIR.LEFT:
                fileName = "MiniGame/Computer/leftPath";
                break;
            case DIR.RIGHT:
                fileName = "MiniGame/Computer/RightPath";
                break;
        }

        obj = Resources.Load<GameObject>(fileName);
        obj = Instantiate(obj, transform);
        obj.transform.localPosition = Vector3.zero;

        if (index - 1 >= 0)
        {
            obj.transform.position = pathways[index - 1].transform.Find("pivot").gameObject.transform.position;
        }

        //if my next pivot location is outside box, return false and destroy
        //Vector3 objPivot = obj.transform.Find("pivot").gameObject.transform.position;
        //if (objPivot.x < minPos.x || objPivot.y < minPos.y || objPivot.x > maxPos.x || objPivot.y > maxPos.y)
        //{
        //    Destroy(obj);
        //    return false;
        //}
        //if (CheckMazeCollision(obj))
        //{
        //    //destroy everything, return false
        //    Destroy(obj);
        //    destroyAll = true;
        //    return true;
        //}

        pathways.Add(obj);
        return true;
    }

    bool CheckMazeCollision(GameObject obj)
    {
        for (int r = 0; r < pathways.Count; ++r)
        {
            if (obj == pathways[r])
                continue;

            RectTransform rectTransform = pathways[r].transform as RectTransform;
            Vector2 pos = obj.transform.Find("pivot").gameObject.transform.position;
            if (pos.y > rectTransform.rect.yMin + rectTransform.position.y && pos.y < rectTransform.rect.yMax + rectTransform.position.y &&
                pos.x > rectTransform.rect.xMin + rectTransform.position.x && pos.x < rectTransform.rect.xMax + rectTransform.position.x)
            {

                return true;
            }
        }

        return false;
    }
}
