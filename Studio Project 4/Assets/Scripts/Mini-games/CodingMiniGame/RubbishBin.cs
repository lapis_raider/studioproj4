﻿using UnityEngine;
using UnityEngine.UI;

public class RubbishBin : MonoBehaviour {

    public Sprite close;
    public Sprite open;

    public void CloseRubbish()
    {
        gameObject.GetComponent<Image>().sprite = close;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        gameObject.GetComponent<Image>().sprite = open;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        CloseRubbish();
    }
}
