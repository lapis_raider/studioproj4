﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class CodeMiniGameConditions : MonoBehaviour {

    public Sprite win;
    public Sprite lose;
    public GameObject compScreen;//computer screen info
    public TextMeshProUGUI text;

    string textAnimation;

    bool winCondition;
    bool animationStart;

    public void Awake()
    {
        gameObject.GetComponent<Image>().enabled = false;
        if (GameDataManager.Instance.computerGame)//if win already
        {
            compScreen.SetActive(false);
            text.SetText("HACKED PASSWORD:\n" + GameDataManager.Instance.password);
        }
    }


    public void Conditions(bool winner)
    {
        if (winner)
        {
            gameObject.GetComponent<Image>().sprite = win;
            AudioManager.instance.Play("Correct");
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = lose;
            AudioManager.instance.Play("Wrong");
        }

        gameObject.GetComponent<Image>().enabled = true;

        gameObject.GetComponent<Animator>().SetBool("Win", winner);
        gameObject.GetComponent<Animator>().SetBool("StartAnimation", true);
    }

    public void FinishLose()
    {
        gameObject.GetComponent<Image>().enabled = false;
        gameObject.GetComponent<Animator>().SetBool("StartAnimation", false);
    }

    public void FinishWin()
    {
        gameObject.GetComponent<Image>().enabled = false;
        compScreen.SetActive(false);

        //activate animation
        gameObject.GetComponent<Animator>().SetBool("StartAnimation", false);

        //start the text
        //set password
        GameDataManager.Instance.password = "";
        for (int i =0; i < GameDataManager.Instance.passwordChar; ++i)
        {
            GameDataManager.Instance.password += Random.Range(0, 9).ToString();
        }
        GameDataManager.Instance.computerGame = true;

        textAnimation = "HACKED PASSWORD:\n" + GameDataManager.Instance.password;

      
        StopAllCoroutines();
        StartCoroutine(AnimateText());
    }
   
    IEnumerator AnimateText()
    {
        string currText = "";

        for (int i =0; i < textAnimation.Length; ++i)
        {
            currText += textAnimation[i];
            text.SetText(currText);

            yield return new WaitForSeconds(0.05f);
        }        
    }
}
