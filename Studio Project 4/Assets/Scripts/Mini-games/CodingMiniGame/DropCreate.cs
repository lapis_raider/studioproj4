﻿using UnityEngine;

public class DropCreate : NodeDrop {

    public GameObject nodePrefab; //node to drop
    public NodeManager manager;

    override public void DropBehaviour()
    {
        manager.Insert(nodePrefab.GetComponent<MovementNode>(), index);
        AudioManager.instance.Play("ComputerButton");
    }
}
