﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NodeDrop : MonoBehaviour, IDropHandler {

    public GameObject choosePanel; //the panel where we choose things
    public GameObject dropPanel; //the panel where we drop things at

    [HideInInspector]
    public int index = -1;

    virtual public void OnDrop(PointerEventData eventData)
    {
        RectTransform choosePanelRect = choosePanel.transform as RectTransform;
        RectTransform dropPanelRect = dropPanel.transform as RectTransform;

        if (!RectTransformUtility.RectangleContainsScreenPoint(choosePanelRect, Input.mousePosition)) //if out of choose panel
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(dropPanelRect, Input.mousePosition))
            {
                DropBehaviour();
            }
        }
    }

    virtual public void DropBehaviour()
    {
        Debug.Log("It's been dropped");
    }
}
