﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MovementNode : NodeType
{
    [HideInInspector]
    public RectTransform rect;

    [HideInInspector]
    public bool rightAdd; //add on the right
    [HideInInspector]
    public bool leftAdd; //add on the left

    [HideInInspector]
    public int index = 0;

    private void Awake()
    {
        rect = transform as RectTransform;

        Material newMaterial = new Material(Shader.Find("Custom/Outline"));
        gameObject.GetComponent<Image>().material = newMaterial;
    }
}
