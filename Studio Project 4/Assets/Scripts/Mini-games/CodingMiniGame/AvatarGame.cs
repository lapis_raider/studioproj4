﻿using UnityEngine;
using DG.Tweening;

public class AvatarGame : MonoBehaviour {

    // Use this for initialization
    Vector2 dir = new Vector2(1,0); //direction facing, init to right

    public Maze mazeInfo;
    public NodeManager nodeInfo;
    public NodeUI nodeUI;
    public CodeMiniGameConditions conditions;
    public GameObject rotateObj;

    Quaternion childRotation;

    float nextRotation;
    Vector2 nextPos;

    bool start = false;

    int currMovementNode = 0;
    int currPosNode = 0;
    bool nextNode = false; //go to the next node

    float SPEED = 1.0f;
    float timer = 0.0f;

	void Start () {

        ResetVar();
        childRotation = rotateObj.transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        //update action, before going to next movement
        if (start)
        {
            if (nextNode) //if check next node
            {
                ++currMovementNode; //cehck whats the next movement
                if (currMovementNode >= nodeInfo.nodeList.Count) //stop updating, check if win
                {
                    start = false;
                    nextNode = false;
                    CheckWin();
                    return;
                }
              
                UpdateMovement();

                nodeUI.slots[currMovementNode - 1].LightDown();
                nodeUI.slots[currMovementNode].LightUp();

                timer = 0.0f;
                nextNode = false;
            }

           
            gameObject.transform.DOMove(nextPos, SPEED);
            gameObject.transform.DORotate(new Vector3(0, 0, nextRotation), SPEED);

            timer += Time.deltaTime;

            if (timer > SPEED)
                nextNode = true;
        }
    }

    public void LateUpdate()
    {
        rotateObj.transform.rotation = childRotation;
    }

    public void UpdateMovement()
    {
       switch (nodeInfo.nodeList[currMovementNode].type)
        {
            case NodeType.MOVE_TYPE.FORWARD:
                {
                    Vector2 checkDir = (mazeInfo.pathways[currPosNode].transform.Find("pivot").gameObject.transform.position - transform.position).normalized;

                    float value = Mathf.Abs(Vector2.Dot(checkDir, dir) - 1);
                    if (value <= 0.0005 && value >= 0)
                    {
                        nextPos = mazeInfo.pathways[currPosNode].transform.Find("pivot").gameObject.transform.position;
                        ++currPosNode;
                    }
                }
                break;
            case NodeType.MOVE_TYPE.RIGHT:
                nextRotation -= 90.0f; //rotate right
                dir = new Vector2(Mathf.Cos(nextRotation * Mathf.Deg2Rad), Mathf.Sin(nextRotation * Mathf.Deg2Rad));
                break;
            case NodeType.MOVE_TYPE.LEFT:
                nextRotation += 90.0f; //rotate left
                dir = new Vector2(Mathf.Cos(nextRotation * Mathf.Deg2Rad), Mathf.Sin(nextRotation * Mathf.Deg2Rad));
                break;
        }
    }

    public void CheckWin()
    {
        if (currPosNode >= mazeInfo.pathways.Count) //last pos win
            conditions.Conditions(true);
        else
            conditions.Conditions(false);

        ResetVar();
    }

    public void StartMovement()
    {
        if (GameDataManager.Instance.computerGame)
            return;

        if (!start && nodeInfo.nodeList.Count > 0) //if havent start yet
        {
            start = true;

            //reset variables
            ResetVar();

            nodeUI.slots[currMovementNode].LightUp();
            UpdateMovement();
        }
    }

    public void ResetVar()
    {
        nextPos = gameObject.transform.position = mazeInfo.pathways[0].gameObject.transform.position;
        dir = (mazeInfo.pathways[0].transform.Find("pivot").gameObject.transform.position - mazeInfo.pathways[0].gameObject.transform.position).normalized;

        nextRotation = Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x);
        gameObject.transform.DORotate(new Vector3(0, 0, nextRotation), 0); //rotate

        currMovementNode = 0;
        currPosNode = 0;
        timer = 0.0f;
        nextNode = false;

       for (int i=0; i < nodeUI.slots.Length; ++i)
        {
            nodeUI.slots[i].LightDown();
        }
    }
}
