﻿using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour {

    // Use this for initialization
    [HideInInspector]
    public List<MovementNode> nodeList = new List<MovementNode>();
    
    public LayerMask targetMask;

    public delegate void OnNodeUpdate();
    public OnNodeUpdate onNodeUpdateCallback;

    public void resetList()
    {
        nodeList.Clear();
    }

    public void Insert(MovementNode node, int index = -1)
    {
        if (index != -1 && index < nodeList.Count)
        {
            nodeList.Insert(index, node);
            onNodeUpdateCallback.Invoke();
            return;
        }

        nodeList.Add(node);
        onNodeUpdateCallback.Invoke();
        return;
    }

    public void ReInsert(MovementNode node)
    {
        nodeList.Remove(node); //remove it from the position
        Insert(node); //reinsert it
    }

    public void RemoveNode(int index)
    {
        nodeList.RemoveAt(index);
        onNodeUpdateCallback.Invoke();
    }
}
