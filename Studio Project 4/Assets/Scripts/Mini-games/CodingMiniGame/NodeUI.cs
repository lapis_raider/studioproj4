﻿using UnityEngine;

public class NodeUI : MonoBehaviour {

    // Use this for initialization
    NodeManager nodeList;

    [HideInInspector]
    public NodeSlotUI[] slots;

   	void Start () {
        nodeList = gameObject.GetComponent<NodeManager>();
        nodeList.onNodeUpdateCallback += UpdateUI;

        slots = gameObject.GetComponentsInChildren<NodeSlotUI>();

        for (int i = 0; i < slots.Length; ++i)
        {
            slots[i].index = i;
        }
    }
	
    void UpdateUI()
    {
        for (int i =0; i < slots.Length; ++i)
        {
            if (i < nodeList.nodeList.Count)
            {
                nodeList.nodeList[i].index = i;
                slots[i].AddNode(nodeList.nodeList[i]);
            }
            else
                slots[i].RemoveNode();
        }
    }
}
