﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeType : MonoBehaviour {

    public enum MOVE_TYPE
    {
        FORWARD,
        RIGHT,
        LEFT
    };

    public MOVE_TYPE type;
}
