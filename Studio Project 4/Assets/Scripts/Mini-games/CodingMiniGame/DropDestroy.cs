﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropDestroy : NodeDrop
{
    public NodeManager manager;

    override public void OnDrop(PointerEventData eventData)
    {
        RectTransform choosePanelRect = choosePanel.transform as RectTransform;
        RectTransform dropPanelRect = dropPanel.transform as RectTransform;

        if (!RectTransformUtility.RectangleContainsScreenPoint(choosePanelRect, Input.mousePosition)) //if out of choose panel
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(dropPanelRect, Input.mousePosition))
            {
                DropBehaviour();
            }
        }
        else
            DropBehaviourInContainer();
    }

    override public void DropBehaviour()
    {
        //remove from list
        gameObject.transform.SetParent(gameObject.GetComponent<NodeDrag>().originalParent.transform);
        gameObject.transform.localPosition = Vector3.zero;

        AudioManager.instance.Play("trash");

        if (dropPanel.gameObject.GetComponent<RubbishBin>())
            dropPanel.gameObject.GetComponent<RubbishBin>().CloseRubbish();

        manager.RemoveNode(GetComponent<MovementNode>().index);
    }

    public void DropBehaviourInContainer()
    {
        gameObject.transform.SetParent(gameObject.GetComponent<NodeDrag>().originalParent.transform);
        gameObject.transform.localPosition = Vector3.zero;
        MovementNode moveNode = new MovementNode();
        moveNode.index = GetComponent<MovementNode>().index;
        moveNode.type = GetComponent<MovementNode>().type;

        manager.nodeList.RemoveAt(GetComponent<MovementNode>().index);
        manager.Insert(moveNode, index);
    }
}
