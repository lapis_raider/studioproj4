﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NodeDrag : MonoBehaviour, IDragHandler, IEndDragHandler {

    public bool snapBack = true;
    public GameObject newParent;
    public GameObject originalParent;

    virtual public void OnDrag(PointerEventData eventData)
    {
        transform.SetParent(newParent.transform);
        transform.position = Input.mousePosition;
    }

    virtual public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(originalParent.transform);
        if (snapBack)
            transform.localPosition = Vector3.zero; //original position
    }
}
