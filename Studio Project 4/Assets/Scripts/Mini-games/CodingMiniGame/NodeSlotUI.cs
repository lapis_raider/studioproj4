﻿using UnityEngine;
using UnityEngine.UI;

public class NodeSlotUI : MonoBehaviour
{
    // Use this for initialization
    public GameObject nodeObj;
    MovementNode node;

    [HideInInspector]
    public int index;

    public void Awake()
    {
        Material newMaterial = new Material(Shader.Find("Custom/Outline"));
        gameObject.GetComponent<Image>().material = newMaterial;
    }
 
    public void AddNode(MovementNode _node)
    {
        node = _node;
        nodeObj.SetActive(true);

        nodeObj.GetComponent<MovementNode>().type = _node.type;
        nodeObj.GetComponent<MovementNode>().index = _node.index;
        switch (_node.type)
        {
            case NodeType.MOVE_TYPE.FORWARD:
                nodeObj.GetComponent<Image>().sprite = Resources.Load<Sprite>("MiniGame/Computer/arrow");
                break;
            case NodeType.MOVE_TYPE.LEFT:
                nodeObj.GetComponent<Image>().sprite = Resources.Load<Sprite>("MiniGame/Computer/anti-clockwise");
                break;
            case NodeType.MOVE_TYPE.RIGHT:
                nodeObj.GetComponent<Image>().sprite = Resources.Load<Sprite>("MiniGame/Computer/clockwise");
                break;
        }
    }

    public void RemoveNode()
    {
        node = null;
        nodeObj.SetActive(false);
    }

    public void LightUp()
    {
        gameObject.GetComponent<Image>().material.SetInt("_OutlineSize", 10);
    }

    public void LightDown()
    {
        gameObject.GetComponent<Image>().material.SetInt("_OutlineSize", 0);
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == nodeObj)
            return;

        if (collision.gameObject.GetComponent<NodeDrop>())
        {
            collision.gameObject.GetComponent<NodeDrop>().index = index;
            LightUp();
        }
            
        nodeObj.GetComponent<Image>().raycastTarget = false;
    }

    protected void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject == nodeObj)
            return;

        collision.gameObject.GetComponent<NodeDrop>().index = -1;

        LightDown();
        nodeObj.GetComponent<Image>().raycastTarget = true;
    }
}
