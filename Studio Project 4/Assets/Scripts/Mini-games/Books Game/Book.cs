﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Book  {

    //These need something? But i cant tell what
    [TextArea(1, 10)]
    public string bookName;
    //Get their sprite?
    public Sprite bookSprite;
    //Position
    public Vector3 position;
    //Desired Position;
    public Vector3 desiredPosition;
    //BookIndex
    public int bookIndex;
    //Move Index
    public int moveIndex;

    public Book(string bookName ="")
    {
        this.bookName = bookName;
    }

    public string GetBookName()
    {
        return bookName;
    }

   public static Book DeepCopy(Book b)
    {
        Book temp = new Book();
        temp.bookName = b.bookName;
        temp.bookIndex = b.bookIndex;
        temp.moveIndex = b.moveIndex;
        temp.position = b.position;
        temp.desiredPosition = b.position;

        return temp;
        
    }

}
