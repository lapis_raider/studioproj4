﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookObject : MonoBehaviour
{
    //Contains a book
    public Book book;

    //Reference to the game manager <bad way but ok>
    public BookGameManager bookGameManager;

    //Access the text and image of that object.
    private Text bookText;
    private Image bookImage;

    // Use this for initialization
    void Start()
    {
        bookText = GetComponentInChildren<Text>();
        book = new Book();
        MakeBook();
        //bookGameManager = FindObjectOfType<BookGameManager>();
        book.bookIndex = bookGameManager.index;
        book.moveIndex = book.bookIndex;    
        bookGameManager.AddBook(book);
    }

    // Update is called once per frame
    void Update()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.localPosition = book.position;

        bookImage = GetComponent<Image>();
        bookImage.sprite = Resources.Load<Sprite>("MiniGame/BookGame/book" + book.bookIndex);

        book = Book.DeepCopy(bookGameManager.bookList[book.bookIndex]);
        bookText.text = book.bookName;
        book.position = Book.DeepCopy(bookGameManager.renderList[book.bookIndex]).position;
    }

    void MakeBook()
    {
        //NameGenerator generator = FindObjectOfType<NameGenerator>();
        //book.bookName = /*bookGameManager.index.ToString(); */generator.CreateNewName(false);

        BookNames bookNames = bookGameManager.GetComponent<BookNames>();
        book.bookName = bookNames.GetBookName();

        Text bookText = GetComponentInChildren<Text>();
        bookText.text = book.bookName;

        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.localPosition = book.position;
    }


}
