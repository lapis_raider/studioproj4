﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookName
{
    //Some stats
    public string bookName = "";

    public bool assigned = false;

  public  BookName()
    {
        bookName = "";
        assigned = false;
    }

    public BookName(string bookName)
    {
        this.bookName = bookName;
        assigned = false;
    }
}

public class BookNames : MonoBehaviour {

    public List<BookName> bookList = new List<BookName>();

	// Use this for initialization
	void Awake () {

        //Slowly add names... kms
        BookName book = new BookName("After Dark");
        bookList.Add(book);
        book = new BookName("Blind Willow");
        bookList.Add(book);
        book = new BookName("Colourless");
        bookList.Add(book);
        book = new BookName("Dark Mysteries");
        bookList.Add(book);
        book = new BookName("Euphoria");
        bookList.Add(book);
        book = new BookName("Fell from Grace");
        bookList.Add(book);
        book = new BookName("Golden Pavillion");
        bookList.Add(book);
        book = new BookName("Human Desires");
        bookList.Add(book);
        book = new BookName("Indestructible");
        bookList.Add(book);
        book = new BookName("Japanese Tales");
        bookList.Add(book);
        book = new BookName("Kiseki");
        bookList.Add(book);
        book = new BookName("Little Girl");
        bookList.Add(book);
        book = new BookName("Mask Madness");
        bookList.Add(book);
        book = new BookName("Norwegian Wood");
        bookList.Add(book);
        book = new BookName("Of Importance");
        bookList.Add(book);
        book = new BookName("Pillow Book");
        bookList.Add(book);
        book = new BookName("Quiet World");
        bookList.Add(book);
        book = new BookName("Rashomon");
        bookList.Add(book);
        book = new BookName("Spring Snow");
        bookList.Add(book);
        book = new BookName("Thousand Cranes");
        bookList.Add(book);
        book = new BookName("Unanimous");
        bookList.Add(book);
        book = new BookName("Vexing Struggles");
        bookList.Add(book);
        book = new BookName("Winter Sleep");
        bookList.Add(book);
        book = new BookName("Xenophiles");
        bookList.Add(book);
        book = new BookName("Yearning Life");
        bookList.Add(book);
        book = new BookName("Zigzag Patterns");
        bookList.Add(book);
        Debug.Log("Debug: " + bookList.Count);
    }
	
    public string GetBookName()
    {
        List<string> tempArray = new List<string>();
        foreach (BookName books in bookList)
        {
            if (!books.assigned)
            {
                tempArray.Add(books.bookName);
            }
        }
        PrintList(tempArray);
        int random = Random.Range(0, tempArray.Count - 1);

        //Inefficient but I guess yolo
        foreach(BookName books in bookList)
        {
            if(books.bookName == tempArray[random])
            {
                books.assigned = true;
                break;
            }
        }

      // PrintBool();
        return tempArray[random];
    }

    void PrintBool()
    {
        string bools = "";
        foreach(BookName books in bookList)
        {
            bools += books.assigned + ",";
        }
        Debug.Log(bools);
    }

    void PrintList(List<string> stringList)
    {
        string list = "";
        foreach (string books in stringList)
        {
            list += books + ",";
        }
        Debug.Log(list);
    }
}



