﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameGenerator : MonoBehaviour
{

    //Simple name generator;
    private List<string> nameSegments;
    private List<string> subsequentNames;

    private void Awake  ()
    {
        nameSegments = new List<string>();
        nameSegments.Add("aba");
        nameSegments.Add("bac");
        nameSegments.Add("caw");
        nameSegments.Add("dew");
        nameSegments.Add("efs");
        nameSegments.Add("fga");
        nameSegments.Add("gwd");
        nameSegments.Add("hiw");
        nameSegments.Add("iaw");
        nameSegments.Add("jaw");
        nameSegments.Add("kaa");
        nameSegments.Add("lwa");
        nameSegments.Add("mca");
        nameSegments.Add("nua");
        nameSegments.Add("owa");
        nameSegments.Add("pla");
        nameSegments.Add("qsa");
        nameSegments.Add("rpe");
        nameSegments.Add("ssa");
        nameSegments.Add("tum");
        nameSegments.Add("ums");
        nameSegments.Add("voq");
        nameSegments.Add("wxq");
        nameSegments.Add("xeq");
        nameSegments.Add("yew");
        nameSegments.Add("zes");

        subsequentNames = new List<string>();
        subsequentNames.Add("the Human");
        subsequentNames.Add("the Legend");
        subsequentNames.Add("the Clown");
        subsequentNames.Add("the Mysterious");

    }

    public string CreateNewName( bool hasLastName = false)
    {
        string firstName = "";
        int numberOfSyllables = Random.Range(2, 4);
        for (int i = 0; i < numberOfSyllables; ++i)
        {
            firstName += nameSegments[Random.Range(0, nameSegments.Count - 1)];
        }
        string firstNameLetter = "";
        firstNameLetter = firstName.Substring(0, 1);
        firstName = firstName.Remove(0, 1);
        firstNameLetter = firstNameLetter.ToUpper();
        firstName = firstNameLetter + firstName;

        string currentName = firstName;
        if (hasLastName)
        {
            string lastName = "";
            numberOfSyllables = Random.Range(1, 3);
            for (int i = 0; i < numberOfSyllables; ++i)
            {
                lastName += subsequentNames[Random.Range(0, subsequentNames.Count - 1)];
            }
            string lastNameLetter = "";
            lastNameLetter = lastName.Substring(0, 1);
            lastName = lastName.Remove(0, 1);
            lastNameLetter = lastNameLetter.ToUpper();
            lastName = lastNameLetter + lastName;
            currentName = firstName + " " + lastName;
        }

        return currentName;
    }

    public string CreateRandomSyllables(int minSyllables, int maxSyllables)
    {
        string firstName = "";
        int numberOfSyllables = Random.Range(minSyllables, maxSyllables);
        for (int i = 0; i < numberOfSyllables; ++i)
        {
            firstName += nameSegments[Random.Range(0, nameSegments.Count - 1)];
        }
        string firstNameLetter = "";
        firstNameLetter = firstName.Substring(0, 1);
        firstName = firstName.Remove(0, 1);
        firstNameLetter = firstNameLetter.ToUpper();
        firstName = firstNameLetter + firstName;

        return firstName;
    }

}
