﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BookShelfAction : Interact { 

    public string sceneName;
    bool update;
    private DialogueTrigger dialogueTrigger;

    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        if (!GameDataManager.Instance.bookgame)
        {
            dialogueTrigger.TriggerDialogue();
        }
        update = true;
    }

    public override void Update()
    {
        if (!update)
            return;

        if (GameDataManager.Instance.bookgame)
        {
            gameObject.tag = "Untagged";
            return;
        }

        
        if (FindObjectOfType<DialogueManager>().HasDialogueEnded())
        {
            StartCoroutine(StartGame());
        }
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        GameDataManager.Instance.doMovement = false;
        update = false;
        StopAllCoroutines();
    }
}
