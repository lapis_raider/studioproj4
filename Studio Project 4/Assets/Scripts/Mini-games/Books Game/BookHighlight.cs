﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using UnityEngine.UI;


public class BookHighlight : MonoBehaviour , IPointerEnterHandler{

    BookObject obj;
    public Text bookText;

    void Start()
    {
        obj = GetComponent<BookObject>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        bookText.text = obj.book.bookName;
    }
}
