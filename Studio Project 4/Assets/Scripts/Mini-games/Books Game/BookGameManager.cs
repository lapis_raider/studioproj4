﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BookGameManager : MonoBehaviour {

    //The starting list and the end list
    public List<Book> bookList;

    //The renderingList
    public List<Book> renderList;

    //Bubble sorted list at start time
    public List<Book> endResultList;

    public float bookWidth;

    public int index = 0;

    private float bookMoveSpeed;

    public Vector3 startingPosition;

    public Image bookStatus;

    private Sprite bookLocked, bookUnlocked;

    public Text bookText;

    public string sceneName;

    public float debugSpeed;

    public bool isDebug = false;

    private bool wonGame = false;
    private float wonGameTimer = 0;

    public float stayTimer = 1;

    private bool playWinSound = true;

    // Use this for initialization
    void Awake() {
        //Initialize the values
        bookList = new List<Book>();
        endResultList = new List<Book>();
    }

    private void Start()
    {
        //InstantiateObjects();
        InitialisePositions();
        bookLocked = Resources.Load<Sprite>("Images/locked");
        bookUnlocked = Resources.Load<Sprite>("Images/unlocked");
        bookText.text = bookList[0].bookName;
    }

    // Update is called once per frame
    void Update()
    {
        if(wonGame)
        {
            if(playWinSound)
            {
                AudioManager.instance.Play("BookUnlocked");
                playWinSound = false;
            }
            bookStatus.sprite = bookUnlocked;
            wonGameTimer += Time.deltaTime;
            if(wonGameTimer >= stayTimer)
            {
                wonGame = false;
                wonGameTimer = 0;
                GameDataManager.Instance.bookgame = true;
                GameDataManager.Instance.doMovement = true;
                StopAllCoroutines();
                SceneManager.UnloadSceneAsync("BookGame");
            }
        }
        endResultList = BubbleSort(endResultList, true);

        //Now i check every frame but later it will be after clicks?
        if (CheckEqualString())
        {

            //won game
            wonGame = true;
            
        }
        else
        {
            bookStatus.sprite = bookLocked;
        }

        foreach(Book book in renderList)
        {
            if (Mathf.Abs(book.desiredPosition.x - book.position.x) > 1)
            {
                //Debug.Log("Lerp");
                Vector3.Lerp(book.position, book.desiredPosition, bookMoveSpeed * Time.deltaTime);
            }
        }

        if(Input.GetKeyDown(KeyCode.M))
        {
            StopAllCoroutines();
            StartCoroutine(DoBubbleSort(bookList, true));
            isDebug = true;

        }

        
    }

    IEnumerator DoBubbleSort(List<Book>theBookList, bool isAscending)
    {

        int length = theBookList.Count;
        for (int i = 1; i < length; ++i) // or int = 0; i < index - 1
        {
            bool sorted = true;

            for (int currentIndex = 0; currentIndex < length - i; ++currentIndex)
            {
                string firstName = theBookList[currentIndex].bookName;
                string secondName = theBookList[currentIndex + 1].bookName;
                if (!isAscending)
                {
                    if (firstName.CompareTo(secondName) < 0)
                    {
                        SwapBooks(theBookList, currentIndex, currentIndex + 1);
                        sorted = false;
                        yield return new WaitForSeconds(debugSpeed);
                    }
                }
                else
                {
                    if (firstName.CompareTo(secondName) > 0)
                    {
                        SwapBooks(theBookList, currentIndex, currentIndex + 1);
                        sorted = false;
                        yield return new WaitForSeconds(debugSpeed);
                    }
                }

            }
            if (sorted)
            {
                isDebug = false;
                yield break;
            }


        }


    }


    public void AddBook(Book book)
    {
        bookList.Add(Book.DeepCopy(book));
        endResultList.Add(Book.DeepCopy(book));
        renderList.Add(Book.DeepCopy(book));
        ++index;
    }

    void InitialisePositions()
    {   
        foreach(Book book in renderList)
        {
            book.position = startingPosition;
            book.desiredPosition = book.position;
            startingPosition.x += bookWidth;
        }
    }

    void PrintBookList(List <Book> theBookList)
    {
        string name = "";
        foreach(Book book in theBookList)
        {
            name += book.bookIndex + " , ";
        }
        //Debug.Log(name);
    }

    List<Book> BubbleSort(List<Book> theBookList, bool isAscending)
    {
        List<Book> tempBookList = new List<Book>(theBookList);
        int length = theBookList.Count;
        for (int i = 1; i < length; ++i) // or int = 0; i < index - 1
        {
            bool sorted = true;

            for (int currentIndex = 0; currentIndex < length - i; ++currentIndex)
            {
                string firstName = tempBookList[currentIndex].bookName;
                string secondName = tempBookList[currentIndex + 1].bookName;
                if (!isAscending)
                {
                    if (firstName.CompareTo(secondName) < 0)
                    {
                        SwapBooks(tempBookList, currentIndex, currentIndex + 1);
                        sorted = false;
                    }
                }
                else
                {
                    if (firstName.CompareTo(secondName) > 0)
                    {
                        SwapBooks(tempBookList, currentIndex, currentIndex + 1);
                        sorted = false;
                    }
                }

            }
            if (sorted)
                break;
        }

        return tempBookList;
    }

    void SwapBooks(List<Book> theBookList,int index1, int index2)
    {
        Book temp = theBookList[index1];
        theBookList[index1] = theBookList[index2];
        theBookList[index2] = temp;
        AudioManager.instance.Play("BookShuffle");
    }

    public bool CheckEqualString()
    {
        //Now we check
         if(bookList.Count != endResultList.Count)
        {
            return false;
        }
        for (int i = 0; i < bookList.Count; i++)
        {
            if (bookList[i].bookName != endResultList[i].bookName)
                return false;
        }
        return true;
    }

    public void SwapBookToEnd(BookObject bookObj)
    {
        int end = bookList.Count - 1;
        Book tempBook = new Book();
        tempBook = Book.DeepCopy(bookObj.book);
        //Debug.Log("TempBookIndex: " + tempBook.bookIndex);
        //Debug.Log(bookList.Count - 1);

        string bookObjName = tempBook.bookName;
        for (int i =tempBook.bookIndex; i < end; i++)
        {
            //Assign the bookName to the bookList for comparing;
            bookList[i].bookName = bookList[i + 1].bookName;
            //Update the rendering List
        }
        bookList[end].bookName = bookObjName;

        MoveToBack(bookObj);

        AudioManager.instance.Play("BookShuffle");
    }

    void MoveToBack(BookObject bookObject)
    {
        Book tempBook = new Book();
        tempBook = bookObject.book;

        RectTransform rectTransform = bookObject.GetComponent<RectTransform>();

        int indexToStart = tempBook.bookIndex;
        
        if (indexToStart > renderList.Count || indexToStart < 0)
        {
            return;
        }
        int size = renderList.Count - 1;

        foreach(Book book in renderList)
        {
            if (Mathf.Abs(book.desiredPosition.x - book.position.x) < 1)
            {
                book.desiredPosition.x -= bookWidth;
            }
        }

        //Then set the back element
        tempBook.desiredPosition.x = (-350 + (size * bookWidth));

        if(Mathf.Abs(tempBook.desiredPosition.x - tempBook.position.x) < 1)
        {
            rectTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);//This works
            tempBook.position = tempBook.desiredPosition;
        }
        else
        {
            //rectTransform.localScale = new Vector3(1.1f, 1.1f, 1.0f);//This works
        }

    }

    public void CloseScene()
    {
        try
        {
            SceneManager.UnloadSceneAsync(sceneName);
            GameDataManager.Instance.doMovement = true;
        }
        catch
        {
            //Debug.Log("Scene isnt load, load scene to unload scene");
        }
    }


}