﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButtonSound : MonoBehaviour {

    AudioManager am;
    public string soundName;
    
    void Start()
    {
        am = FindObjectOfType<AudioManager>();
    }
    
    public void PlaySound()
    {
        am.Play(soundName);
    }    
}
