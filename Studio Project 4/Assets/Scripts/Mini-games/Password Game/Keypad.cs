﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Keypad : MonoBehaviour {

    public TextMeshProUGUI text;
    public string sceneName;
    public PasswordConditions conditions;

    string password;
    string enteredPassword;

    void Start()
    {
        password = GameDataManager.Instance.password;
        enteredPassword = text.text = "";
    }

    public enum BUTTON_TYPE
    {
        NUMPAD0 = 0,
        NUMPAD1,
        NUMPAD2,
        NUMPAD3,
        NUMPAD4,
        NUMPAD5,
        NUMPAD6,
        NUMPAD7,
        NUMPAD8,
        NUMPAD9,
        NUMPADCANCEL,
        NUMPADENTER,
        NUMPAD_TOTAL,
    }

    public void ButtonAction(string buttonAction)
    {
        BUTTON_TYPE buttonType = BUTTON_TYPE.NUMPAD_TOTAL;
        try
        {
            buttonType = (BUTTON_TYPE)int.Parse(buttonAction);
        }
        catch
        {
            if (string.Compare(buttonAction, "C") == 0)
                buttonType = BUTTON_TYPE.NUMPADCANCEL;
            else if (string.Compare(buttonAction, "E") == 0)
                buttonType = BUTTON_TYPE.NUMPADENTER;
            else
                Debug.Log("Invalid Input");
        }

        if (buttonType != BUTTON_TYPE.NUMPADCANCEL && buttonType != BUTTON_TYPE.NUMPADENTER)
        {
            if (enteredPassword.Length >= 10 && enteredPassword.Length != 0)
                return;
        }

        switch (buttonType)
        {
            case BUTTON_TYPE.NUMPAD1:
                enteredPassword += "1";
                break;
            case BUTTON_TYPE.NUMPAD2:
                enteredPassword += "2";
                break;
            case BUTTON_TYPE.NUMPAD3:
                enteredPassword += "3";
                break;
            case BUTTON_TYPE.NUMPAD4:
                enteredPassword += "4";
                break;
            case BUTTON_TYPE.NUMPAD5:
                enteredPassword += "5";
                break;
            case BUTTON_TYPE.NUMPAD6:
                enteredPassword += "6";
                break;
            case BUTTON_TYPE.NUMPAD7:
                enteredPassword += "7";
                break;
            case BUTTON_TYPE.NUMPAD8:
                enteredPassword += "8";
                break;
            case BUTTON_TYPE.NUMPAD9:
                enteredPassword += "9";
                break;
            case BUTTON_TYPE.NUMPAD0:
                enteredPassword += "0";
                break;
            case BUTTON_TYPE.NUMPADCANCEL:
                enteredPassword = enteredPassword.Remove(enteredPassword.Length - 1);
                break;
            case BUTTON_TYPE.NUMPADENTER:
                EnterPass();
                break;
        }
        text.text = enteredPassword;
    }

    void EnterPass()
    {
        if (password.Length == enteredPassword.Length)
        {
            // if same
            if (string.Compare(password, enteredPassword) == 0)
            {
                conditions.Conditions(true);
                GameDataManager.Instance.passwordGame = true;
            }
        }
        else
            conditions.Conditions(false);
        enteredPassword = "";
    }
}
