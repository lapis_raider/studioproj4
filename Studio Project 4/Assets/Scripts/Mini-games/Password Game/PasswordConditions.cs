﻿using UnityEngine;
using UnityEngine.UI;

public class PasswordConditions : MonoBehaviour
{
    [SerializeField]
    private Sprite win;
    [SerializeField]
    private Sprite lose;

    public void Awake()
    {
        gameObject.GetComponent<Image>().enabled = false;
    }

    public void Conditions(bool winner)
    {
        if (winner)
        {
            gameObject.GetComponent<Image>().sprite = win;
            AudioManager.instance.Play("Correct");
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = lose;
            AudioManager.instance.Play("Wrong");
        }

        gameObject.GetComponent<Image>().enabled = true;

        gameObject.GetComponent<Animator>().SetBool("Win", winner);
        gameObject.GetComponent<Animator>().SetBool("StartAnimation", true);
    }

    public void FinishLose()
    {
        gameObject.GetComponent<Image>().enabled = false;
        gameObject.GetComponent<Animator>().SetBool("StartAnimation", false);
    }

    public void FinishWin()
    {
        gameObject.GetComponent<Image>().enabled = false;
        gameObject.GetComponent<Animator>().SetBool("StartAnimation", false);
    }

}
