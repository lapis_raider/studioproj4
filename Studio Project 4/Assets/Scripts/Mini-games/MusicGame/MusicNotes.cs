﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//remove the s later for musicnote(s)
public class MusicNotes : MonoBehaviour {

    //Access the manager;
    MusicGame musicGameManager;
    //Access the sound manager;
    AudioManager audioManager;
    //Do some codes here?
    //~Can remove this~
    private Text musicText;
    //The char that this note is carrying.
    public char noteChar;
    //This variable is used to check if the collider's path and this unit is path to be the same
    //for something to happen.
    [HideInInspector]
    public int pathNumber;
    //The score bar gameObject.
    [HideInInspector]
    public GameObject bookStatus;
    //The reference to the collidedObject(collider)
    [HideInInspector]
    public GameObject collidedObject;
    //Boolean to tell if the player hit a note.
    [HideInInspector]
    public bool isTapped;
    //Boolean to tell if the player missed a note.
    [HideInInspector]
    public bool isLate;
    //The rate at which the note lerps to the score bar.
    [SerializeField]
    private float stepSpeed = 5;
    //The rate at which the note travels.
    [SerializeField]
    private float noteTravelSpeed = 250;
    //This bool will be true if its in range of a collider.
    public bool isInRadius = false;
    //Starting alpha.
    private float alpha = 1;
    //When it is lerping to a certain place, the alpha decreases.
    [SerializeField]
    private float alphaFadeSpeed;
    //When the miss has occured.
    [SerializeField]
    public GameObject crossPrefab;
    //The current index
    private int currentIndex;
    //The list of waypoints.
    public List<Vector3> waypoints;
    //The note's rigidbody
    Rigidbody2D rb;
    //The note canvas groip
    CanvasGroup cg;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        cg = GetComponent<CanvasGroup>();
        musicGameManager = FindObjectOfType<MusicGame>();
        audioManager = FindObjectOfType<AudioManager>();
        musicText = GetComponentInChildren<Text>();
        musicText.text = char.ToUpper(noteChar).ToString();
        currentIndex = 0;

        //PrintWaypoints();
	}
	
	// Update is called once per frame
	void Update () {
        if (musicGameManager.isLose || musicGameManager.isWin)
        {
            Destroy(gameObject);
        }

        RectTransform canvasTransform = GameObject.FindGameObjectWithTag("Canvas").GetComponent<RectTransform>();
        Rect rect = canvasTransform.rect;
        
        //Just delete any objects that are below 50
        if(transform.position.y < -50)
        {
            gameObject.SetActive(false);
        }

        if(isTapped)
        {
            transform.position = Vector3.Lerp(transform.position, bookStatus.transform.position, stepSpeed * Time.deltaTime);
            FadeOut();
        }
        else if (isLate)
        {
            rb.gravityScale = 100;
            FadeOut();
        }
        else
        {
            Move();
        }
        //Mobile version        
        //Then do dead anim;
    }

    public void TriggerLateAnim()
    {
        isLate = true;
        //Do the dequeing here too?
        musicGameManager.Dequeue();
        ++musicGameManager.missed;

    }

    public void TriggerHitAnim()
    {
        isTapped = true;
        //Do the dequeing here too?
        musicGameManager.Dequeue();

        rb.velocity = Vector3.zero;

        switch(pathNumber)
        {
            case 0:
                audioManager.Play("GKey");
                break;
            case 1:
                audioManager.Play("FKey");
                break;
            case 2:
                audioManager.Play("EKey");
                break;
            case 3:
                audioManager.Play("DKey");
                break;
        }

        CalculateScore();
        ++musicGameManager.keysHit;
    }

    void FadeOut()
    {
        alpha = Mathf.Max(0, alpha - (alphaFadeSpeed * Time.deltaTime));
        cg.alpha = alpha;
    }

    public void CalculateScore()
    {
        if(collidedObject)
        {
            CircleCollider2D circleCollider = collidedObject.GetComponent<CircleCollider2D>();
            //0.1, 0.5, 1;
            float xDistance = transform.position.x - collidedObject.transform.position.x;

            const float PERFECT = 0.5f;
            const float GOOD = 0.8f;
            const float DECENT = 1.0f;
            //Debug.Log("Distance between two : " + xDistance);
            //Debug.Log("R"+circleCollider.radius);

            if (Mathf.Abs(xDistance) <= circleCollider.radius * PERFECT)
            {
                //Debug.Log("PERFECT");
                ++musicGameManager.perfect;
            }
            else if (Mathf.Abs(xDistance) <= circleCollider.radius * GOOD)
            {
               //Debug.Log("GOOD");
                ++musicGameManager.good;
            }
            else if (Mathf.Abs(xDistance) <= circleCollider.radius * DECENT)
            {
                //Debug.Log("DECENT");
                ++musicGameManager.decent;
            }
        }
    }

    private void Move()
    {
        if (waypoints.Count == 0)
        {
            //Debug.Log("List is empty");
            return;
        }

        if (currentIndex < waypoints.Count)
        {
            Vector3 targetPosition = waypoints[currentIndex];
            //Debug.Log("Current target: " + targetPosition.ToString());

            //Debug.Log("Position: " + transform.position.ToString());
            //transform.position = targetPosition;
           transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, noteTravelSpeed * Time.deltaTime);
            if (Vector3.Distance(transform.localPosition, targetPosition) < 0.1f)
            {
                ++currentIndex;
            }
        }
        else
        {
            //We do something?
            Vector3 target = transform.localPosition;
            target.x -= 5;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, target , noteTravelSpeed * Time.deltaTime);
        }


    }

    void PrintWaypoints()
    {
        string print = "";
        foreach(Vector3 vec in waypoints)
        {
            //print += vec;
        }

        Debug.Log(print);
    }
}
