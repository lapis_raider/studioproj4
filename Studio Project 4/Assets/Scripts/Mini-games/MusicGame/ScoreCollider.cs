﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCollider : MonoBehaviour {

    private Text scoreText;
    private MusicGame musicGameManager;
    private int count;

    float realAccuracy;

	// Use this for initialization
	void Start () {
        scoreText = GetComponent<Text>();
        musicGameManager = FindObjectOfType<MusicGame>();
        count = 0;

        realAccuracy = 100;

        string format = "";
        format += "Hit / Keys \n" + count + " / " + musicGameManager.maxSpawnCount + "\nAccuracy: " + realAccuracy + "%";
        scoreText.text = format;
    }

    void Update()
    {
        float maxCount = musicGameManager.maxSpawnCount;
        float accuracy = musicGameManager.CalculateAccuracy();
        realAccuracy = Mathf.Lerp(realAccuracy, accuracy, 5 * Time.deltaTime);
        string format = "";
        format += "Hit / Keys \n" + count + " / " + maxCount + "\nAccuracy: " + (int)realAccuracy + "%";
        scoreText.text = format;

        if(Mathf.Abs(100 - realAccuracy) < 0.5)
        {
            realAccuracy = 100;
        }

        if(musicGameManager.isLose)
        {
            count = 0;
        }

    }


    //Collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "MusicNote")
        {
            ++count;

            collision.gameObject.SetActive(false);
        }
    }


}
