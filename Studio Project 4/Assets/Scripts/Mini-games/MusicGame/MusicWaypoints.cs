﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MusicWaypoints {

    public List<Waypoint> musicWaypoints;
}
[System.Serializable]
public class Waypoint
{
    public List<Vector3> waypoints;
}
