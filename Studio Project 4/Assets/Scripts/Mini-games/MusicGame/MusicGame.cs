﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MusicGame : MonoBehaviour {

    private string sceneName = "MusicMinigame";
    //List of keycodes
    //The key queue that is used to check for the front key to press.
    Queue<char> keyQueue;
    //The key that assigns music Notes.
    Queue<char> assignQueue;
    //The queue of music Note objs.
    Queue<GameObject> musicNotes;
    //The name generator(Not even using after code changed)
    NameGenerator generator;
    //The prefab of how the music notes look.
    [SerializeField]
    private GameObject prefab;
    //The spawn Timer...
    private float spawnTimer = 0;
    //An initialisable timer between each note.
    public float timerToSpawn;
    //Speed multiplier to increase the speed of how fast each note is spawned
    public float speedMultiplier = 1;
    //The max spawn count? But this variable isnt used except debugging.
    //The index literally;
    [HideInInspector]
    public int maxSpawnCount;
    //The current amount of notes that has been spawned so far.
    private int currentSpawnCount;

    //Not used anymore
    [Header("Long string of letters for debugging")]
    [SerializeField]
    private int minSyllables;
    [SerializeField]
    private int maxSyllables;
    //~

     //The min and max keys that will be used for generation
    [Header("Amount of keys")]
    [SerializeField]
    private int minKeys;
    [SerializeField]
    private int maxKeys;

    public int keysHit;
    private int targetKeys;

    //The score board I guess?
    [Header("The lane positions")]
    [SerializeField]
    private GameObject gameStatus;

    //The accuracy counter for the game.
    [HideInInspector]
    public int missed ,decent, good, perfect;
    private float accuracy;

    //Decent 50;
    //Good 100;
    //Perfect 300;
   
      //Debug Text.
    [SerializeField]
    private Text debugText;

    //The list that stores the chars.
    [Header("Letters for generation")]
    public List<char> charList;

    //The class that allows me to store lists of waypoints.
    [Header("Waypoints for paths")]
    public MusicWaypoints waypoints;

    //Texts...
    [SerializeField]
    private Text laneText;
    [SerializeField]
    private Text laneText2;
    [SerializeField]
    private Text laneText3;
    [SerializeField]
    private Text laneText4;

    //And more texts...
    [SerializeField]
    private TextMeshProUGUI keyText1;
    [SerializeField]
    private TextMeshProUGUI keyText2;
    [SerializeField]
    private TextMeshProUGUI keyText3;
    [SerializeField]
    private TextMeshProUGUI keyText4;

    //Target keys
    [SerializeField]
    private Text targetText;

    //Conditions
    [SerializeField]
    private MusicConditions conditions;

    public bool isWin = false;
    public bool isLose = false;

    [SerializeField]
    private GameObject musicBar;
    [SerializeField]
    private GameObject mobileMusicBar;

    private void Start()
    {
        spawnTimer = 0;
        keyQueue = new Queue<char>();
        musicNotes = new Queue<GameObject>();
        
        generator = GetComponent<NameGenerator>();

        //Debugging
        //foreach (char c in generator.CreateRandomSyllables(minSyllables, maxSyllables))
        //{
        //    keyQueue.Enqueue(c);
        //}
        //Force?
        if (GameDataManager.Instance.keys != null)
        {
            charList.Add(GameDataManager.Instance.keys["MoveUp"].ToString()[0]);
            charList.Add(GameDataManager.Instance.keys["MoveDown"].ToString()[0]);
            charList.Add(GameDataManager.Instance.keys["MoveLeft"].ToString()[0]);
            charList.Add(GameDataManager.Instance.keys["MoveRight"].ToString()[0]);
        }
        else
        {
            charList.Add('W');
            charList.Add('S');
            charList.Add('A');
            charList.Add('D');
        }

        //Set the text?
        laneText.text = charList[0].ToString();
        laneText2.text = charList[1].ToString();
        laneText3.text = charList[2].ToString();
        laneText4.text = charList[3].ToString();

        keyText1.text = laneText.text;
        keyText2.text = laneText2.text;
        keyText3.text = laneText3.text;
        keyText4.text = laneText4.text;

        keyText1.outlineColor = Color.red;
        keyText2.outlineColor = Color.red;
        keyText3.outlineColor = Color.red;
        keyText4.outlineColor = Color.red;

        int numberToGenerate = Random.Range(minKeys, maxKeys);

        for(int i =0; i < numberToGenerate; ++i)
        {
            int randomChar = Random.Range(0, 4);
            keyQueue.Enqueue(charList[randomChar]);
        }

        assignQueue = new Queue<char>(keyQueue);
        PrintQueue();

           maxSpawnCount = keyQueue.Count - 1;
        accuracy = 100;

        keysHit = 0;
        float keys = (float)(maxSpawnCount * 0.8f);
        targetKeys = (int)keys;

        targetText.text = "You need to hit " + targetKeys + " keys! Keys left: " + targetKeys;

        musicBar.SetActive(true);
        mobileMusicBar.SetActive(false);

        //Mobile
#if UNITY_ANDROID
            musicBar.SetActive(false);
            mobileMusicBar.SetActive(true);
#endif


    }

    private void Update()
    {
        //Debug.Log("Missed: " + missed);
        if (isLose)
        {
            #if UNITY_ANDROID
                     ResetScene();
            #endif
            targetText.text = "Press Enter to start again!";
            if (Input.GetKeyDown(KeyCode.Return))
            {
                ResetScene(); 
            }
        }
        if (!isWin)
        {
            if (CheckWin())
            {
                GameDataManager.Instance.musicGame = true;
                conditions.Conditions(true);
                isWin = true;
            }
        }
        if (!isLose)
        {
            if (CheckLose())
            {
                //Do lose scene
                conditions.Conditions(false);
                isLose = true;
            }
        }

        if(conditions.done)
        {
            CloseScene();
            return; // idk 
        }

        if (isWin || isLose)
            return;

        keyText1.outlineWidth = 0f;
        keyText2.outlineWidth = 0f;
        keyText3.outlineWidth = 0f;
        keyText4.outlineWidth = 0f;

        //Update the text
        targetText.text = "You need to hit " + targetKeys + " keys! Keys left: " + (targetKeys - keysHit).ToString();

        if (keyQueue.Count > 0)
        {
            //Check the input from the player? 
            foreach (char c in Input.inputString)
            {
                const float outLine = 0.75f;
                //Optimisation?????????
                if (keyText1.text.Equals(c.ToString()) || keyText1.text.Equals(char.ToUpper(c).ToString()))
                {
                    keyText1.outlineWidth = outLine;
                }
                else if (keyText2.text.Equals(c.ToString()) || keyText2.text.Equals(char.ToUpper(c).ToString()))
                {
                    keyText2.outlineWidth = outLine;
                }
                else if (keyText3.text.Equals(c.ToString()) || keyText3.text.Equals(char.ToUpper(c).ToString()))
                {
                    keyText3.outlineWidth = outLine;
                }
                else if (keyText4.text.Equals(c.ToString()) || keyText4.text.Equals(char.ToUpper(c).ToString()))
                {
                    keyText4.outlineWidth = outLine;
                }

                //Check if the char is equal to the first of queue
                //Hacky method                 
                if (musicNotes.Count > 0)
                {
                    MusicNotes musicNote = musicNotes.Peek().GetComponent<MusicNotes>();
                    char musicChar = musicNote.noteChar;
                    if (musicNote.isInRadius)
                    {
                        if (c.Equals(musicChar) || char.ToUpper(c).Equals(musicChar))
                        {
                            //Assign the status to game status
                            //Debug.Log("hit");
                            musicNote.bookStatus = gameStatus;
                            musicNote.TriggerHitAnim();
                            break;
                        }
                    }
                }
            }
            // PrintQueue();
        }
        else
        {
         //   Debug.Log("Empty queue");
            //Win con?
        }

        //Spawn stuff?
        if (currentSpawnCount < maxSpawnCount)
        {
            spawnTimer += speedMultiplier * Time.deltaTime;
            if (spawnTimer >= timerToSpawn)
            {
                spawnTimer = 0;
                GenerateObject();
                ++currentSpawnCount;
                //Debug.Log("Current: " + currentSpawnCount + "Max: " + maxSpawnCount);
            }
        }
    }

    private void PrintQueue()
    {
        string temp = "The queue in order: ";
        foreach (char c in keyQueue)
        {   
            temp += c;
        }

        //Debug.Log(temp);
    }
    private void PrintMusicNotesQueue()
    {
        string temp = "The queue in order: ";
        foreach (GameObject c in musicNotes)
        {
            temp += c.GetComponent<MusicNotes>().noteChar + ",";
        }

        //Debug.Log(temp);
    }

    public char DequeueQueue()
    {
        return keyQueue.Dequeue();
    }

    public GameObject DequeueMusicQueue()
    {
       return musicNotes.Dequeue();
    }

    public void Dequeue()
    {
        DequeueQueue();
        DequeueMusicQueue();
    }

    private void GenerateObject()
    {
        //Instantiate the prefab
        GameObject obj = Instantiate(prefab);
        //Put it under canvas
        obj.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
        //Get the canvas' rect
        RectTransform canvasTransform = GameObject.FindGameObjectWithTag("Canvas").GetComponent<RectTransform>();

        //Set pos
        float xPos, yPos;
        xPos = yPos = 0;
        Rect rect = canvasTransform.rect;
        xPos = rect.width + (rect.width * 0.01f);

        int path = -1;
        MusicNotes musicNote = obj.GetComponent<MusicNotes>();
        //Try to access the music notes variable
        musicNote.noteChar = assignQueue.Dequeue();
        
        for(int i =0; i < charList.Count;++i)
        {
            if (musicNote.noteChar.Equals(charList[i]))
            {
                path = i;
                break;
            }

        }

        // path = Random.Range(0, waypoints.musicWaypoints.Count);
        yPos = waypoints.musicWaypoints[path].waypoints[0].y;
        obj.transform.localPosition = new Vector3(xPos, yPos,1);
        obj.transform.localScale = new Vector3(1.25f, 1.25f, 1f);

        obj.GetComponent<MusicNotes>().pathNumber = path;

        //Rigidbody 2D for velocity and gravity
       // Rigidbody2D rigidbody = obj.GetComponent<Rigidbody2D>();
     //  rigidbody.velocity = new Vector3(-150, 0, 0);

        musicNote.waypoints = new List<Vector3>(waypoints.musicWaypoints[path].waypoints);
        //Queue that note
        musicNotes.Enqueue(obj);
    }

    public float CalculateAccuracy()
    {
        if (decent > 0 || good > 0 || perfect > 0)
        {
            accuracy = (float)((decent * 50) + (good * 100) + (perfect * 300)) /
               (float)((missed + decent + good + perfect) * 300) * 100f;
        }   

        return accuracy;

    }

    private bool CheckWin()
    {
        return ((targetKeys - keysHit) <= 0);
    }

    private bool CheckLose()
    {
        return (missed > (maxSpawnCount - targetKeys));
    }


    public void CloseScene()
    {
        try
        {
            SceneManager.UnloadSceneAsync(sceneName);
            GameDataManager.Instance.doMovement = true;
        }
        catch
        {
           // Debug.Log("Scene isnt load, load scene to unload scene");
        }
    }

    void ResetScene()
    {               
        //Clear all the stuff
        keyQueue.Clear();
        assignQueue.Clear();
        musicNotes.Clear(); 
        isLose = false;
        isWin = false;
        int numberToGenerate = Random.Range(minKeys, maxKeys);

        for (int i = 0; i < numberToGenerate; ++i)
        {
            int randomChar = Random.Range(0, 4);
            keyQueue.Enqueue(charList[randomChar]);
        }

        assignQueue = new Queue<char>(keyQueue);
        PrintQueue();

        maxSpawnCount = keyQueue.Count - 1;
        accuracy = 100;

        keysHit = 0;
        float keys = (float)(maxSpawnCount * 0.8f);
        targetKeys = (int)keys;

        targetText.text = "You need to hit " + targetKeys + " keys! Keys left: " + targetKeys;

        currentSpawnCount = 0;
        decent = 0;
        good = 0;
        perfect = 0;
        missed = 0;

    }

    public void OnClickButton(int pathNumber)
    {
        //Only want this function to work on the mobile
        // #if UNITY_ANDROID
        float outLine = 0.75f;

        switch(pathNumber)
        {
            case 0:
                keyText1.outlineWidth = outLine;
                break;
            case 1:
                keyText2.outlineWidth = outLine;
                break;
            case 2:
                keyText3.outlineWidth = outLine;
                break;
            case 3:
                keyText4.outlineWidth = outLine;
                break;
                
        }

        if (musicNotes.Count > 0)
        {
            MusicNotes musicNote = musicNotes.Peek().GetComponent<MusicNotes>();
            if (musicNote.isInRadius)
            {
                if (pathNumber.Equals(musicNote.pathNumber))
                {
                    //Assign the status to game status
                    musicNote.bookStatus = gameStatus;
                    musicNote.TriggerHitAnim();
                }
            }
        }
    
    //#endif

    }
}
