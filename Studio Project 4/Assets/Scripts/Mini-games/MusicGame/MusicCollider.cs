﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicCollider : MonoBehaviour {

    //May want to not hardcode
    [SerializeField]
    private int laneNumber;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "MusicNote")
        {
            MusicNotes musicNote = collision.gameObject.GetComponent<MusicNotes>();
            if (musicNote.pathNumber == laneNumber)
            {
                musicNote.isInRadius = true;
                musicNote.collidedObject = gameObject;
                
                collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(-50,0);
            }

            
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        MusicNotes musicNote = collision.gameObject.GetComponent<MusicNotes>();

        //Ignore the collision?
        if (musicNote.pathNumber == laneNumber)
        {
            musicNote.isInRadius = true;
            musicNote.collidedObject = gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "MusicNote")
        {
            MusicNotes musicNote = collision.gameObject.GetComponent<MusicNotes>();
            if (musicNote.pathNumber == laneNumber)
            {
                if (!musicNote.isTapped)
                {
                    musicNote.TriggerLateAnim();
                    
                }
                musicNote.isInRadius = false;
                musicNote.collidedObject = null;
            }
        }
    }
}
