﻿using UnityEngine;

public class Interact : MonoBehaviour {

    // Use this for initialization
    public float interactRadius = 3f; //default

    bool selected = false;
    protected bool doneInteraction = false; //make sure interaction is only done once
    Transform player;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, interactRadius);
    }

    virtual public void isSelected(Transform _player)
    {
        selected = true;
        doneInteraction = false;
        player = _player;
    }

    virtual public void isUnselected()
    {
        selected = false;
        doneInteraction = false;
        player = null;
    }

    virtual public void InteractResponse()
    {
        //Debug.Log("Interacted");
    }

    // Update is called once per frame
    virtual public void Update () {

        if (selected && !doneInteraction)
        {
            //do action
            InteractResponse();
            doneInteraction = true;
        }
	}
}
