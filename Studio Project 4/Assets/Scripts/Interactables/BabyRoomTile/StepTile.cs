﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepTile : MonoBehaviour {

    DialogueTrigger[] dialogueTrigger;
    static DialogueManager dialogueManager;
    static int currentDialogue = 0;
    static bool firstStep;
    //static int tileTriggerID = -1;
    public int tileID = -1;
	// Use this for initialization
	void Awake () {
        dialogueTrigger = GetComponents<DialogueTrigger>();
        firstStep = false;
        dialogueManager = FindObjectOfType<DialogueManager>();
    }
	
	// Update is called once per frame
	void Update () {
        if (firstStep && currentDialogue < dialogueTrigger.Length)
        {
            if (dialogueManager.HasDialogueEnded())
            {
                    dialogueTrigger[currentDialogue++].TriggerDialogue();
            }
            if (currentDialogue == 2)
            {
                GameDataManager.Instance.doMovement = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Step Tile trigger enter called");
        if (collision.CompareTag("Player"))
        {
            if (Random.Range(0, 2) == 0)
            {
                AudioManager.instance.Play("Squeak1");
            }
            else
            {
                AudioManager.instance.Play("Squeak2");
            }

            if (!firstStep)
            {
                //dialogueTrigger[currentDialogue].TriggerDialogue();
                firstStep = true;
                GameDataManager.Instance.doMovement = false;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
            else
            {
                GameObject go = GameObject.FindGameObjectWithTag("Baby");
                if (go != null)
                {
                    go.GetComponent<BabyInfo>().SM.SetNextState(ref go, "crying");
                }
                //else
                    //Debug.Log("Cant Find Baby");
            }
        }
    }
}
