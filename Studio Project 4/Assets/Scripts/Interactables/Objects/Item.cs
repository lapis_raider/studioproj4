﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Item", menuName ="Inventory/Item")]
public class Item : ScriptableObject {

    // Use this for initialization
    new public string name = "New item";
    public Sprite icon = null;
    public bool addToInventory = true;
    public string infomation = "";
    public PlayerData.CLUES type;
}
