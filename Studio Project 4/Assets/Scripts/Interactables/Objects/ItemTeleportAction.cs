﻿using System.Collections;
using UnityEngine;

public class ItemTeleportAction : ItemAction
{
    override public void Behaviour()
    {
        if (collided)
        {
            StartCoroutine(ParticleEffect());

            beingUsed = false;
        }
    }

    IEnumerator ParticleEffect()
    {
        GameObject player = GameObject.FindWithTag("Player");

        ParticleSystem particle = player.transform.Find("teleportIn").gameObject.GetComponent<ParticleSystem>();
        if (particle)
        {
            particle.Play();
            yield return new WaitForSeconds(0.2f);
        }

        player.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, player.transform.position.z);
        AudioManager.instance.Play("Teleport");

        particle = player.transform.Find("teleportOut").gameObject.GetComponent<ParticleSystem>();
        if (particle)
            particle.Play();

    }
}