﻿using UnityEngine;

public class ItemAction : MonoBehaviour {

    // Use this for initialization
    protected bool beingUsed;
    protected Vector3 direction = new Vector3(0,1,0);
	
    float currentSpeed;
    public float initialSpeed = 1.0f; //initial
    
    public float acceleration = -10.0f;
  
    protected float timer = 0.0f;
    protected bool collided = false;
    protected Rigidbody2D rigidbdy;

    float destroyTimer = 5.0f;
    float destroyTimeCurr = 0.0f;

    void Start()
    {
        rigidbdy = GetComponent<Rigidbody2D>();
        rigidbdy.gravityScale = 0;

        currentSpeed = initialSpeed;
        timer = 0.0f;
    }


    virtual public void Update () {
        //throw on the ground, behviour
        if (beingUsed)
        {
            if (!collided) //still being thrown if havent collided onto floor or wall
                Thrown();
            else
            {
                rigidbdy.velocity = new Vector2(0, 0);
            }

            Behaviour();
        }
        else
        {
            destroyTimeCurr += Time.deltaTime;

            if (destroyTimeCurr >= destroyTimer)
                Destroy(gameObject);
        }
    }

    virtual public void Thrown()
    {
        //fly a certain distance
        timer += Time.deltaTime;
        currentSpeed = initialSpeed + acceleration * timer;
        rigidbdy.velocity = direction * currentSpeed;

        if (currentSpeed <= 0) //when speed is too low, stop it
            collided = true;
    }

    virtual public void Behaviour()
    {
        Debug.Log("Being used");
    }

    virtual public void BeingUsed(Vector3 dir) //for being thrown
    {
        if (!dir.Equals(Vector3.zero))
            direction = dir;

        beingUsed = true;
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Walls"))
        {
            currentSpeed = initialSpeed = rigidbdy.velocity.magnitude;
            direction = rigidbdy.velocity.normalized;
            timer = 0.0f;
        }
        else
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
    }
}
