﻿using UnityEngine;

public class FoundKey : Interact
{
    private DialogueTrigger dialogueTrigger;
    public OpenDoor[] openDoor;
    DialogueManager dialogueManager;

    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
        dialogueManager = FindObjectOfType<DialogueManager>();
    }

    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        AudioManager.instance.Play("SlidingWood");
        GameDataManager.Instance.doMovement = false;
        dialogueTrigger.TriggerDialogue();
        for (int i = 0; i < openDoor.Length; ++i)
        {
            openDoor[i].Open();
        }
        gameObject.tag = "Untagged";
        
    }

    public override void Update()
    {
        // blank
    }
}
