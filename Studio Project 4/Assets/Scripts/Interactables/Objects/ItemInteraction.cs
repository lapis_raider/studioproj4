﻿
public class ItemInteraction : Interact {

    public Item item;
    private DialogueTrigger dialogue;

    public void Start()
    {
        dialogue = GetComponent<DialogueTrigger>();
    }

    public override void InteractResponse()
    {
        PickUp();
    }

    void PickUp()
    {
        GameDataManager.Instance.playerData.inventoryData.Add(item);
        dialogue.TriggerDialogue();

        Destroy(gameObject);
    }
}
