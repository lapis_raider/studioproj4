﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/UsableItem")]
public class ItemUsable : ScriptableObject
{
    // Use this for initialization
    new public string name = "New item";
    public bool reusable = false;

    public float coolDownTimer = 0.0f;

    [HideInInspector]
    public float currTimer = 0.0f;
    [HideInInspector]
    public bool startCoolDown = false;

    public GameObject prefab;

    //New variables, change if u wish to.
    public Sprite prefabUIImage;
    //The index for the item.
    public Sprite prefabUIIndex;
}
