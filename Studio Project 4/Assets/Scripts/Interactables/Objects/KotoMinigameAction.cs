﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class KotoMinigameAction : Interact
{
    public string sceneName;
    bool update = false;
    private DialogueTrigger dialogueTrigger;

    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        if (!GameDataManager.Instance.playerData.clues[(int)PlayerData.CLUES.MUSICSHEET_1]
       || !GameDataManager.Instance.playerData.clues[(int)PlayerData.CLUES.MUSICSHEET_2]
       || !GameDataManager.Instance.playerData.clues[(int)PlayerData.CLUES.MUSICSHEET_3]
       || !GameDataManager.Instance.playerData.clues[(int)PlayerData.CLUES.MUSICSHEET_4])
        {
            dialogueTrigger.TriggerDialogue();
        }
        else
            update = true;

    }

    public override void Update()
    {
        if (!update)
            return;

        if (FindObjectOfType<DialogueManager>().HasDialogueEnded())
        {
            StartCoroutine(StartGame());
        }
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(0.5f);

        GameDataManager.Instance.doMovement = false;

        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        update = false;
        StopAllCoroutines();
    }
}
