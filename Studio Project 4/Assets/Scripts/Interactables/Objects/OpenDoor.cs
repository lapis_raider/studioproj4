﻿using UnityEngine;

public class OpenDoor : MonoBehaviour {
    
    // Have opened and closed based on in case we need to close it for some reason
    public Sprite OpenedDoor;
    public Sprite ClosedDoor;
    public GameObject wall;
    
    bool opened;

    void Start()
    {
        opened = false;
    }

    public void Open()
    {
        opened = true;
        if (opened)
        {
            GetComponent<SpriteRenderer>().sprite = OpenedDoor;
            wall.SetActive(false);
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = ClosedDoor;
            wall.SetActive(true);
        }
    }
}
