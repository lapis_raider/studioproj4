﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TrapDoorAction : Interact
{
    bool update;
    DialogueTrigger dialogueTrigger;
    public GameObject TrapDoor;
    public GameObject TriggerBox;

    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        dialogueTrigger.TriggerDialogue();
        update = true;
    }

    public override void Update()
    {
        if (!update)
            return;

        if (FindObjectOfType<DialogueManager>().HasDialogueEnded())
        {
            StartCoroutine(OpenDoor());
        }
    }

    IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(0.5f);

        TrapDoor.SetActive(true);
        TriggerBox.SetActive(true);
        AudioManager.instance.Play("TrapDoor");
        Destroy(gameObject);

        update = false;
        StopAllCoroutines();
    }
}
