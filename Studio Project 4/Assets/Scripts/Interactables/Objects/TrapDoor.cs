﻿using UnityEngine;

public class TrapDoor : MonoBehaviour {

    public GameObject unlockedDoor;

	// Update is called once per frame
	void Update () {
        for (int i = 0; i < GameDataManager.Instance.playerData.clues.Length; ++i)
        {
            //if (i == (int)PlayerData.CLUES.JOURNAL)
            //    continue;
            //if (i == (int)PlayerData.CLUES.ARM)
            //    continue;

            if (i == (int)PlayerData.CLUES.FAMILY)
                continue;

            if (!GameDataManager.Instance.playerData.clues[i])
                return;
        }

        unlockedDoor.SetActive(true);
        Destroy(gameObject);
	}
}
