﻿using UnityEngine;
using UnityEngine.AI;
public class ItemRockAction : ItemAction
{
    public float distractionTime = 10;
    public float rangeDistract = 20;
    float TimeLapsed = 0;
    bool PositionSet = false;
    Vector2 pos;
    override public void Behaviour()
    {
        if(collided)
        {
            if (PositionSet == false)
            {
                pos = rigidbdy.position;
                GameObject[] Butlers = GameObject.FindGameObjectsWithTag("Butler");
                for (int i = 0; i < Butlers.Length; ++i)
                {
                    if ((Butlers[i].transform.position - transform.position).magnitude > rangeDistract)
                        continue;

                    Butlers[i].GetComponent<ButlerInfo>().GetQueueDest.Add(pos);
                }
                
                PositionSet = true;
                AudioManager.instance.Play("Rock");
            }
            else
            {
                if(TimeLapsed < distractionTime)
                    TimeLapsed += Time.deltaTime;
                if (TimeLapsed >= distractionTime)
                {
                    GameObject[] Butlers = GameObject.FindGameObjectsWithTag("Butler");
                    for (int i = 0; i < Butlers.Length; ++i)
                    {
                       
                        Butlers[i].GetComponent<ButlerInfo>().GetQueueDest.Remove(pos);
                 
                        if(Butlers[i].GetComponent<ButlerInfo>().GetQueueDest.Count == 0)
                        {
                            int closestNodeID = 0;
                            float distance = 100000;
                            for (int j = 0; j < Butlers[i].GetComponent<ButlerInfo>().GetNumOfPatrolPathNode; ++j)
                            {
                                float currDistance = (Butlers[i].GetComponent<Rigidbody2D>().position - Butlers[i].GetComponent<ButlerInfo>().GetPathNodePos(j)).magnitude;
                                if (currDistance < distance)
                                {
                                    distance = currDistance;
                                    closestNodeID = j;
                                }
                            }
                            Butlers[i].GetComponent<ButlerInfo>().CurrentNavID = closestNodeID;
                            Butlers[i].GetComponent<NavMeshAgent>().SetDestination(Butlers[i].GetComponent<ButlerInfo>().GetPathNodePos(closestNodeID));
                        }
                    }
                    beingUsed = false;
                    Destroy(gameObject);
                }
            }
        }
    }
}
