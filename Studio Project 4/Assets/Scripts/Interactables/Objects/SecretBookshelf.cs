﻿using UnityEngine;

public class SecretBookshelf : MonoBehaviour {

    Animator anim;
    bool once = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void LateUpdate () {

        if (once)
            Destroy(this);
       
		if (GameDataManager.Instance.bookgame)
        {
            if (anim)
            {
                AudioManager.instance.Play("SlidingWood");
                anim.Play("MoveBookshelf");
            }
            else
                Debug.Log("No controller attatched");

            once = true;
        }
	}
}
