﻿using UnityEngine;

public class OpenItem : MonoBehaviour {
    
    public GameObject itemOpen;
    public GameObject clue;    
    public PlayerData.CLUES itemType;

	// Update is called once per frame
	void Update () {
        switch (itemType)
        {
            case PlayerData.CLUES.MEMENTO:
                if (GameDataManager.Instance.musicGame)
                {   
                    itemOpen.SetActive(true);
                    clue.SetActive(true);
                    Destroy(gameObject);
                }
                break;
            case PlayerData.CLUES.WEAPON:
                if (GameDataManager.Instance.passwordGame)
                {
                    itemOpen.SetActive(true);
                    clue.SetActive(true);
                    AudioManager.instance.Play("OpenSafe");
                    Destroy(gameObject);
                }
                break;
            default:
                Debug.Log("Invalid item type. Valid item types are MEMENTO, WEAPON");
                break;
        }
	}
}
