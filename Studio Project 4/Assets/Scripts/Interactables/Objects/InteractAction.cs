﻿using UnityEngine;

public class InteractAction : Interact {
    
    protected DialogueTrigger dialogueTrigger;

    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        dialogueTrigger.TriggerDialogue();  
    }

    public override void Update()
    {
        // blank
    }
}
