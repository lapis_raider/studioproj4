﻿using UnityEngine;

public class InteractFridge : InteractAction
{
    public GameObject cheese;
    public GameObject openFridge;

    public override void InteractResponse()
    {
        dialogueTrigger.TriggerDialogue();

        openFridge.SetActive(true);
        cheese.SetActive(true);
        AudioManager.instance.Play("Fridge");

        Destroy(gameObject);
    }
}
