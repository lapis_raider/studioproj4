﻿using UnityEngine;

public class HidingSpot : Interact {

    private DialogueTrigger dialogue;
    static bool firstHide = false;
    static DialogueTrigger trigger = null;
    private void Awake()
    {
        if (trigger == null)
            trigger = GetComponent<DialogueTrigger>();
    }
    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        if(!firstHide)
        {
            trigger.TriggerDialogue();
            Destroy(trigger);
            firstHide = true;
            trigger = null;
        }

        GameObject player = GameObject.FindWithTag("Player");
        player.GetComponent<PlayerMovement>().hiding = !player.GetComponent<PlayerMovement>().hiding;

        if (player.GetComponent<PlayerMovement>().hiding)
        {
            player.transform.localPosition += new Vector3(0, 0, 100);
            player.GetComponent<BoxCollider2D>().enabled = false;
            GameDataManager.Instance.doMovement = false;
            AudioManager.instance.Play("ClothesShuffle");
        }
        else
        {
            player.transform.localPosition += new Vector3(0, 0, -100);
            player.GetComponent<BoxCollider2D>().enabled = true;
            GameDataManager.Instance.doMovement = true;
            AudioManager.instance.Play("ClothesShuffle");
        }
    }

    public override void Update()
    {
        // blank
    }
}
