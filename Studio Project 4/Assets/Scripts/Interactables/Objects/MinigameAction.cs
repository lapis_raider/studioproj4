﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MinigameAction : Interact
{
    public string sceneName;
    bool update;    
    private DialogueTrigger dialogueTrigger;

    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    public override void isSelected(Transform _player)
    {
        InteractResponse();
    }

    public override void isUnselected()
    {
    }

    public override void InteractResponse()
    {
        dialogueTrigger.TriggerDialogue();
        update = true;
    }

    public override void Update()
    {
        if (!update)
            return; 

        if (FindObjectOfType<DialogueManager>().HasDialogueEnded())
        {
            StartCoroutine(StartGame());
        }
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(0.5f);

        GameDataManager.Instance.doMovement = false;

        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        update = false;
        StopAllCoroutines();
    }
}
