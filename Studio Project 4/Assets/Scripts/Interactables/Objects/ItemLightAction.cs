﻿using UnityEngine;

public class ItemLightAction : ItemAction {

    public float lightIntensity;
    public int lightRadius;
    public float killRadius;

    public Light itemLight;

    float timerLight = 0.0f;
    float timerRadius = 0.0f;

    public Sprite openLight;
    public GameObject particles;
    bool exploded = false;

    override public void Behaviour()
    {
        if (itemLight)
        {
            //change the intensity and stuff
            if (collided) //once collided with something change radius and intensity
            {
                if (!exploded)
                {
                    gameObject.GetComponent<SpriteRenderer>().sprite = openLight;
                    particles.GetComponent<ParticleSystem>().Play();
                    exploded = true;
                    AudioManager.instance.Play("Light");
                }

                timerLight += Time.deltaTime;
                timerRadius += Time.deltaTime;

                itemLight.range = Mathf.Lerp(itemLight.range, lightRadius, timerRadius);
                itemLight.intensity = Mathf.Lerp(itemLight.intensity, lightIntensity, timerLight);

                if (timerRadius > 1.0f && timerLight > 1.0f)
                {
                    KillEnemies(); //kill nearby enemies when it finishes exploding
                    beingUsed = false;
                }
            }    
        } 
    }


    void KillEnemies()
    {
        GameObject[] enemyList = GameObject.FindGameObjectsWithTag("Butler");

        for (int i = 0; i < enemyList.Length; ++i)
        {
            if (Vector3.Magnitude(transform.position - enemyList[i].transform.position) <= killRadius)
            {
                enemyList[i].GetComponent<ButlerInfo>().SM.SetNextState(ref enemyList[i], "disappear");
            }
        }
    }
}
