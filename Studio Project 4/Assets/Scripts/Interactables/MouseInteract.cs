﻿using UnityEngine;
using DG.Tweening;

public class MouseInteract : Interact
{
    public GameObject objTaken;
    public GameObject newParent;
    public Sprite bone;
    private DialogueTrigger dialogue;

    public void Start()
    {
        dialogue = GetComponent<DialogueTrigger>();
    }

    public override void InteractResponse()
    {
        if (gameObject.GetComponent<MouseInfo>().hiding || gameObject.GetComponent<MouseInfo>().takeCheese) //if mouse hiding, cant interact with it
            return;

        if (GameDataManager.Instance.gotCheese) //if player has gotten the cheese
        {
            GameObject mouse = gameObject;
            gameObject.GetComponent<MouseInfo>().SM.SetNextState(ref mouse, "giveCheese");
            gameObject.GetComponent<MouseInfo>().takeCheese = true;

            //take cheese away from player inventory
            for (int i =0; i < GameDataManager.Instance.playerData.inventoryData.items.Count; ++i)
            {
                if (GameDataManager.Instance.playerData.inventoryData.items[i].name == "Cheese")
                {
                    GameDataManager.Instance.playerData.inventoryData.Remove(GameDataManager.Instance.playerData.inventoryData.items[i]);
                    break;
                }
            }
            
            objTaken.SetActive(true);
        }
        else
        {
            //mouse run away state
            GameObject mouse = gameObject;
            gameObject.GetComponent<MouseInfo>().SM.SetNextState(ref mouse, "runAway");
            dialogue.TriggerDialogue();
        }

        AudioManager.instance.Play("Mouse");
    }

    public void SetOjActive(bool active)
    {
        objTaken.SetActive(active);
    }

    public void ChangeSprite()
    {
        objTaken.GetComponent<SpriteRenderer>().sprite = bone;
    }

    public void ChangeParent()
    {
        objTaken.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
        objTaken.transform.SetParent(newParent.transform); //place the bone on the floor for the player
    }
}
