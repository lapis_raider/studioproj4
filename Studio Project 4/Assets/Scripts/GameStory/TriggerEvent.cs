﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour {
    DialogueTrigger[] dialogue;
    DialogueManager dialogueManager;
    public bool DestroyOnTrigger = false;
    bool hasTriggered = false;
    int currentDialogue= 0;
    public bool DestroyOnEnd = true;
    private void Awake()
    {

        dialogue = GetComponents<DialogueTrigger>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        hasTriggered = true;
        dialogueManager = FindObjectOfType<DialogueManager>();
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("player entered collider");
            dialogue[0].TriggerDialogue();
            GameDataManager.Instance.doMovement = false;
        }
        if (DestroyOnTrigger)
            Destroy(gameObject);
    }

    private void Update()
    {
        if(hasTriggered)
        {
            if(dialogueManager.HasDialogueEnded())
            {
                if (++currentDialogue < dialogue.Length)
                    dialogue[currentDialogue].TriggerDialogue();
                else if (DestroyOnEnd)
                {
                    GameDataManager.Instance.doMovement = true;
                    Destroy(gameObject);
                }
                else
                    GameDataManager.Instance.doMovement = true;

            }
        }
    }
}
