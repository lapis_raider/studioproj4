﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstSeenEnemy : MonoBehaviour {

    DialogueTrigger[] dialogue;
    int currentDialogue = 0;
    DialogueManager dialogueManager;
    SpriteRenderer spriteRenderer;
    CameraFollow cam;
    GameObject player;
    public float maxSeenTime = 2;
    public float cameraZSize = 2.5f;
    public float followTime = 2;
    public float rayRange = 20;
    bool hasBeenSeen = false;
    bool isFollowing = false;
    bool startCutscene = false;
    Vector2 dir = Vector2.zero;

    GameObject fog;
    
	// Use this for initialization
	void Awake () {
        dialogue = GetComponents<DialogueTrigger>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        cam = Camera.main.GetComponent<CameraFollow>();
        dialogueManager = FindObjectOfType<DialogueManager>();
        player = GetComponent<BaseEnemyInfo>().GetPlayer;
        if (player != null)
            dir = -(transform.position - player.transform.position).normalized;
    }
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            player = GetComponent<BaseEnemyInfo>().GetPlayer;
            dir = (player.transform.position - transform.position).normalized;
        }
        if (hasBeenSeen && !isFollowing)
        {
            if (dialogueManager.HasDialogueEnded())
            {
                if (++currentDialogue < dialogue.Length)
                    dialogue[currentDialogue].TriggerDialogue();
                else
                {
                    GameDataManager.Instance.doMovement = true;
                    BigmanInfo.BigManStop = false;
                    ButlerInfo.butlerStop = false;
                    MouseInfo.mouseStop = false;
                    Destroy(this);
                }
            }
        }
        else if (spriteRenderer.isVisible)
        {

            if (!startCutscene)
            {
                dir = (player.transform.position - transform.position).normalized;
                RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position,dir , rayRange);
                for (int i = 0; i < hits.Length; ++i)
                {
                    if (hits[i].collider.CompareTag("Player"))
                    {
                        startCutscene = true;
                        break;
                    }
                }
            }

            maxSeenTime -= Time.deltaTime;
            if (startCutscene)
            {
                if (maxSeenTime <= 0)
                {
                    if (isFollowing)
                    {
                       
                        cam.objectFollow = null;
                        isFollowing = false;
                        dialogue[0].TriggerDialogue();
                        cam.followingObject = false;
                        if (fog)
                            fog.SetActive(true);
                    }
                    else
                    {
                        fog = GameObject.Find("FogPlane");
                        if (fog)
                            fog.SetActive(false);
                        cam.objectFollow = gameObject;
                        cam.objectZSize = cameraZSize;
                        cam.followingObject = true;
                        maxSeenTime = followTime;
                        isFollowing = true;
                        hasBeenSeen = true;
                        GameDataManager.Instance.doMovement = false;
                        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        BigmanInfo.BigManStop = true;
                        ButlerInfo.butlerStop = true;
                        MouseInfo.mouseStop = true;

                    }
                }
            }
        }
	}
}
