﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class spawnDummyButler : MonoBehaviour {

    
    public GameObject dummyButler;
    public GameObject spawnButler;
    public GameObject spawnWoman;
    public GameObject sbigMan;
    public Vector3 spawnPos;
    public Vector3 butlerSpawnPos2;
    public Vector3[] targets;
    int currentTarget = 0;
    SpriteRenderer spriteRenderer;
    Sprite[] sprites;
    NavMeshAgent agent;
    CameraFollow cam;
    DialogueTrigger[] dialogue;
    int currentDialogue = 0;
    DialogueManager dialogueManager = null;
    bool triggeredDialogue = false;
    bool butlerSpeech = false;
    bool spawned = false;

    public AnimationClip[] animationClips;
    Animator animator;
    AnimatorOverrideController animatorOverrideController;
    Vector3 lastPosition;
    Vector3 speed = Vector3.zero;
    // Use this for initialization
    void Awake () {
        sprites = Resources.LoadAll<Sprite>("Enemies/BigMan/BigManSpriteSheet");
        cam = Camera.main.GetComponent<CameraFollow>();
        dialogue = GetComponents<DialogueTrigger>();
        dialogueManager = FindObjectOfType<DialogueManager>();

        animator = dummyButler.GetComponent<Animator>();
        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;
        lastPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (!spawned)
            return;
        dummyButler.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
        if (dialogueManager == null)
            dialogueManager = FindObjectOfType<DialogueManager>();
        if (triggeredDialogue)
        {
            ButlerInfo.butlerStop = true;
            BigmanInfo.BigManStop = true;
            MouseInfo.mouseStop = true;
            if (dialogueManager.HasDialogueEnded())
            {
                if (++currentDialogue < dialogue.Length)
                {
                    dialogue[currentDialogue].TriggerDialogue();
                }
                else
                {
                    ButlerInfo.butlerStop = false;
                    BigmanInfo.BigManStop = false;
                    MouseInfo.mouseStop = false;
                    sbigMan.SetActive(true);
                    Destroy(dummyButler);

                    Vector3 temp = new Vector3(-12.52f, 9.46f, -0.1651916f);
                    spawnButler.transform.position = temp;
                    dummyButler = Instantiate(spawnButler);

                    temp = new Vector3(4.19f, 9.46f, -0.1651916f);
                    spawnButler.transform.position = temp;
                    dummyButler = Instantiate(spawnButler);

                    Destroy(GameObject.Find("stopNextLevel"));

                    Instantiate(spawnWoman);
                    GameDataManager.Instance.doMovement = true;
                    Destroy(gameObject);
                }
            }
            return;
        }
        GameDataManager.Instance.doMovement = false;
        NavMeshHit hit;
        NavMesh.SamplePosition(targets[currentTarget], out hit, 500, 1);
        targets[currentTarget] = hit.position;
        Vector3 currentTargetPos = targets[currentTarget];
        if (agent.destination != currentTargetPos)
            agent.destination = currentTargetPos;
        if ((agent.destination - dummyButler.transform.position).magnitude < 1)
        {
            if (dialogueManager.HasDialogueEnded())
            {
                
                if (++currentTarget >= targets.Length)
                {
                     triggeredDialogue = true;
                    dialogue[currentDialogue++].TriggerDialogue();
                   
                }
                else if(currentTarget == targets.Length -1)
                {
                    if (!butlerSpeech)
                    {
                        --currentTarget;
                        butlerSpeech = true;
                        dialogue[currentDialogue++].TriggerDialogue();
                    }
                }
            }
            if(currentTarget == targets.Length -1)
            {
                cam.objectFollow = null;
                cam.followingObject = false;
            }
        }

        speed = dummyButler.transform.position - lastPosition;
        lastPosition = dummyButler.transform.position;
        if (Mathf.Abs(speed.x) > Mathf.Abs(speed.y))
        {
            if(Mathf.Abs(speed.x) < 0.02)
            {
                animatorOverrideController["dummy"] = animationClips[2];
            }
            else if (speed.x > 0)
            {
                animatorOverrideController["dummy"] = animationClips[1];
            }
            else
            {
                animatorOverrideController["dummy"] = animationClips[1];
            }
        }
        else
        {
            if (Mathf.Abs(speed.y) < 0.02)
            {
                animatorOverrideController["dummy"] = animationClips[2];
            }
            else if (speed.y > 0)
            {
                animatorOverrideController["dummy"] = animationClips[0];
            }
            else
            {
                    animatorOverrideController["dummy"] = animationClips[0];
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            ButlerInfo.butlerStop = true;
            BigmanInfo.BigManStop = true;
            MouseInfo.mouseStop = true;

            agent = dummyButler.GetComponent<NavMeshAgent>();
            agent.updateRotation = false;
            spriteRenderer = dummyButler.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprites[0];
            dummyButler.transform.position = spawnPos;
            lastPosition = spawnPos;
            dummyButler.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

            dummyButler.SetActive(true);
            GameDataManager.Instance.doMovement = false;
            cam.objectFollow = dummyButler;
            cam.objectZSize = 2.5f;
            cam.followingObject = true;
            spawned = true;
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
