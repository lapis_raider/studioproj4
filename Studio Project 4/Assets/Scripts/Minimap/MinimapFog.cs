﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapFog : MonoBehaviour {

    public GameObject fogPlane;
    public Transform playerTransform;
    public LayerMask fogLayer;
    public float m_radius = 30f;
    private float m_radiusSquared { get { return m_radius * m_radius; } }

    private Mesh mesh;
    private Vector3[] vertices;
    private Color[] colors;
	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = new Ray(transform.position, playerTransform.position - transform.position);
        RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction * 500,Color.red);

        if(Physics.Raycast(ray,out hit, 1000, fogLayer,QueryTriggerInteraction.Collide))
        {
            for(int i =0; i < vertices.Length; i++)
            {
                Vector3 v = fogPlane.transform.TransformPoint(vertices[i]);
                float dist = Vector3.SqrMagnitude(v - hit.point);
                if(dist < m_radiusSquared)
                {
                    float alpha = Mathf.Min(colors[i].a, dist / m_radiusSquared);
                    colors[i].a = alpha;
                }
            }
            UpdateColor();
        }


	}

    void Initialize()
    {
        mesh = fogPlane.GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        colors = new Color[vertices.Length];

        for(int i =0; i < colors.Length; i++)
        {
            colors[i] = Color.black;
        }
        UpdateColor();
    }

    void UpdateColor()
    {
        mesh.colors = colors;
    }
}
