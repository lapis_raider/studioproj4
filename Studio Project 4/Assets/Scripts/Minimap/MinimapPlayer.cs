﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapPlayer : MonoBehaviour {

    //This is the player position
    public Transform target;

    //Camera to use
    public Camera mainCamera;

    [SerializeField]
    private GameObject minimap;

    [SerializeField]
    private float minimapScale;


    //Scale of 1 is 1920 so the rest...
    
	// Use this for initialization
	void Start () {
        //mainCamera = Camera.main;
        //Convert playerPos to screenspace?
        //mainCamera.orthographicSize *= (maxScale - minimapScale);
    }
	
	// Update is called once per frame
	void Update () {
//Convert the player's world pos to screen pos

        //Change the playericon's pos to screenpos
        //playerIcon.position = screenPos;
        //Then scale down the minimap to a desired amount

        //Update the scale in editor, can put inside start as well, both works.
        minimap.GetComponent<RectTransform>().localScale = new Vector3(minimapScale,minimapScale);
    }
}
