﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdate : MonoBehaviour {
	// Update is called once per frame
	void Update () {
        switch (GameDataManager.Instance.playerData.level)
        {
            case PlayerData.LEVEL.LEVEL1:
                GetComponent<Text>().text = "Current Area : Level 1";
                break;
            case PlayerData.LEVEL.LEVEL2:
                GetComponent<Text>().text = "Current Area : Level 2";
                break;
            case PlayerData.LEVEL.LEVELMAZE:
                GetComponent<Text>().text = "Current Area : Maze";
                break;
            case PlayerData.LEVEL.LEVELBASEMENT:
                GetComponent<Text>().text = "Current Area : Basement";
                break;

        }
	}
}
