﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsVisor : MonoBehaviour {

    private Text formattedText;

    void Start()
    {
        formattedText = GetComponent<Text>();
    }
    // Update is called once per frame
    void Update () {

        string movementString = GameDataManager.Instance.keys["Visor"].ToString();
        formattedText.text = "Press " + movementString +
            " to use your visor. Using the visor allows you to highlight points of interests in the area. Use it wisely though, the energy for it drains fast.";

#if UNITY_ANDROID
         formattedText.text = "Press the visor button to use your visor. Using the visor allows you to highlight points of interests in the area. Use it wisely though, the energy for it drains fast.";
#endif
    }
}
