﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsItem : MonoBehaviour {

    private Text formattedText;

    // Use this for initialization
    void Start () {
        formattedText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

        string movementString = GameDataManager.Instance.keys["Action"].ToString();

        formattedText.text = "Press " + movementString +
            " to use an item that is selected in your inventory.";

#if UNITY_ANDROID
                 formattedText.text = "Press the skill icon to use an item that is selected in your inventory.";
#endif
    }
}
//
