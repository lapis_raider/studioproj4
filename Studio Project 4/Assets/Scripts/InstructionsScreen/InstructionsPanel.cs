﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsPanel : MonoBehaviour
{
    //Used for the sprite image
    [SerializeField]
    private GameObject prefab;

    //The image
    [SerializeField]
    private Image imageToChange;

    [SerializeField]
    private Text textToChange;

    [SerializeField]
    [TextArea(3, 10)]
    private string textString;

    // Use this for initialization
    void Start()
    {
        imageToChange.sprite = prefab.GetComponent<SpriteRenderer>().sprite;
        textToChange.text = textString;
    }
}
