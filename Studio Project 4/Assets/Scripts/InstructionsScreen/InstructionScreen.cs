﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InstructionScreen : MonoBehaviour {

    [Header("Instruction Screen")]
    [SerializeField] //cause people dont need to know the sceneName???
    private string instructionSceneName = "";

    [Header("Panels")]
    [Tooltip("The storyPanel")]
    public GameObject storyPanel;
    [Tooltip("The actionsPanel")]
    public GameObject actionPanel;
    [Tooltip("The sanityPanel")]
    public GameObject sanityPanel;

    [Header("Selection Panels")]
    [Tooltip("The selection for the background")]
    public GameObject selectionBackground;
    [Tooltip("The selection for the actions background")]
    public GameObject selectionAction;
    [Tooltip("The selection for the sanity background")]
    public GameObject selectionSanity;

    [Header("ActionPanel")]
    [Tooltip("The selection for the action/movement")]
    public GameObject selectionMovement;
    [Tooltip("The selection for the action/interactions")]
    public GameObject selectionInteractions;
    [Tooltip("The selection for the action/visor")]
    public GameObject selectionVisor;
    [Tooltip("The selection for the action/items")]
    public GameObject selectionThrowables;

    [Header("SubActionPanel")]
    [Tooltip("The subpanel for the action/movement")]
    public GameObject subSelectionMovement;
    [Tooltip("The subpanel for the action/interactions")]
    public GameObject subSelectionInteractions;
    [Tooltip("The subpanel for the action/visor")]
    public GameObject subSelectionVisor;
    [Tooltip("The subpanel for the action/items")]
    public GameObject subSelectionThrowables;

    [Header("SubItemPanel")]
    [Tooltip("The subpanel for the item/distractor")]
    public GameObject subDistractor;
    [Tooltip("The subpanel for the item/light")]
    public GameObject subLight;
    [Tooltip("The subpanel for the item/teleport")]
    public GameObject subTeleport;

    [Header("SubItemSelection")]
    [Tooltip("The selection for the item/distractor")]
    public GameObject subDistractorSelection;
    [Tooltip("The selection for the item/light")]
    public GameObject subLightSelection;
    [Tooltip("The selection for the item/teleport")]
    public GameObject subTeleportSelection;


    public void  EnableStoryPanel()
    {
        //Enable story
        storyPanel.SetActive(true);
        selectionBackground.SetActive(true);

        //Actions
        actionPanel.SetActive(false);
        selectionAction.SetActive(false);

        //Story
        sanityPanel.SetActive(false);
        selectionSanity.SetActive(false);
    }

    public void EnableActionPanel()
    {
        //Enable action
        storyPanel.SetActive(false);
        selectionBackground.SetActive(false);

        //Actions
        actionPanel.SetActive(true);
        selectionAction.SetActive(true);

        //Story
        sanityPanel.SetActive(false);
        selectionSanity.SetActive(false);
    }

    public void EnableSanityPanel()
    {
        //Enable sanity
        storyPanel.SetActive(false);
        selectionBackground.SetActive(false);

        //Actions
        actionPanel.SetActive(false);
        selectionAction.SetActive(false);

        //Story
        sanityPanel.SetActive(true);
        selectionSanity.SetActive(true);
    }

    //use string i guess
    public void EnableActionMenu(string actionMenu)
    {
        if(actionMenu.Equals("Movement"))
        {
            selectionMovement.SetActive(true);
            subSelectionMovement.SetActive(true);

            selectionInteractions.SetActive(false);
            subSelectionInteractions.SetActive(false);

            selectionVisor.SetActive(false);
            subSelectionVisor.SetActive(false);

            selectionThrowables.SetActive(false);
            subSelectionThrowables.SetActive(false);
        }
        else if (actionMenu.Equals("Interactions"))
        {
            selectionMovement.SetActive(false);
            subSelectionMovement.SetActive(false);

            selectionInteractions.SetActive(true);
            subSelectionInteractions.SetActive(true);

            selectionVisor.SetActive(false);
            subSelectionVisor.SetActive(false);

            selectionThrowables.SetActive(false);
            subSelectionThrowables.SetActive(false);
        }
        else if (actionMenu.Equals("Visor"))
        {
            selectionMovement.SetActive(false);
            subSelectionMovement.SetActive(false);

            selectionInteractions.SetActive(false);
            subSelectionInteractions.SetActive(false);

            selectionVisor.SetActive(true);
            subSelectionVisor.SetActive(true);

            selectionThrowables.SetActive(false);
            subSelectionThrowables.SetActive(false);
        }
        else if (actionMenu.Equals("Items"))
        {
            selectionMovement.SetActive(false);
            subSelectionMovement.SetActive(false);

            selectionInteractions.SetActive(false);
            subSelectionInteractions.SetActive(false);

            selectionVisor.SetActive(false);
            subSelectionVisor.SetActive(false);

            selectionThrowables.SetActive(true);
            subSelectionThrowables.SetActive(true);
        }
    }

    public void RestoreDefaults()
    {


    }

    public void EnableDistractorPanel()
    {
        subDistractor.SetActive(true);
        subDistractorSelection.SetActive(true);

        subLightSelection.SetActive(false);
        subLight.SetActive(false);

        subTeleport.SetActive(false);
        subTeleportSelection.SetActive(false);
    }
    public void EnableTeleportPanel()
    {
        subDistractor.SetActive(false);
        subDistractorSelection.SetActive(false);

        subLightSelection.SetActive(false);
        subLight.SetActive(false);

        subTeleport.SetActive(true);
        subTeleportSelection.SetActive(true);
    }
    public void EnableLightPanel()
    {
        subDistractor.SetActive(false);
        subDistractorSelection.SetActive(false);

        subLightSelection.SetActive(true);
        subLight.SetActive(true);

        subTeleport.SetActive(false);
        subTeleportSelection.SetActive(false);
    }

    public void ReturnMainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void PlaySound(string soundName)
    {
        AudioManager.instance.Play(soundName);
    }

}
