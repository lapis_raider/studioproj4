﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TO BE APPLIED TO AN OBJECT WITH MASKING
public class InstructionArrow : MonoBehaviour {

    [SerializeField]
    public GameObject arrowObject;

    //Start and end?
    public Vector3 startPos;
    public Vector3 endPos;
    public float speed;

    [Header("Offsets (if you want them) Positive values only.")]
    public float startOffset;
    public float endOffset;

	// Use this for initialization
	void Start () {
        RectTransform rt, thatRt;
        rt = GetComponent<RectTransform>();
        thatRt = arrowObject.GetComponent<RectTransform>();

        Vector3 projectedPosition = new Vector3((rt.rect.width * 0.5f) + (thatRt.rect.width * 0.5f) + endOffset, 0);
        startPos = rt.position - projectedPosition;
        endPos = rt.position + projectedPosition;
    }
	
	// Update is called once per frame
	void Update () {
        MoveFunction();
	}

    void MoveFunction()
    {
        RectTransform thatRt;
        thatRt = arrowObject.GetComponent<RectTransform>();

        thatRt.position = Vector3.MoveTowards(thatRt.position, endPos, speed * Time.deltaTime);

        if(thatRt.position.x >= endPos.x)
        {
            thatRt.position = startPos;
        }
    }
}

