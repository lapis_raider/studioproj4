﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsInteraction : MonoBehaviour {

    // Use this for initialization
    private Text formattedText;

    void Start()
    {
        formattedText = GetComponent<Text>();
    }

    void Update()
    {
        string movementString = GameDataManager.Instance.keys["Interact"].ToString();

        formattedText.text = "Press " + movementString +
            " to interact with surrounding objects. \nYou can hide from monsters and ghosts, and sometimes even find clues in them!";

#if UNITY_ANDROID
          formattedText.text =  "Press the interact button to interact with surrounding objects. \nYou can hide from monsters and ghosts, and sometimes even find clues in them!";
#endif
    }
}

