﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsMovement : MonoBehaviour {

    private Text formattedText;

    void Start()
    {
        formattedText = GetComponent<Text>();
    }

    void Update()
    {
        string movementString = GameDataManager.Instance.keys["MoveUp"].ToString() + "/" +
            GameDataManager.Instance.keys["MoveLeft"].ToString() + "/" +
            GameDataManager.Instance.keys["MoveDown"].ToString() + "/" +
            GameDataManager.Instance.keys["MoveRight"].ToString();

        formattedText.text = "Use " + movementString +
            " to move your character around. The flashlight will face the direction you are facing.";

#if UNITY_ANDROID
               formattedText.text = "Use the joystick to move your character around. The flashlight will face the direction you are facing.";
#endif
    }
}
