﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadScene : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if(GetComponent<SpriteRenderer>().color.a >= 0.95f)
            SceneManager.LoadScene("COMBINE");
    }
}
