﻿using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class MouseGiveState : State
{
    public enum MOUSE_MODES
    {
        TAKE_CHEESE, //go into hole to hide cheese
        TAKE_BONE, //come out of hole with bone
        GO_HOME
    };

    MOUSE_MODES modes;

    float TAKE_CHEESE_WAIT_TIME = 1.2f;
    float DEAD_TIMER = 0.8f;

    public override void Enter(ref GameObject go)
    {
        go.GetComponent<MouseInfo>().timer = 0.0f;

        go.GetComponent<MouseInfo>().destination = go.GetComponent<MouseInfo>().GetPathNodePos(0);
        go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<MouseInfo>().destination); //go to mouse hole
        go.GetComponent<NavMeshAgent>().speed = 10.0f;
        go.GetComponent<NavMeshAgent>().acceleration = 6.0f;

        modes = MOUSE_MODES.TAKE_CHEESE;
    }

    public override void _updates(ref GameObject go)
    {
        if ((go.GetComponent<Rigidbody2D>().position - go.GetComponent<MouseInfo>().destination).magnitude <= 2.2f) //reach destination
        {
            if (modes == MOUSE_MODES.TAKE_CHEESE) //go into hole
            {
                go.GetComponent<MouseInfo>().timer += Time.deltaTime;

                if (!go.GetComponent<MouseInfo>().bone)
                {
                    go.GetComponent<Animator>().SetTrigger("RunAwayDisappear");
                    go.GetComponent<MouseInfo>().timer = 0.0f;
                    go.GetComponent<MouseInfo>().bone = true;
                    go.GetComponent<MouseInteract>().SetOjActive(false);
                }

                if (!go.GetComponent<MouseInfo>().finishAnimation && go.GetComponent<MouseInfo>().bone && go.GetComponent<MouseInfo>().timer >= TAKE_CHEESE_WAIT_TIME)
                {
                    go.GetComponent<Animator>().SetTrigger("MouseAppear");
                    go.GetComponent<MouseInfo>().timer = 0.0f;
                    go.GetComponent<MouseInfo>().finishAnimation = true;
                }

                if (go.GetComponent<MouseInfo>().finishAnimation && go.GetComponent<MouseInfo>().timer >= TAKE_CHEESE_WAIT_TIME)
                {
                    go.GetComponent<MouseInfo>().destination = go.GetComponent<MouseInfo>().GetPathNodePos(1); //go out
                    go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<MouseInfo>().destination);
                    go.GetComponent<MouseInfo>().timer = 0.0f;
                    go.GetComponent<MouseInteract>().ChangeSprite(); //change the cheese to a bone
                    go.GetComponent<MouseInteract>().SetOjActive(true);
                    modes = MOUSE_MODES.TAKE_BONE;
                }
            }
            else if (modes == MOUSE_MODES.TAKE_BONE)
            {
                go.GetComponent<MouseInfo>().destination = go.GetComponent<MouseInfo>().GetPathNodePos(0);
                go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<MouseInfo>().destination); //go to mouse hole
                go.GetComponent<MouseInteract>().ChangeParent();//leave the clue on the floor for player


                modes = MOUSE_MODES.GO_HOME;
            }
            else
            {
                go.GetComponent<Animator>().SetTrigger("RunAwayDisappear");

                go.GetComponent<MouseInfo>().timer += Time.deltaTime;

                if (go.GetComponent<MouseInfo>().timer > DEAD_TIMER)
                    go.GetComponent<MouseInfo>().destroy = true;
            }
        }
        else
        {
            go.GetComponent<MouseInfo>().rotation = Mathf.Rad2Deg * Mathf.Atan2(go.GetComponent<NavMeshAgent>().velocity.y, go.GetComponent<MouseInfo>().GetComponent<NavMeshAgent>().velocity.x);     
        }
        go.transform.DORotate(new Vector3(0, 0, go.GetComponent<MouseInfo>().rotation), 0.2f);
        //  go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }
}
