﻿
public class MouseStateMachine : StateMachine
{
    //Add state variable here
    State stateIdle = new MouseIdleState();
    State stateRunAway = new MouseRunState();
    State stateGiveCheese = new MouseGiveState();

    // Use this for initialization
    public override State Init()
    {
        //Add State here
        AddState(stateIdle);
        stateIdle.StateName = "idle";
        //Set State Name here
        AddState(stateRunAway);
        stateRunAway.StateName = "runAway";

        AddState(stateGiveCheese);
        stateGiveCheese.StateName = "giveCheese";

        return stateIdle;
    }
}
