﻿using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class MouseIdleState : State
{
    float RANDOMIZE_TIMER = 2.0f;
    float RANDOM_RADIUS = 5.0f;

    public override void Enter(ref GameObject go)
    {
        go.GetComponent<MouseInfo>().hiding = false; //get out of hiding spot
        go.GetComponent<MouseInfo>().timer = 0.0f;

        go.GetComponent<NavMeshAgent>().speed = 2.0f;
        go.GetComponent<NavMeshAgent>().acceleration = 1.0f;
    }

    public override void _updates(ref GameObject go)
    {
        go.GetComponent<MouseInfo>().timer += Time.deltaTime;

        if (go.GetComponent<MouseInfo>().timer > RANDOMIZE_TIMER || 
            (go.GetComponent<Rigidbody2D>().position - go.GetComponent<MouseInfo>().destination).magnitude <= 0.2f) //check if reach destination
        {
            SetNewDestination(ref go); //set new destination
            go.GetComponent<MouseInfo>().timer = 0.0f;
        }

        //go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
        go.GetComponent<MouseInfo>().rotation = Mathf.Rad2Deg * Mathf.Atan2(go.GetComponent<NavMeshAgent>().velocity.y, go.GetComponent<MouseInfo>().GetComponent<NavMeshAgent>().velocity.x);
        go.transform.DORotate(new Vector3(0, 0, go.GetComponent<MouseInfo>().rotation), 0.2f);
    }

    void SetNewDestination(ref GameObject go)
    {
        Vector3 endDestination;

        NavMeshHit navHit;
        do
        {
            go.GetComponent<MouseInfo>().dir = Random.insideUnitSphere;
            endDestination = go.transform.position + go.GetComponent<MouseInfo>().dir * RANDOM_RADIUS;
        } while (!NavMesh.SamplePosition(endDestination, out navHit, RANDOM_RADIUS, -1));

        go.GetComponent<MouseInfo>().destination = navHit.position;

        go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<MouseInfo>().destination);
    }
}
