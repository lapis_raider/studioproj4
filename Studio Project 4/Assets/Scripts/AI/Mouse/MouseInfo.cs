﻿using UnityEngine;
using UnityEngine.AI;

public class MouseInfo : BaseEnemyInfo
{
    public static MouseStateMachine sm = new MouseStateMachine();

    NavMeshAgent agent;

    [HideInInspector]
    public bool hiding = false; //check if mouse hiding in hole or not
    [HideInInspector]
    public bool takeCheese = false; //check if mouse hiding in hole or not
    [HideInInspector]
    public bool finishAnimation = false;
    [HideInInspector]
    public bool bone = false;
    [HideInInspector]
    public bool destroy = false; //check if mouse hiding in hole or not
    [HideInInspector]
    public Vector2 destination; //next destination
    [HideInInspector]
    public float timer = 0.0f;

    [HideInInspector]
    public float rotation = 0.0f;

    [HideInInspector]
    public Vector3 dir;

    [HideInInspector]
    public static bool mouseStop = false;
    public override void Start()
    {
        base.Start();

        if (sm != null)
            nextState = sm.Init();

        PatrolPathNodes = WayPointMouse.instance.wayPoints;

        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;

        destination = transform.position;
    }

    public override void Update()
    {
        base.Update();
        if (stopUpdate|| mouseStop)
        {
            transform.Rotate(new Vector3(1, 0, 0), 90);
            GetComponent<NavMeshAgent>().updatePosition = false;
            return;
        }
        else
            GetComponent<NavMeshAgent>().updatePosition = true;


        if (sm != null) //update statemachine
        {
            sm.GO = gameObject;
            if (sm.GO != null)
                sm.Update();
        }

        if (destroy)
            Destroy(gameObject);
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (sm != null)
        {
            GameObject go = gameObject;
            if (currState != null)
                currState.OnCollideEnter(ref go, collision);
        }
    }

    public MouseStateMachine SM
    {
        get { return sm; }
        set { sm = value; }
    }
}
