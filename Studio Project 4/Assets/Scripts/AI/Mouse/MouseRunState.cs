﻿using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class MouseRunState : State
{
    float WAIT_TIME = 2.0f;

    public override void Enter(ref GameObject go)
    {
        go.GetComponent<MouseInfo>().timer = 0.0f;

        go.GetComponent<MouseInfo>().destination = go.GetComponent<MouseInfo>().GetPathNodePos(0);
        go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<MouseInfo>().destination); //go to mouse hole
        go.GetComponent<NavMeshAgent>().speed = 10.0f;
        go.GetComponent<NavMeshAgent>().acceleration = 6.0f;
    }

    public override void _updates(ref GameObject go)
    {
        if (!go.GetComponent<MouseInfo>().hiding) //if mouse not hiding
        {
            if ((go.GetComponent<Rigidbody2D>().position - go.GetComponent<MouseInfo>().destination).magnitude <= 2.5f) //reach destination
            {
                go.GetComponent<MouseInfo>().hiding = true;

                //do animation
               // go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
             
                go.GetComponent<Animator>().SetTrigger("RunAwayDisappear");
                go.GetComponent<MouseInfo>().rotation = Mathf.Rad2Deg * Mathf.Atan2(go.GetComponent<NavMeshAgent>().velocity.y, go.GetComponent<MouseInfo>().GetComponent<NavMeshAgent>().velocity.x);
                go.transform.DORotate(new Vector3(0, 0, go.GetComponent<MouseInfo>().rotation), 0.2f);
            }
            else
            {
              //  go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
                go.GetComponent<MouseInfo>().rotation = Mathf.Rad2Deg * Mathf.Atan2(go.GetComponent<NavMeshAgent>().velocity.y, go.GetComponent<MouseInfo>().GetComponent<NavMeshAgent>().velocity.x);
                go.transform.DORotate(new Vector3(0, 0, go.GetComponent<MouseInfo>().rotation), 0.2f);
            }
        }
        else
        {
            go.GetComponent<MouseInfo>().timer += Time.deltaTime;

            if (go.GetComponent<MouseInfo>().timer > WAIT_TIME) //mouse got out of hole
            {
                go.GetComponent<MouseInfo>().hiding = false;
                go.GetComponent<Animator>().SetTrigger("MouseAppear");
                go.GetComponent<MouseInfo>().SM.SetNextState(ref go, "idle"); //change state

            }
            go.transform.DORotate(new Vector3(0, 0, go.GetComponent<MouseInfo>().rotation), 0.2f);
        }
        //go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }
}
