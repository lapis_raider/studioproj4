﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ButlerPatrol : State
{

    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
        if (go.GetComponent<ButlerInfo>().CurrentNavID == -1)
        {
            int closestNodeID = 0;
            float distance = 100000;
            for (int i = 0; i < go.GetComponent<ButlerInfo>().GetNumOfPatrolPathNode; ++i)
            {
                float currDistance = (go.GetComponent<Rigidbody2D>().position - go.GetComponent<ButlerInfo>().GetPathNodePos(i)).magnitude;
                if (currDistance < distance)
                {
                    distance = currDistance;
                    closestNodeID = i;
                }
            }
            SetNewDestination(ref go, closestNodeID);
        }
        else
        {
            NavMeshPath path = new NavMeshPath();
            go.GetComponent<NavMeshAgent>().CalculatePath(go.GetComponent<ButlerInfo>().PlayerLastSeenPos, path);
            if (path.status == NavMeshPathStatus.PathPartial)
            {
                go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<ButlerInfo>().PlayerLastSeenPos);
                go.GetComponent<ButlerInfo>().PatrolPlayerLastSeen = true;
                //Debug.Log("Going to last seen Player pos");
            }
            else
            {
                int closestNodeID = 0;
                float distance = 100000;
                for (int i = 0; i < go.GetComponent<ButlerInfo>().GetNumOfPatrolPathNode; ++i)
                {
                    float currDistance = (go.GetComponent<Rigidbody2D>().position - go.GetComponent<ButlerInfo>().GetPathNodePos(i)).magnitude;
                    if (currDistance < distance)
                    {
                        distance = currDistance;
                        closestNodeID = i;
                    }
                }
                SetNewDestination(ref go, closestNodeID);
            }
        }
        go.GetComponent<ButlerInfo>().attacking = false;
       // Debug.Log("Butler has " + go.GetComponent<ButlerInfo>().GetNumOfPatrolPathNode);
    }

    public override void _updates(ref GameObject go)
    {
        if ((go.GetComponent<ButlerInfo>().GetPlayer.GetComponent<Rigidbody2D>().position - go.GetComponent<Rigidbody2D>().position).magnitude < 1000)
        {
            RaycastHit2D hit = Physics2D.Raycast(go.transform.position, -(go.transform.position - go.GetComponent<ButlerInfo>().GetPlayer.transform.position).normalized);
            if (hit.collider.CompareTag("Player"))
                go.GetComponent<ButlerInfo>().SM.SetNextState(ref go, "attack");
            //else
            //  Debug.Log("Raycast Not hit");
        }
        if (go.GetComponent<ButlerInfo>().GetQueueDest.Count > 0)
        {
            Vector2 temp = new Vector2(go.GetComponent<NavMeshAgent>().destination.x, go.GetComponent<NavMeshAgent>().destination.y);
            if ((temp - go.GetComponent<ButlerInfo>().GetQueueDest[0]).magnitude > 0.1)
                go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<ButlerInfo>().GetQueueDest[0]);
        }
        else if (go.GetComponent<ButlerInfo>().PatrolPlayerLastSeen)
        {
            if ((go.GetComponent<Rigidbody2D>().position - go.GetComponent<ButlerInfo>().PlayerLastSeenPos).magnitude < 0.2)
            {
                int closestNodeID = 0;
                float distance = 100000;
                for (int i = 0; i < go.GetComponent<ButlerInfo>().GetNumOfPatrolPathNode; ++i)
                {
                    float currDistance = (go.GetComponent<Rigidbody2D>().position - go.GetComponent<ButlerInfo>().GetPathNodePos(i)).magnitude;
                    if (currDistance < distance)
                    {
                        distance = currDistance;
                        closestNodeID = i;
                    }
                }
                SetNewDestination(ref go, closestNodeID);
                go.GetComponent<ButlerInfo>().PatrolPlayerLastSeen = false;
            }
        }
        else if ((go.GetComponent<Rigidbody2D>().position - go.GetComponent<ButlerInfo>().GetPathNodePos(go.GetComponent<ButlerInfo>().CurrentNavID)).magnitude < 0.2)
        {
            SetNewDestination(ref go);
        }
        go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }

    void SetNewDestination(ref GameObject go)
    {
        go.GetComponent<ButlerInfo>().CurrentNavID = Random.Range(0, go.GetComponent<ButlerInfo>().GetNumOfPatrolPathNode);
        go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<ButlerInfo>().GetPathNodePos(go.GetComponent<ButlerInfo>().CurrentNavID));
        //Debug.Log("Going Next RandomPath Node " + go.GetComponent<ButlerInfo>().CurrentNavID);
    }
    void SetNewDestination(ref GameObject go, int nodeID)
    {
        go.GetComponent<ButlerInfo>().CurrentNavID = nodeID;
        if(go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<ButlerInfo>().GetPathNodePos(nodeID)))
            Debug.LogWarning("FAILED TO SET NEW DESTINATION");
        else
            Debug.Log("Going Next RandomPath Node " + go.GetComponent<ButlerInfo>().CurrentNavID);
    }
}
