﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ButlerAttack : State {

    public override void Enter(ref GameObject go)
    {
        go.GetComponent<ButlerInfo>().attacking = true;
    }
    public override void _updates(ref GameObject go)
    {
        ButlerInfo butlerInfo = go.GetComponent<ButlerInfo>();
        RaycastHit2D hit = Physics2D.Raycast(go.transform.position, -(go.transform.position - butlerInfo.GetPlayer.transform.position).normalized);
        NavMeshPath path = new NavMeshPath();
        go.GetComponent<NavMeshAgent>().CalculatePath(butlerInfo.GetPlayer.transform.position, path);
        if (!hit.collider.CompareTag("Player") || path.status == NavMeshPathStatus.PathPartial)
        {
            if(hit.collider.IsTouchingLayers(LayerMask.NameToLayer("Enemy")))
            {
                if((go.GetComponent<Rigidbody2D>().position - butlerInfo.GetPlayer.GetComponent<Rigidbody2D>().position).magnitude > 5)
                    butlerInfo.SM.SetNextState(ref go, "patrol");
            }
            else
                butlerInfo.SM.SetNextState(ref go, "patrol");
            
        }
        else if ((butlerInfo.PlayerLastSeenPos - butlerInfo.GetPlayer.GetComponent<Rigidbody2D>().position).magnitude > 0.2)
        {
            if (go.GetComponent<NavMeshAgent>().SetDestination(butlerInfo.GetPlayer.GetComponent<Rigidbody2D>().position))
                butlerInfo.PlayerLastSeenPos = butlerInfo.GetPlayer.GetComponent<Rigidbody2D>().position;
            //else
            //    Debug.Log("Butler Failed to set dir");
        }
        go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }
    public override void OnTriggerEnter(ref GameObject go, Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            GameDataManager.Instance.playerData.PlayerSanityDecrease(go.GetComponent<ButlerInfo>().sanityDecreaser);
            go.GetComponent<ButlerInfo>().SM.SetNextState(ref go, "disappear");
        }
        go.GetComponent<NavMeshAgent>().updatePosition = false;
    }

}
