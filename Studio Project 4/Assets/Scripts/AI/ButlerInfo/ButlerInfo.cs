﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ButlerInfo : BaseEnemyInfo {

    public static ButlerStateMachine sm = new ButlerStateMachine();
    Vector3 playerLastSeenPos = new Vector3(0, 0, 0);
    bool patrolPlayerLastSeen = false;
    List<Vector2> DestQueue = new List<Vector2>();
    int currentFlashCount = 0;
    float currentFlashTimeLapsed = 0;
    [HideInInspector]
    public bool attacking = false;
    NavMeshAgent agent;
    public AnimationClip[] animationClips;
    Animator animator;
    protected AnimatorOverrideController animatorOverrideController;
    Vector3 lastPosition;
    Vector3 speed = Vector3.zero;
    public static bool butlerStop = false;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (sm != null)
            nextState = sm.Init();
        if (WayPointButler.instance.level == 1)
            PatrolPathNodes = WayPointButler.instance.wayPoints;
        else if (WayPointButler.instance.level == 2)
            PatrolPathNodes = WayPointButler.instance.wayPoints_2;
        else if (WayPointButler.instance.level == 3)
            PatrolPathNodes = WayPointButler.instance.wayPoints_3;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        animator = GetComponent<Animator>();

        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;

        lastPosition = transform.position;
    }
    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (stopUpdate || butlerStop)
        {
            transform.Rotate(new Vector3(1, 0, 0), 90);
            GetComponent<NavMeshAgent>().updatePosition = false;
            return;
        }
        else if (!GetComponent<NavMeshAgent>().updatePosition)
            GetComponent<NavMeshAgent>().updatePosition = true;
        if (sm != null)
        {
            sm.GO = gameObject;
            if (sm.GO != null)
                sm.Update();
        }
        SpriteAnimUpdate();
        agent.updateRotation = false;
        //transform.Rotate(new Vector3(1, 0, 0), 90);
    }
    void SpriteAnimUpdate()
    {
        speed = transform.position - lastPosition;
        lastPosition = transform.position;
        if (Mathf.Abs(speed.x) > Mathf.Abs(speed.y))
        {
            if (speed.x > 0)
            {
                if (attacking)
                {
                    animatorOverrideController["butleridledown"] = animationClips[7];

                }
                else
                {
                    animatorOverrideController["butleridledown"] = animationClips[3];
                }
            }
            else
            {
                if (attacking)
                {
                    animatorOverrideController["butleridledown"] = animationClips[6];
                }
                else
                {
                    animatorOverrideController["butleridledown"] = animationClips[2];
                }
            }
        }
        else
        {
            if (speed.y > 0)
            {
                if (attacking)
                {
                    animatorOverrideController["butleridledown"] = animationClips[4];
                }
                else
                {
                    animatorOverrideController["butleridledown"] = animationClips[0];
                }
            }
            else
            {
                if (attacking)
                {
                    animatorOverrideController["butleridledown"] = animationClips[5];
                }
                else
                {
                    animatorOverrideController["butleridledown"] = animationClips[1];
                }
            }
        }
    }
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (sm != null)
        {
            GameObject go = gameObject;
            if (currState != null)
                currState.OnCollideEnter(ref go, collision);
        }
    }

    public ButlerStateMachine SM
    {
        get { return sm; }
        set { sm = value; }
    }
    public Vector2 PlayerLastSeenPos
    {
        get { return playerLastSeenPos; }
        set { playerLastSeenPos = value; }
    }
    public bool PatrolPlayerLastSeen
    {
        get { return patrolPlayerLastSeen; }
        set { patrolPlayerLastSeen = value; }
    }
    public List<Vector2> GetQueueDest
    {
        get { return DestQueue; }
        set { DestQueue = value; }
    }
    public int CurrentFlashCount
    {
        get { return currentFlashCount; }
        set { currentFlashCount = value; }
        
    }
    public float CurrentFlashTimeLapsed
    {
        get { return currentFlashTimeLapsed; }
        set { currentFlashTimeLapsed = value; }
    }
}