﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButlerDisappear : State {
    int flashCount = 7;
    float MaxflashTimeLapse = 0.1f;
    public override void _updates(ref GameObject go)
    {
        ButlerInfo butlerInfo = go.GetComponent<ButlerInfo>();

        if ((butlerInfo.CurrentFlashTimeLapsed += Time.deltaTime) >= MaxflashTimeLapse)
        {
            if (butlerInfo.CurrentFlashCount % 2 == 0) // Is even, because something divided by two without remainder is even, i.e 4/2 = 2, remainder 0
            {
                Color tmp = go.GetComponent<SpriteRenderer>().color;
                tmp.a = 0f;
                go.GetComponent<SpriteRenderer>().color = tmp;
            }
            else
            {
                Color tmp = go.GetComponent<SpriteRenderer>().color;
                tmp.a = 1;
                go.GetComponent<SpriteRenderer>().color = tmp;
            }
            butlerInfo.CurrentFlashTimeLapsed -= MaxflashTimeLapse;
            ++butlerInfo.CurrentFlashCount;
            if (butlerInfo.CurrentFlashCount >= flashCount)
            {
                butlerInfo.SM.SetNextState(ref go, "patrol");
                go.SetActive(false);
            }
        }
        go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }
}
