﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButlerStateMachine : StateMachine
{

    //Add state variable here
    State statePatrol = new ButlerPatrol();
    State stateAttack = new ButlerAttack();
    State stateDisappear = new ButlerDisappear();
    // Use this for initialization
    public override State Init()
    {
        //Add State here
        AddState(statePatrol);
        statePatrol.StateName = "patrol";
        //Set State Name here
        AddState(stateAttack);
        stateAttack.StateName = "attack";

        AddState(stateDisappear);
        stateDisappear.StateName = "disappear";
        return statePatrol;
    }
}
