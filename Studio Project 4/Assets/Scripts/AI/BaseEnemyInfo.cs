﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BaseEnemyInfo : MonoBehaviour
{
    public int sanityDecreaser = 0;
    //private Variables

    protected State currState;
    protected State nextState;
    protected int currentNavID;
    protected List<Vector2> PatrolPathNodes;
    protected GameObject player;
    protected Vector2 newDest; //Override current Destination
    protected Sprite[] sprite;
    static public bool stopUpdate = false;
    // Use this for initialization
    public virtual void Start()
    {
        currState = new State();
        PatrolPathNodes = new List<Vector2>();
        currentNavID = -1;
        player = GameObject.FindGameObjectWithTag("Player");
        sprite = null;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (GetPlayer.GetComponent<PlayerMovement>().hiding)
        {
            stopUpdate = false;
        }
        else
        {
            stopUpdate = !GameDataManager.Instance.doMovement;
        }
    }
    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
      
         GameObject go = gameObject;
         if (currState != null)
             currState.OnCollideEnter(ref go, collision);
    }
    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject go = gameObject;
        if (currState != null)
            currState.OnTriggerEnter(ref go, collider);
    }
    public virtual void OnTriggerExit2D(Collider2D collider)
    {
        GameObject go = gameObject;
        if (currState != null)
            currState.OnTriggerExit(ref go, collider);
    }

    //all GET & SET functions below here
    public State CurrState
    {
        get { return currState; }
        set { currState = value; }
    }

    public State NextState
    {
        get { return nextState; }
        set { nextState = value; }
    }
    public int CurrentNavID
    {
        get { return currentNavID; }
        set { currentNavID = value; }
    }
    public Vector2 AddPatrolPathNode
    {
        set { PatrolPathNodes.Add(value); }
    }
    public int GetNumOfPatrolPathNode
    {
        get { return PatrolPathNodes.Count; }
    }
    public Vector2 GetPathNodePos(int value)
    {
        return PatrolPathNodes[value];
    }
    public GameObject GetPlayer
    {
        get { return player; }
        set { player = value; }
    }
    public void RenderSprites()
    {
        if (sprite.Length <= 1)
        {
          //  Debug.Log("Attempted To access multiple sprites with only one sprite");
            return;
        }
        if (sprite != null)
        {
            NavMeshAgent agent = GetComponent<NavMeshAgent>();
            if (Mathf.Abs(agent.velocity.x) > Mathf.Abs(agent.velocity.y))
            {
                if (agent.velocity.x > 0)
                {
                    GetComponent<SpriteRenderer>().sprite = sprite[1];
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = sprite[0];
                }
            }
            else
            {
                if (agent.velocity.y > 0)
                {
                    GetComponent<SpriteRenderer>().sprite = sprite[2];
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = sprite[3];
                }
            }
        }
    }
}
