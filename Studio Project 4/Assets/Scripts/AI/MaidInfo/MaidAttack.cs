﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaidAttack : State {
    float m_speed = 1000;
    static DialogueTrigger[] dialogues = null;
    static int currentDialogue = 0;
    static DialogueManager dialogueManager = null;
    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
    
        MaidInfo maidInfo = go.GetComponent<MaidInfo>();
        Vector3 tempPos = go.transform.position;
        Vector3 tempPlayer = maidInfo.GetPlayer.transform.position;
        tempPlayer.z = tempPos.z;
        dialogueManager = Object.FindObjectOfType<DialogueManager>();
    }

    public override void _updates(ref GameObject go)
    {
       
        MaidInfo maidObject = go.GetComponent<MaidInfo>();
        if(!maidObject.firstSeen)
        {
            if (dialogues == null)
            {
                dialogues = go.GetComponents<DialogueTrigger>();
                GameDataManager.Instance.doMovement = false;
                Camera.main.GetComponent<CameraFollow>().objectFollow = go;
                Camera.main.GetComponent<CameraFollow>().followingObject = true;
                Camera.main.GetComponent<CameraFollow>().objectZSize = 2.5f;
            }
            if(dialogueManager == null)
                dialogueManager = Object.FindObjectOfType<DialogueManager>();
            if(dialogueManager.HasDialogueEnded())
            {
                if((currentDialogue + 1) > dialogues.Length)
                {
                    maidObject.firstSeen = true;
                    GameDataManager.Instance.doMovement = true;
                    for(int i = 0; i < dialogues.Length; ++i)
                        Object.Destroy(dialogues[i]);
                    Camera.main.GetComponent<CameraFollow>().objectFollow = null;
                    Camera.main.GetComponent<CameraFollow>().followingObject = false;
                    return;
                }
                dialogues[currentDialogue++].TriggerDialogue();
            }
            return;
        }
        GameObject tempCam = go.GetComponentInChildren<Camera>().gameObject;
        Vector3 oldTempPos = tempCam.transform.position;
        Vector3 tempPos = new Vector3(tempCam.transform.position.x, tempCam.transform.position.y, maidObject.GetPlayer.transform.position.z);
        tempCam.transform.position = tempPos;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(go.GetComponentInChildren<Camera>());
       // Debug.Log((go.transform.position - maidObject.GetPlayer.transform.position).magnitude);

        Vector3 tempPosNi = go.transform.position;
        Vector3 tempPlayer = maidObject.GetPlayer.transform.position;
        tempPlayer.z = tempPosNi.z;
        if (!GeometryUtility.TestPlanesAABB(planes, maidObject.GetPlayer.GetComponent<BoxCollider2D>().bounds) || (tempPosNi - tempPlayer).magnitude > 4)
        {
            maidObject.SM.SetNextState(ref go, "lookout");
           
        }
        tempCam.transform.position = oldTempPos;

        if (!maidObject.attacking)
        {
            if (maidObject.GraceTimeLapse < maidObject.GraceTime)
                maidObject.GraceTimeLapse += Time.deltaTime;
            if (maidObject.GraceTimeLapse >= maidObject.GraceTime)
            {
                if(maidObject.GetPlayer.GetComponent<Rigidbody2D>().velocity != Vector2.zero)
                {
                    maidObject.attacking = true;
                    AudioManager.instance.Play("Scream");
                    GameDataManager.Instance.playerData.PlayerSanityDecrease(maidObject.sanityDecreaser);
                }
            }
        }
        else if (maidObject.attacking)
        {
            switch (maidObject.direction)
            {
                case MaidInfo.Direction.DOWN:
                    {
                        maidObject.GetPlayer.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -1) * m_speed);
                    }
                    break;
                case MaidInfo.Direction.UP:
                    {
                        maidObject.GetPlayer.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * m_speed);
                    }
                    break;
                case MaidInfo.Direction.LEFT:
                    {
                        maidObject.GetPlayer.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, 0) * m_speed);
                    }
                    break;
                case MaidInfo.Direction.RIGHT:
                    {
                        maidObject.GetPlayer.GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 0) * m_speed);
                    }
                    break;


            }
        }
        if((maidObject.CurrentDirTimeLapsed += Time.deltaTime) >= maidObject.DirTime)
        {
            maidObject.SM.SetNextState(ref go, "lookout");
        }
    }
    public override void OnCollideEnter(ref GameObject go, Collision2D collider)
    {
        //trt

    }
}
