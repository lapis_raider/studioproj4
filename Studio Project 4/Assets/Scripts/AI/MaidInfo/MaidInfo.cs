﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MaidInfo : BaseEnemyInfo {

    public static MaidStateMachine sm = new MaidStateMachine();
    public Item clue;
    public Dialogue cluePickUp;
    double randomDirTime = 0;
    double currentDirTimeLapsed = 0;
    double graceTimeLapsed = 0;
    public double maxGraceTime = 0.5;
    NavMeshAgent agent;
    static Sprite[] spriteIdle;
    static Sprite[] spriteAttack;
    SpriteRenderer spriteRenderer;
    public bool attacking = false;
    public float minDirTime = 0f;
    public float maxDirTime = 1.5f;
    public bool firstSeen = false;
    [HideInInspector]
    public bool cluePickUpDone = false;
    public enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
    public Direction direction = Direction.DOWN;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (sm != null)
            nextState = sm.Init();

        agent = GetComponent<NavMeshAgent>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteIdle = Resources.LoadAll<Sprite>("Enemies/Maid/maidIdle");
        spriteAttack = Resources.LoadAll<Sprite>("Enemies/Maid/maidAttack");
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (stopUpdate)
        {
            return;
        }
        if (sm != null)
        {
            sm.GO = gameObject;
            if (sm.GO != null)
                sm.Update();
        }

        switch (direction)
        {
            case Direction.DOWN:
                {
                    if (attacking)
                        spriteRenderer.sprite = spriteAttack[0];
                    else
                        spriteRenderer.sprite = spriteIdle[0];
                }
                break;
            case Direction.LEFT:
                {
                    if (attacking)
                        spriteRenderer.sprite = spriteAttack[1];
                    else
                        spriteRenderer.sprite = spriteIdle[1];
                }
                break;
            case Direction.UP:
                {
                    if (attacking)
                        spriteRenderer.sprite = spriteAttack[2];
                    else
                        spriteRenderer.sprite = spriteIdle[2];
                }
                break;
            case Direction.RIGHT:
                {
                    if (attacking)
                        spriteRenderer.sprite = spriteAttack[3];
                    else
                        spriteRenderer.sprite = spriteIdle[3];
                }
                break;
            default:
                break;
        }
    }
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (sm != null)
        {
            GameObject go = gameObject;
            if (currState != null)
                currState.OnCollideEnter(ref go, collision);
        }
    }

    public MaidStateMachine SM
    {
        get { return sm; }
        set { sm = value; }

    }
    public double CurrentDirTimeLapsed
    {
        get { return currentDirTimeLapsed; }
        set { currentDirTimeLapsed = value; }
    }
    public double DirTime
    {
        get { return randomDirTime; }
        set { randomDirTime = value; }
    }
    public double GraceTimeLapse
    {
        get { return graceTimeLapsed; }
        set { graceTimeLapsed = value; }
    }
    public double GraceTime
    {
        get { return maxGraceTime; }
        set { maxGraceTime = value; }
    }
    public void OnBecameInvisible()
    {

    }
}
