﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaidStateMachine : StateMachine {

    //Add state variable here
    State stateLookout = new MaidLookout();
    State stateAttack = new MaidAttack();
    // Use this for initialization
    public override State Init()
    {
        //Add State here
        AddState(stateLookout);
        stateLookout.StateName = "lookout";
        //Set State Name here
        AddState(stateAttack);
        stateAttack.StateName = "attack";
        return stateLookout;
    }
}
