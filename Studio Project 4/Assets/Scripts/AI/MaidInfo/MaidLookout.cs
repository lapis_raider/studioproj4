﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaidLookout : State {
    bool playerInBound = false;
    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
       //Debug.Log("Woman is look out mode");
        MaidInfo maidInfo = go.GetComponent<MaidInfo>();
        maidInfo.DirTime = Random.Range(maidInfo.minDirTime, maidInfo.maxDirTime);
        go.GetComponent<MaidInfo>().attacking = false;
    }

    public override void _updates(ref GameObject go)
    {
        MaidInfo maidObject = go.GetComponent<MaidInfo>();
        GameObject tempCam = go.GetComponentInChildren<Camera>().gameObject;
        Vector3 oldTempPos = tempCam.transform.position;
        Vector3 tempPos = new Vector3(tempCam.transform.position.x, tempCam.transform.position.y, maidObject.GetPlayer.transform.position.z);
        tempCam.transform.position = tempPos;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(go.GetComponentInChildren<Camera>());
        if (maidObject.GraceTimeLapse < maidObject.GraceTime)
            maidObject.GraceTimeLapse += Time.deltaTime;


        {
            maidObject.CurrentDirTimeLapsed += Time.deltaTime;
            if (maidObject.CurrentDirTimeLapsed >= maidObject.DirTime)
            {
                maidObject.DirTime = Random.Range(maidObject.minDirTime, maidObject.maxDirTime);
                maidObject.CurrentDirTimeLapsed = 0;
                int RandomDir = 0;

                if (!maidObject.firstSeen)
                    RandomDir = 2;
                else
                {
                    if (maidObject.direction == MaidInfo.Direction.UP)
                        RandomDir = 2;
                    else
                        RandomDir = 1;
                }

                maidObject.GraceTimeLapse = 0;
                if (RandomDir == 1)
                {
                    tempCam.transform.eulerAngles = new Vector3(-90, 90, 90);
                    maidObject.direction = MaidInfo.Direction.UP;
                }
                else if (RandomDir == 4)
                {
                    tempCam.transform.eulerAngles = new Vector3(0, 90, 90);
                    maidObject.direction = MaidInfo.Direction.RIGHT;
                }
                else if (RandomDir == 3)
                {
                    tempCam.transform.eulerAngles = new Vector3(180, 90, 90);
                    maidObject.direction = MaidInfo.Direction.LEFT;
                }
                else if (RandomDir == 2)
                {
                    tempCam.transform.eulerAngles = new Vector3(90, 90, 90);
                    maidObject.direction = MaidInfo.Direction.DOWN;
                }
            }
        }
        if (GeometryUtility.TestPlanesAABB(planes, maidObject.GetPlayer.GetComponent<BoxCollider2D>().bounds))
        {
            maidObject.SM.SetNextState(ref go, "attack");
        }
        tempCam.transform.position = oldTempPos;
    }
    public override void OnTriggerEnter(ref GameObject go, Collider2D collider)
    {
        if (collider.CompareTag("Player"))
            playerInBound = true;
    }
    public override void OnTriggerExit(ref GameObject go, Collider2D collider)
    {
        if (collider.CompareTag("Player"))
            playerInBound = false;
    }
}
