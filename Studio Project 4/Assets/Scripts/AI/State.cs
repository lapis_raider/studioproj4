﻿using UnityEngine;

public class State
{

    string stateName = "NONE";

    public string StateName
    {
        get { return stateName; }
        set { stateName = value; }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public virtual void _updates(ref GameObject go) {}
    public virtual void Exit(ref GameObject go){}
    public virtual void Enter(ref GameObject go){}
    public virtual void OnCollideEnter(ref GameObject go, Collision2D collider) { }
    public virtual void OnTriggerEnter(ref GameObject go, Collider2D collider) { }
    public virtual void OnTriggerExit(ref GameObject go, Collider2D collider) { }
}
