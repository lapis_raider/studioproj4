﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BabyStateMachine : StateMachine
{

    //Add state variable here
    State stateIdle = new BabyIdle();
    State stateCrying = new BabyCrying();
    // Use this for initialization
    public override State Init()
    {
        //Add State here
        AddState(stateIdle);
        stateIdle.StateName = "idle";
        //Set State Name here
        AddState(stateCrying);
        stateCrying.StateName = "crying";
        return stateIdle;
    }
}
