﻿
using UnityEngine;
using UnityEngine.AI;
public class BabyCrying : State {
    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
        AudioManager.instance.Play("Baby");
        GameObject bigMan = GameObject.FindGameObjectWithTag("BigMan");
        if(!bigMan)
        {
            bigMan = GameObject.Find("BigManSpawn").GetComponent<spawnBigMan>().CallBigManToBabyRoom();
        }
        if(bigMan)
        {
            bigMan.GetComponent<BigmanInfo>().GetQueueDest.Add(go.GetComponent<Rigidbody2D>().position);
            bigMan.GetComponent<BigmanInfo>().GoingQueue = true;
        }
        go.GetComponent<BabyInfo>().babySleeping = true;
        go.GetComponent<Animator>().SetBool("crying", true);
        GameDataManager.Instance.playerData.PlayerSanityDecrease(go.GetComponent<BabyInfo>().sanityDecreaser);
    }

    public override void _updates(ref GameObject go)
    {
       
    }

    public override void OnCollideEnter(ref GameObject go, Collision2D collider) {
        if (collider.collider.CompareTag("BigMan"))
        {
            GameObject bigMan = GameObject.FindGameObjectWithTag("BigMan");
            bigMan.GetComponent<BigmanInfo>().GetQueueDest.RemoveAt(0);
            go.GetComponent<BabyInfo>().SM.SetNextState(ref go, "idle");
            if(bigMan.GetComponent<BigmanInfo>().GetQueueDest.Count == 0)
            {
                int closestNodeID = 0;
                float distance = 100000;
                for (int i = 0; i < bigMan.GetComponent<BigmanInfo>().GetNumOfPatrolPathNode; ++i)
                {
                    float currDistance = (bigMan.GetComponent<Rigidbody2D>().position - bigMan.GetComponent<BigmanInfo>().GetPathNodePos(i)).magnitude;
                    if (currDistance < distance)
                    {
                        distance = currDistance;
                        closestNodeID = i;
                    }
                }
                bigMan.GetComponent<BigmanInfo>().CurrentNavID = closestNodeID;
                bigMan.GetComponent<NavMeshAgent>().SetDestination(bigMan.GetComponent<BigmanInfo>().GetPathNodePos(closestNodeID));
                //Debug.Log("Going Next RandomPath Node " + bigMan.GetComponent<BigmanInfo>().CurrentNavID);
            }
        }
    }
}
