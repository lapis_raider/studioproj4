﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyIdle : State {

    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
        //Debug.Log("Baby is Sleeping Mode");
        go.GetComponent<BabyInfo>().babySleeping = true;
        go.GetComponent<Animator>().SetBool("crying", false);
    }

    public override void _updates(ref GameObject go)
    {
        if ((go.GetComponent<BabyInfo>().GetPlayer.GetComponent<Rigidbody2D>().position - go.GetComponent<Rigidbody2D>().position).magnitude < 2)
        {
            go.GetComponent<BabyInfo>().SM.SetNextState(ref go, "crying");
        }
    }
}
