﻿using UnityEngine;

public class BabyInfo : BaseEnemyInfo {
    public static BabyStateMachine sm = new BabyStateMachine();
    public bool babySleeping = true;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (sm != null)
            nextState = sm.Init();
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (stopUpdate)
        {
            return;
        }
        if (sm != null)
        {
            sm.GO = gameObject;
            if (sm.GO != null)
                sm.Update();
        }
    }
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (sm != null)
        {
            GameObject go = gameObject;
            if (currState != null)
                currState.OnCollideEnter(ref go, collision);
        }
    }

    public BabyStateMachine SM
    {
        get { return sm; }
        set { sm = value; }
    }
}
