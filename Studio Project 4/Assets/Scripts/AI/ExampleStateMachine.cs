﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleStateMachine : StateMachine {

    //Add state variable here
    State statePatrol = new BigManPatrol();
    State stateAttack = new BigManAttack();
    // Use this for initialization
    public override State Init()
    {
        //Add State here
        AddState(statePatrol);
        statePatrol.StateName = "patrol";
        //Set State Name here
        AddState(stateAttack);
        stateAttack.StateName = "attack";
        return statePatrol;
    }

    
}
