﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine {

    List<State> stateMap = new List<State>();
    GameObject go;
    // Use this for initialization
    void Awake ()
    {
	}
    public virtual State Init()
    {
        return null;
    }
    public void AddState(State newState)
    {
        int count = stateMap.Count;//check number of states inside the StateMachine before adding
        stateMap.Add(newState);//add newState as first node
    
        if (stateMap.Count == count)//check if state has been successfully added
            Debug.Log("Failed to Add Class");
    }
    public void SetNextState(ref GameObject go, string stateName)
    {
        for(int i = 0; i < stateMap.Count; ++i)
        {//loop through the stateMap
            if(stateMap[i].StateName == stateName)
            {//if state is found 
                go.GetComponent<BaseEnemyInfo>().NextState = stateMap[i];//set state as next state
                break;// break from the loop
            }
        }

        if (go.GetComponent<BaseEnemyInfo>().NextState.StateName != stateName)//if cant find next state or if next state is = to currState
            Debug.Log("Failed to Set Next State");
    }
    // Update is called once per frame
    public void Update () {
       // Debug.Log(go.GetComponent<BaseEnemyInfo>().CurrState.StateName);
       // Debug.Log(go.GetComponent<BaseEnemyInfo>().NextState.StateName);
        if (go.GetComponent<BaseEnemyInfo>().CurrState.StateName != go.GetComponent<BaseEnemyInfo>().NextState.StateName)
        {// check if
            go.GetComponent<BaseEnemyInfo>().CurrState.Exit(ref go);
            go.GetComponent<BaseEnemyInfo>().CurrState = go.GetComponent<BaseEnemyInfo>().NextState;
            go.GetComponent<BaseEnemyInfo>().CurrState.Enter(ref go);
        }
        go.GetComponent<BaseEnemyInfo>().CurrState._updates(ref go);
	}

    public GameObject GO
    {
        get { return go; }
        set { go = value; }
    }
}
