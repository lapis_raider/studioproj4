﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class SmallWomanCrying : State {

    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
        SmallWomanInfo smallWomanInfo = go.GetComponent<SmallWomanInfo>();
        GameDataManager.Instance.doMovement = true;
        go.GetComponent<SmallWomanInfo>().womanContinue = false;
        //Debug.Log("Woman is crying mode");
        smallWomanInfo.AttackEnded = false;

        Vector3 finalPosition = smallWomanInfo.GetPathNodePos(Random.Range(0, smallWomanInfo.GetNumOfPatrolPathNode-1));
        Vector3 oldPos = go.transform.position;
        go.transform.position = finalPosition;

        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        while (GeometryUtility.TestPlanesAABB(planes, go.GetComponent<PolygonCollider2D>().bounds))
        {
            finalPosition = smallWomanInfo.GetPathNodePos(Random.Range(0, smallWomanInfo.GetNumOfPatrolPathNode - 1));
            go.transform.position = finalPosition;
        }
    }

    public override void _updates(ref GameObject go)
    {
        SmallWomanInfo smallWomanInfo = go.GetComponent<SmallWomanInfo>();

        if (!go.GetComponent<SpriteRenderer>().isVisible)
        {
            Vector3 finalPosition = smallWomanInfo.GetPathNodePos(Random.Range(0, smallWomanInfo.GetNumOfPatrolPathNode - 1));
            Vector3 oldPos = go.transform.position;
            go.transform.position = finalPosition;
            Camera cam = Camera.main;
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);
            while (GeometryUtility.TestPlanesAABB(planes, go.GetComponent<PolygonCollider2D>().bounds))
            {
                finalPosition = smallWomanInfo.GetPathNodePos(Random.Range(0, smallWomanInfo.GetNumOfPatrolPathNode - 1));
                go.transform.position = finalPosition;
            }
        }
    }

    public override void OnTriggerEnter(ref GameObject go, Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            go.GetComponent<SmallWomanInfo>().SM.SetNextState(ref go, "attack");
            GameDataManager.Instance.doMovement = false;
            go.GetComponent<SmallWomanInfo>().womanContinue = true;
            go.GetComponent<SmallWomanInfo>().GetPlayer.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }
    }
}
