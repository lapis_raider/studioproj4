﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallWomanAttack : State {
    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
        Camera.main.GetComponent<CameraFollow>().followWoman = true;
        go.GetComponent<Animator>().SetBool("smallWomanAttack", true);

    }

    public override void _updates(ref GameObject go)
    {
        //Debug.Log("Woman is attacking");
        SmallWomanInfo smallWomanInfo = go.GetComponent<SmallWomanInfo>();
        if (smallWomanInfo.AttackEnded == true)
        {
            go.GetComponent<Animator>().SetBool("smallWomanAttack", false);
            Camera.main.GetComponent<CameraFollow>().followWoman = false;
            smallWomanInfo.SM.SetNextState(ref go, "crying");
            GameDataManager.Instance.playerData.PlayerSanityDecrease(smallWomanInfo.sanityDecreaser);
        }
    }
}
