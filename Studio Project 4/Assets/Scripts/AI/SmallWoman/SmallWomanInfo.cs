﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallWomanInfo : BaseEnemyInfo {

    public static SmallWomanStateMachine sm = new SmallWomanStateMachine();
    public bool playCrackHeadEffect = false;
    public bool finishedAttack = false;
    public bool lowerBackgroundAmbience = false;
    public bool womanContinue = false;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (sm != null)
            nextState = sm.Init();
        PatrolPathNodes = WayPointSmallWoman.instance.wayPoints;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if(stopUpdate && !womanContinue)
        {
            return;
        }
        if (sm != null)
        {
            sm.GO = gameObject;
            if (sm.GO != null)
                sm.Update();
        }
        if(playCrackHeadEffect)
        {
            AudioManager.instance.Play("crack head");
            playCrackHeadEffect = false;
        }
        if(lowerBackgroundAmbience)
        {
            AudioManager.instance.GetSound("background").volume = Mathf.Lerp(AudioManager.instance.GetSound("background").volume, 0, 0.04f);
        }
        else
        {
            AudioManager.instance.GetSound("background").volume = Mathf.Lerp(AudioManager.instance.GetSound("background").volume, 0.6f, 0.04f);
        }

        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -0.1f);

    }
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (sm != null)
        {
            GameObject go = gameObject;
            if (currState != null)
                currState.OnCollideEnter(ref go, collision);
        }
    }

    public SmallWomanStateMachine SM
    {
        get { return sm; }
        set { sm = value; }
    }
   public bool AttackEnded
    {
        get { return finishedAttack; }
        set { finishedAttack = value; }
    }
}
