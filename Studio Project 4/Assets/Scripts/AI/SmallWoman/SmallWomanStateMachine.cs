﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallWomanStateMachine : StateMachine {

    //Add state variable here
    State stateCrying = new SmallWomanCrying();
    State stateAttack = new SmallWomanAttack();
    // Use this for initialization
    public override State Init()
    {
        //Add State here
        AddState(stateCrying);
        stateCrying.StateName = "crying";
        //Set State Name here
        AddState(stateAttack);
        stateAttack.StateName = "attack";
        return stateCrying;
    }
}
