﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointBigMan : MonoBehaviour {
    public List<Vector2> wayPoints;
    public List<Vector2> wayPoints_2;
    public List<Vector2> wayPoints_3;
    public int level = 1;
    public static WayPointBigMan instance;

    // Use this for initialization
    void Awake()
    {
        if (instance != null)
        {
            if(instance.level == 1)
            instance.wayPoints.Add(gameObject.transform.position);
            else if (instance.level == 2)
            instance.wayPoints_2.Add(gameObject.transform.position);
            else if (instance.level == 3)
                instance.wayPoints_3.Add(gameObject.transform.position);
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            if (instance.level == 1)
                instance.wayPoints.Add(gameObject.transform.position);
            else if (instance.level == 2)
                instance.wayPoints_2.Add(gameObject.transform.position);
            else if (instance.level == 3)
                instance.wayPoints_2.Add(gameObject.transform.position);
        }
    }

}
