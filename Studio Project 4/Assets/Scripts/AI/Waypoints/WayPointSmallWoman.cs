﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointSmallWoman : MonoBehaviour
{
    public List<Vector2> wayPoints;
    public static WayPointSmallWoman instance;

    // Use this for initialization
    void Awake()
    {
        if (instance != null)
        {
            instance.wayPoints.Add(gameObject.transform.position);
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            wayPoints.Add(gameObject.transform.position);
        }
    }
}
