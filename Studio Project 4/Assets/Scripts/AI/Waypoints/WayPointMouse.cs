﻿using System.Collections.Generic;
using UnityEngine;

public class WayPointMouse : MonoBehaviour {
    public List<Vector2> wayPoints;
    public static WayPointMouse instance;

    // Use this for initialization
    void Awake()
    {
        if (instance != null)
        {
            instance.wayPoints.Add(gameObject.transform.position);
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            wayPoints.Add(gameObject.transform.position);
        }
    }
}
