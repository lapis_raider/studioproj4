﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBigMan : MonoBehaviour {

    string currentLevel;
    string nextLevel;
     Vector2 newPosition;
    float spawnTime;
    public GameObject bigManPrefab;
    bool spawned = true;
    Vector2 pos;
	// Update is called once per frame
	void Update () {
        if (!spawned)
        {
            spawnTime -= Time.deltaTime;
            if (spawnTime <= 0)
            {
                bigManPrefab.transform.localPosition = newPosition;
                GameObject bigman = Instantiate(bigManPrefab);
                
                //Debug.Log(bigman.transform.position);
                spawned = true;
            }
        }
	}

    public void SetNewSpawn(string CurrentLevel, string NextLevel, Vector2 NewPosition, float SpawnTime)
    { 
            currentLevel = CurrentLevel;
            nextLevel = NextLevel;
            spawnTime = SpawnTime;
            newPosition = NewPosition;
            spawned = false;
    }

    public void SetNewSpawn(string CurrentLevel, string NextLevel, Vector2 NewPosition)
    {
        BigmanInfo bigman = FindObjectOfType<BigmanInfo>();

        if (NextLevel == currentLevel && !spawned)
        {//when player goes back to first seen before big man spawns
            currentLevel = CurrentLevel;
            nextLevel = NextLevel;
            spawnTime = 2;
            newPosition = pos;
        }
        else
        {
            currentLevel = CurrentLevel;
            nextLevel = NextLevel;
            newPosition = NewPosition;
            if (bigman != null)
                spawnTime = bigman.CalculateNextSpawnTime() + 5;
            else
                spawnTime = 1000;
        }

            spawned = false;
        if (bigman != null)
        {
            pos = bigman.transform.position;
            bigman.destry = true;
        }
    }

    public GameObject CallBigManToBabyRoom()
    {
        if (!spawned)
        {
            spawnTime = -1;
            if (spawnTime <= 0)
            {
                bigManPrefab.transform.localPosition = newPosition;
                GameObject bigman = Instantiate(bigManPrefab);

                //Debug.Log(bigman.transform.position);
                spawned = true;
                return bigman;
            }
        }
        return GameObject.FindGameObjectWithTag("BigMan");
    }
}
