﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BigManPatrol : State {

    // Update is called once per frame
    public override void Enter(ref GameObject go)
    {
        if(go.GetComponent<BigmanInfo>().GetQueueDest.Count > 0)
        {
            go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<BigmanInfo>().GetQueueDest[0]);
        }
        else
        {
            if (go.GetComponent<BigmanInfo>().CurrentNavID == -1)
            {
                go.GetComponent<BigmanInfo>().CurrentNavID = 0;
                go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<BigmanInfo>().GetPathNodePos(0));
            }
            else
            {
                go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<BigmanInfo>().PlayerLastSeenPos);
                go.GetComponent<BigmanInfo>().PatrolPlayerLastSeen = true;
                //Debug.Log("Going to last seen Player pos");
            }
        }
    }

    public override void _updates(ref GameObject go)
    {
        BigmanInfo bigmanInfo = go.GetComponent<BigmanInfo>();
        Rigidbody2D rigidbody2D = go.GetComponent<Rigidbody2D>();
        NavMeshAgent agent = go.GetComponent<NavMeshAgent>();

        if ((bigmanInfo.GetPlayer.GetComponent<Rigidbody2D>().position - rigidbody2D.position).magnitude < 1000)
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(go.transform.position, -(go.transform.position - bigmanInfo.GetPlayer.transform.position).normalized);

            for(int i = 0; i < hits.Length; ++i)
            {
                if(hits[i].collider.CompareTag("Player"))
                {
                    if(i == 0)
                        bigmanInfo.SM.SetNextState(ref go, "attack");
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (!hits[j].collider.CompareTag("tile"))
                            break;
                        if(j == 0)
                            bigmanInfo.SM.SetNextState(ref go, "attack");
                    }
                    break;
                }
            }
                
            //else
            //  Debug.Log("Raycast Not hit");
        }
        if (bigmanInfo.GetQueueDest.Count > 0)
        {
            Vector2 temp = new Vector2(agent.destination.x, agent.destination.y);
            if ((temp - bigmanInfo.GetQueueDest[0]).magnitude > 0.1)
            {
                if (!agent.SetDestination(bigmanInfo.GetQueueDest[0]))
                {

                }
                   // Debug.Log("Failed to set new direction according to queue dest 0");
            }
        }
        else if (bigmanInfo.PatrolPlayerLastSeen)
        {
            if ((rigidbody2D.position - bigmanInfo.PlayerLastSeenPos).magnitude < 0.2)
            {
                int closestNodeID = 0;
                float distance = 100000;
                for(int i = 0; i < bigmanInfo.GetNumOfPatrolPathNode; ++i)
                {
                    float currDistance = (rigidbody2D.position - bigmanInfo.GetPathNodePos(i)).magnitude;
                    if (currDistance < distance)
                    {
                        distance = currDistance;
                        closestNodeID = i;
                    }
                }
                SetNewDestination(ref go, closestNodeID);
                
                bigmanInfo.PatrolPlayerLastSeen = false;
            }
        }
        else if ((rigidbody2D.position - bigmanInfo.GetPathNodePos(bigmanInfo.CurrentNavID)).magnitude < 0.2)
        {
            SetNewDestination(ref go);
        }
        go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }

    void SetNewDestination(ref GameObject go)
    {
        go.GetComponent<BigmanInfo>().CurrentNavID = Random.Range(0, go.GetComponent<BigmanInfo>().GetNumOfPatrolPathNode);
        go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<BigmanInfo>().GetPathNodePos(go.GetComponent<BigmanInfo>().CurrentNavID));
        //Debug.Log("Going Next RandomPath Node " + go.GetComponent<BigmanInfo>().CurrentNavID);
    }
    void SetNewDestination(ref GameObject go, int nodeID)
    {
        go.GetComponent<BigmanInfo>().CurrentNavID = nodeID;
        go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<BigmanInfo>().GetPathNodePos(nodeID));
        Vector2 temp = new Vector2(go.GetComponent<NavMeshAgent>().destination.x, go.GetComponent<NavMeshAgent>().destination.y);
        if( temp == go.GetComponent<BigmanInfo>().GetPathNodePos(nodeID))
        {

        }
        //Debug.Log("Going Next RandomPath Node " + go.GetComponent<BigmanInfo>().CurrentNavID);
       // else
            //Debug.Log("failed to set next idPos " + go.GetComponent<BigmanInfo>().CurrentNavID);
    }
}
