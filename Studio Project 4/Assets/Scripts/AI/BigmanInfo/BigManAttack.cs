﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BigManAttack : State {
    public override void Enter(ref GameObject go)
    {
        go.GetComponent<BigmanInfo>().chasing = true;
        go.GetComponent<BigmanInfo>().attacking = false;
    }
    public override void _updates(ref GameObject go)
    {
        RaycastHit2D hit = Physics2D.Raycast(go.transform.position, -(go.transform.position - go.GetComponent<BigmanInfo>().GetPlayer.transform.position).normalized);
        if (!hit.collider.CompareTag("Player"))
            go.GetComponent<BigmanInfo>().SM.SetNextState(ref go, "patrol");
        else if ((go.GetComponent<BigmanInfo>().PlayerLastSeenPos - go.GetComponent<BigmanInfo>().GetPlayer.GetComponent<Rigidbody2D>().position).magnitude > 0.2)
        {
            if (go.GetComponent<NavMeshAgent>().SetDestination(go.GetComponent<BigmanInfo>().GetPlayer.GetComponent<Rigidbody2D>().position))
                go.GetComponent<BigmanInfo>().PlayerLastSeenPos = go.GetComponent<BigmanInfo>().GetPlayer.transform.position;           
           // else
               // Debug.Log("Failed to set dir");
        }
        go.GetComponent<Transform>().Rotate(new Vector3(1, 0, 0), 90);
    }
    public override void OnCollideEnter(ref GameObject go, Collision2D collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            go.GetComponent<BigmanInfo>().chasing = false;
            go.GetComponent<BigmanInfo>().attacking = true;
            GameDataManager.Instance.playerData.PlayerSanityDecrease(GameDataManager.Instance.playerData.playerFullSanity);
        }
           
    }
}
