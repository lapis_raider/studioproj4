﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Sprites;
public class BigmanInfo : BaseEnemyInfo {

    public static ExampleStateMachine sm = new ExampleStateMachine();
    Vector3 playerLastSeenPos = new Vector3(0, 0, 0);
    bool patrolPlayerLastSeen = false;
    List<Vector2> DestQueue = new List<Vector2>();
    bool goingQueue = false;
    NavMeshAgent agent;
    public bool destry = false;

    public AnimationClip[] animationClips;
    Animator animator;
    protected AnimatorOverrideController animatorOverrideController;
    Vector3 lastPosition;
    Vector3 speed = Vector3.zero;

    public bool attacking = false;
    public bool chasing = false;
    public bool idle = false;
    public static bool BigManStop = false;
    // Use this for initialization
    public override void Start()
    {

        base.Start();
        if (sm != null)
            nextState = sm.Init();
        agent = GetComponent<NavMeshAgent>();
        if (WayPointBigMan.instance.level == 1)
            PatrolPathNodes = WayPointBigMan.instance.wayPoints;
        else if (WayPointBigMan.instance.level == 2)
            PatrolPathNodes = WayPointBigMan.instance.wayPoints_2;
        //Debug.Log("BIG MAN NODES = " + PatrolPathNodes.Count);
        agent.updateRotation = false;
        sprite = Resources.LoadAll<Sprite>("Enemies/BigMan/BigManSpriteSheet");


        animator = GetComponent<Animator>();
        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;
        lastPosition = transform.position;
    }

    // Update is called once per frame
    public override void Update()
    {
        if (destry)
        {
            Destroy(gameObject);
        }
        base.Update();
        if (stopUpdate || BigManStop)
        {
            transform.Rotate(new Vector3(1, 0, 0), 90);
            GetComponent<NavMeshAgent>().updatePosition = false;
            return;
        }
        else
            GetComponent<NavMeshAgent>().updatePosition = true;
        if (sm != null)
        {
            sm.GO = gameObject;
            if (sm.GO != null)
                sm.Update();
        }
        speed = transform.position - lastPosition;
        lastPosition = transform.position;

        if (Mathf.Abs(speed.x) > Mathf.Abs(speed.y))
        {
            if (speed.x > 0)//right
            {
                if (attacking)
                {
                    agent.speed = 0.1f;
                    animatorOverrideController["BigManAnim"] = animationClips[11];
                }
                else if (chasing)
                {
                    agent.speed = 2.5f;
                    animatorOverrideController["BigManAnim"] = animationClips[7];
                }
                else
                {
                    agent.speed = 1;
                    animatorOverrideController["BigManAnim"] = animationClips[3];
                }
            }
            else
            {
                if (attacking)//left
                {
                    agent.speed = 0.1f;
                    animatorOverrideController["BigManAnim"] = animationClips[10];
                }
                else if (chasing)
                {
                    agent.speed = 2.5f;
                    animatorOverrideController["BigManAnim"] = animationClips[6];
                }
                else
                {
                    agent.speed = 1;
                    animatorOverrideController["BigManAnim"] = animationClips[2];
                }
            }
        }
        else
        {
            if (speed.y > 0)
            {
                if (attacking)//up
                {
                    agent.speed = 0.1f;
                    animatorOverrideController["BigManAnim"] = animationClips[8];
                }
                else if (chasing)
                {
                    agent.speed = 2.5f;
                    animatorOverrideController["BigManAnim"] = animationClips[4];
                }
                else
                {
                    agent.speed = 1;
                    animatorOverrideController["BigManAnim"] = animationClips[0];
                }
            }
            else
            {
                if (attacking)//down
                {
                    agent.speed = 0.1f;
                    animatorOverrideController["BigManAnim"] = animationClips[9];
                }
                else if (chasing)
                {
                    agent.speed = 2.5f;
                    animatorOverrideController["BigManAnim"] = animationClips[5];
                }
                else
                {
                    agent.speed = 1;
                    animatorOverrideController["BigManAnim"] = animationClips[1];
                }
            }
        }
        Vector3 pos = transform.position;
        pos.z = -0.5f;
        transform.position = pos;
    }


    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (sm != null)
        {
            GameObject go = gameObject;
            if (currState != null)
                currState.OnCollideEnter(ref go, collision);
        }
    }

    public ExampleStateMachine SM
    {
        get { return sm; }
        set { sm = value; }
    }
    public Vector2 PlayerLastSeenPos
    {
        get { return playerLastSeenPos; }
        set { playerLastSeenPos = value; }
    }
    public bool PatrolPlayerLastSeen
    {
        get { return patrolPlayerLastSeen; }
        set { patrolPlayerLastSeen = value; }
    }
    public bool GoingQueue
    {
        get { return goingQueue; }
        set { goingQueue = value; }
    }
    public List<Vector2> GetQueueDest
    {
        get { return DestQueue; }
        set { DestQueue = value; }
    }

    public float CalculateNextSpawnTime()
    {
        float distance = 0;
        if(patrolPlayerLastSeen)
            distance = (GetComponent<Rigidbody2D>().position - GetPlayer.GetComponent<Rigidbody2D>().position).magnitude;
        else
        {
            distance = (GetComponent<Rigidbody2D>().position - GetPathNodePos(currentNavID)).magnitude;
            distance += (GetPathNodePos(currentNavID) - GetPlayer.GetComponent<Rigidbody2D>().position).magnitude;
        }
        return (distance / agent.velocity.magnitude);
        
    }
}
