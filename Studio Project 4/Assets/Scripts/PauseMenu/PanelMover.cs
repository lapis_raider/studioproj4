﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMover : MonoBehaviour {

    private GameObject panelToMove;
    [SerializeField]
    private Panel[] panels;

    [SerializeField]
    private float lerpSpeed = 1;

    private int previousIndex = 0;

	// Use this for initialization
	void Start () {
        panelToMove = this.gameObject;
        panels = FindObjectsOfType<Panel>();
       // panelToMove.transform.position
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        ////Check if the panel is at the same position at which index
        //for (int i = 0; i < panels.Length; i++)
        //{
        //    if (panels[i].rt.position == panelToMove.transform.position)
        //    {
        //        previousIndex = i;
        //    }
        //}

    //Then we attempt to move?
        for(int i =0; i < panels.Length; i++)
        {
            if(panels[i].hovered)
            {
                previousIndex = i;
                //return;
            }
        }

        panelToMove.transform.position = new Vector3(panelToMove.transform.position.x,
        Mathf.Lerp(panelToMove.transform.position.y, panels[previousIndex].rt.position.y, lerpSpeed * Time.deltaTime));

        //Attempt the lerp, then we try to push it.
        if (Mathf.Abs(panels[previousIndex].rt.position.y - panelToMove.transform.position.y) < 1f)
        {
            panelToMove.transform.position = new Vector3(panelToMove.transform.position.x,
                panels[previousIndex].rt.transform.position.y);
        }

        //Else there is no hover.. so we go back to the index.
        //We attempt to lerp to the previousIndex;
        //    panelToMove.transform.position = new Vector3(panelToMove.transform.position.x,
        //Mathf.Lerp(panelToMove.transform.position.y, panels[previousIndex].rt.position.y, lerpSpeed * Time.deltaTime));

        //    if (Mathf.Abs(panels[previousIndex].rt.position.y - panelToMove.transform.position.y) < 1f)
        //    {
        //        panelToMove.transform.position = new Vector3(panelToMove.transform.position.x,
        //            panels[previousIndex].rt.transform.position.y);
        //    }

    }
}
