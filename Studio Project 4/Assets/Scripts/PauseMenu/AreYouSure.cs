﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreYouSure : MonoBehaviour {

    [Header("Are you sure components")]
    [SerializeField]
    private Text exitText;
    [SerializeField]
    public string[] texts;

    public void GenerateText()
    {
        exitText.text = texts[Random.Range(0, texts.Length)];
    }
}
