﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoPause : MonoBehaviour {

    private bool isPaused = false;
    private bool changed = false;
    public string pauseScene = "PauseScene";

    SceneManagement sceneManager;

    void Start()
    {
        sceneManager = GetComponent<SceneManagement>();
    }

	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            string name = SceneManager.GetActiveScene().name;
            if (name != "Credits" && 
                name != "MainMenu" &&
                name != "PauseScene" &&
                name != "WinLoseScene" &&
                name != "InGameOption" &&
                name != "InstructionsScene" &&
                name != "StartScreen")
            {
                isPaused = !isPaused;
                changed = true;
            }
        }

        if (changed)
        {
            if (isPaused)
            {
                GameDataManager.Instance.isPaused = true;
                GameDataManager.Instance.doMovement = false;
                sceneManager.LoadSceneAdditive(pauseScene);
            }
            else
            {
                GameDataManager.Instance.isPaused = false;
                GameDataManager.Instance.doMovement = true;
                sceneManager.UnloadScene(pauseScene);
            }
            changed = false;
        }
	}

    //Mobile function.
    public void PauseGame()
    {
        if (GameDataManager.Instance.isPaused)
            return;

        isPaused = !isPaused;
        changed = true;
        sceneManager.LoadSceneAdditive(pauseScene);
    }
}
