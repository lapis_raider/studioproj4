﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public string mainMenuScene = "";
    public string optionScene = "";
    public string pauseScene = "";

    private AreYouSure sure;

    [Header("Selection GameObject")]
    public GameObject selectionObject;

    [Header("Selection Buttons")]
    public GameObject buttonResume;
    public GameObject buttonOptions;
    public GameObject buttonMainMenu;

    [Header("Are you sure?")]
    public GameObject areYouSure;

    [Header("PausePanel")]
    public GameObject pausePanel;

    // Use this for initialization
    void Start() {
        sure = GetComponent<AreYouSure>();
    }

    // Update is called once per frame
    void Update() {

    }

    public void Return()
    {
        GameDataManager.Instance.isPaused = false;
        GameDataManager.Instance.doMovement = true;
        SceneManager.UnloadSceneAsync(pauseScene);
    }

    public void Options()
    {
        GameDataManager.Instance.isPaused = false;
        GameDataManager.Instance.doMovement = true;
        //SceneManager.UnloadSceneAsync(pauseScene);
        GetComponent<KeybindsData>().inOptions = true;
        SceneManager.LoadScene(optionScene, LoadSceneMode.Additive);
    }
    public void MainMenu()
    {
        areYouSure.SetActive(true);
        pausePanel.SetActive(false);
        sure.GenerateText();
    }

    public void ReturnAreYouSure()
    {
        areYouSure.SetActive(false);
        pausePanel.SetActive(true);
    }

    public void GoToMainMenu()
    {
        GameDataManager.Instance.isPaused = false;
        GameDataManager.Instance.doMovement = true;
        SceneManager.UnloadSceneAsync(pauseScene);
        SceneManager.LoadScene(mainMenuScene);
        Destroy(GameDataManager.Instance);
    }
}
