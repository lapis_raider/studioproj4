﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneUnloader : MonoBehaviour {

    public string sceneName = "";

    [Header("The pause menu object")]
    public GameObject pauseObject;

    public void UnloadScene()
    {
        pauseObject.SetActive(true);
        SceneManager.UnloadSceneAsync(sceneName);
    }
}
