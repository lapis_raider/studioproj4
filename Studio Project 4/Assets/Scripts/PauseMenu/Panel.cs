﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Panel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    private Button theButton;

    [HideInInspector]
    public RectTransform rt;

    //[HideInInspector]
    public bool hovered = false;

	// Use this for initialization
	void Start () {
        theButton = GetComponent<Button>();
        rt = theButton.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void OnPointerEnter(PointerEventData eventData)
    { 
        hovered = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {     
        hovered = false;
    }



}
