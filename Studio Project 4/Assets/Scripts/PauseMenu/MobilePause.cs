﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobilePause : MonoBehaviour {

    GameDataManager gameManager;
    SceneManagement sceneManager;

    private bool isPaused = false;
    private bool changed = false;
    public string pauseScene = "PauseScene";

    // Use this for initialization
    void Start() {
        gameManager = FindObjectOfType<GameDataManager>();
        sceneManager = gameManager.GetComponent<SceneManagement>();
    }

    public void ClickPauseGame()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            GameDataManager.Instance.isPaused = true;
            GameDataManager.Instance.doMovement = false;
            sceneManager.LoadSceneAdditive(pauseScene);
        }
        else
        {
            GameDataManager.Instance.isPaused = false;
            GameDataManager.Instance.doMovement = true;
            sceneManager.UnloadScene(pauseScene);
        }
    }
}
