﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCull : MonoBehaviour {

    [SerializeField]
    private Light[] minimapLights;

    [SerializeField]
    private float minimapLightIntensity = 0.5f;

    private void Start()
    {
        foreach (Light light in minimapLights)
        {
            if (light != null)
            {
                light.intensity = minimapLightIntensity;
            }
        }
    }

    private void Update()
    {
        //Just in case it changes?
        foreach (Light light in minimapLights)
        {
            if (light != null)
            {
                light.intensity = minimapLightIntensity;
            }
        }
    }

    private void OnPreCull()
    { 
        foreach (Light light in minimapLights)
        {
            if (light != null)
            {
                light.enabled = false;
            }
        }
        
    }

    private void OnPreRender()
    {
        foreach (Light light in minimapLights)
        {
            if (light != null)
            {
                light.enabled = false;
            }
        }
    }

    private void OnPostRender()
    {
        foreach (Light light in minimapLights)
        {
            if (light != null)
            {
                light.enabled = true;
            }
        }
    }
}
