﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileUI : MonoBehaviour {

    public GameObject mobileLayout;
    public GameObject PCLayout;

    // Use this for initialization
    void Start () {
        //Windows defaults
        PCLayout.SetActive(true);
        mobileLayout.SetActive(false);

        //Not windows defaults? 
#if UNITY_ANDROID
          PCLayout.SetActive(false);
        mobileLayout.SetActive(true);
#endif
    }


}
