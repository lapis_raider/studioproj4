﻿using UnityEngine;

[System.Serializable]
public class PlayerVisor {

    // Use this for initialization
    public float MAX_BATTERY = 20.0f;
   
    public float DECREASE_RATE = 2.0f;
    public Material material;

    public string tagName = "clues"; //the layer the clues are on, to make them active

    [HideInInspector]
    public bool visorActive = false;
    [HideInInspector]
    public float batteryLife;

    public delegate void OnVisorActivate();
    public OnVisorActivate OnVisorActivateCallBack;


    public void Init()
    {
        batteryLife = MAX_BATTERY;
        GameDataManager.Instance.playerData.visor.visorActive = false;
        material.SetInt("_OutlineSize", 0);
        material.SetColor("_ColorOutline", new Color(0,0,0,0));
    }

    public void Update () {

        if (GameDataManager.Instance.isPaused)
            return;

        if (visorActive)
        {
            batteryLife -= Time.deltaTime * DECREASE_RATE;
            if (batteryLife <= 0.0f)
            {
                batteryLife = 0.0f;
                visorActive = false;
                SwitchOff();
                OnVisorActivateCallBack.Invoke();
            }
        }
	}

    public void Activate()
    {
        if (batteryLife <= 0.0f) //no battery to activate or do anything
            return;

        visorActive = !visorActive;

        if (visorActive) //if activate it
            SwitchOn();
        else
            SwitchOff();

        OnVisorActivateCallBack.Invoke();
    }

    void SwitchOn()
    {
        //searches for all interactive objects, and turn on their lights
        GameObject[] clues = GameObject.FindGameObjectsWithTag(tagName);

        for (int i =0; i < clues.Length; ++i) //make the sprite active
        {
            clues[i].gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }

      //make material glow
      material.SetInt("_OutlineSize", 10);
        material.SetColor("_ColorOutline", new Color(1, 0, 0, 1));
    }

    void SwitchOff()
    {
        //gets all interactive objects and turn off the lights
        GameObject[] clues = GameObject.FindGameObjectsWithTag(tagName);

        for (int i = 0; i < clues.Length; ++i)
        {
            clues[i].gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        //make material not glow
        material.SetInt("_OutlineSize", 0);
        material.SetColor("_ColorOutline", new Color(0, 0, 0, 0));
    }
}
