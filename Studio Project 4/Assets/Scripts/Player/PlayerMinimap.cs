﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMinimap : MonoBehaviour {

    [SerializeField]
    private GameObject minimapObject;

    private bool isMinimapOpen;
	// Use this for initialization
	void Start () {
        isMinimapOpen = false;
        minimapObject.SetActive(isMinimapOpen);
	}
	
	// Update is called once per frame
	void Update () {
        //change to minimap key

        //if(Input.GetKeyDown(KeyCode.M))
        if ((Input.GetKeyDown(GameDataManager.Instance.keys["Minimap"])))
        {
            isMinimapOpen = !isMinimapOpen;
            minimapObject.SetActive(isMinimapOpen);
            //set the size of the object to be the size of the children

            RectTransform rt = minimapObject.GetComponent<RectTransform>();
            RectTransform thatRt = minimapObject.GetComponentInChildren<RectTransform>();

            rt.localScale = thatRt.localScale;
        }

        //Can do alpha fade in?
	}

    public void TurnOnMinimap()
    {
        isMinimapOpen = !isMinimapOpen;
        minimapObject.SetActive(isMinimapOpen);
        //set the size of the object to be the size of the children

        RectTransform rt = minimapObject.GetComponent<RectTransform>();
        RectTransform thatRt = minimapObject.GetComponentInChildren<RectTransform>();

        rt.localScale = thatRt.localScale;

        AudioManager.instance.Play("minimap");
    }
}
