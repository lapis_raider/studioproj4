﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    // Use this for initialization
    Rigidbody2D rigidbdy;
    public float speed;
    [HideInInspector]
    public Vector3 dir;
    Vector3 thrownDir;
    public float torqueResistance = 0.5f;

    Interact focusedEntity;

    [HideInInspector]
    public bool hiding;
    public GameObject interactSprite;
    float defaultSpeed;
    Animator animator;
    string animatorState;
    void Start()
    {
        rigidbdy = this.GetComponent<Rigidbody2D>();
        rigidbdy.gravityScale = 0;
        hiding = false;
        animator = GetComponent<Animator>();
        animatorState = "playerUp";
        defaultSpeed = animator.speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameDataManager.Instance.isPaused)
        {
            rigidbdy.velocity = Vector3.zero;
            return;
        }

#if !UNITY_ANDROID
        //if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        if (Input.GetKey(GameDataManager.Instance.keys["MoveUp"]) ||
            Input.GetKey(GameDataManager.Instance.keys["MoveDown"]) ||
             (Input.GetKey(GameDataManager.Instance.keys["MoveRight"]) ||
             (Input.GetKey(GameDataManager.Instance.keys["MoveLeft"]))))
        {
            Move();
        }
        else
        {
            rigidbdy.velocity = Vector3.zero;
            animator.Play(animatorState, 0, 0.1f);
            animator.speed = 0;
        }
#endif

        if (Input.GetKeyDown(GameDataManager.Instance.keys["Interact"]))
            Interact();

        ScrollItem();

        if (Input.GetKeyDown(GameDataManager.Instance.keys["Action"]))
            Action();

        if (Input.GetKeyDown(GameDataManager.Instance.keys["Visor"])) // not yet implemeneted, dont uncomment
            Visor();

        // Enable/Disable interact sprite
        bool shouldSetActive = false;
        GameObject[] objectList = GameObject.FindGameObjectsWithTag("Interactable");
        for (int i = 0; i < objectList.Length; ++i)
        {
            Interact interactEntity = objectList[i].GetComponent<Interact>();
            if (!interactEntity)
                continue;

            if (Vector2.SqrMagnitude((Vector2)transform.position - (Vector2)interactEntity.transform.position) <= interactEntity.interactRadius * interactEntity.interactRadius)
            {
                shouldSetActive = true;
                break;
            }
        }
        interactSprite.SetActive(shouldSetActive);

        UpdateObjects();
        GameDataManager.Instance.playerData.visor.Update();
    }

    // Controls, called by either keyboard/mouse input or touch
    public void Move()
    {
        if (GameDataManager.Instance.doMovement)
        {
            //movement
            Vector2 dirPressed = Vector2.zero;

            if (Input.GetKey(GameDataManager.Instance.keys["MoveUp"]))
            {
                dirPressed = new Vector2(dirPressed.x, 1.0f);
            }

            if (Input.GetKey(GameDataManager.Instance.keys["MoveDown"]))
            {
                dirPressed = new Vector2(dirPressed.x, -1.0f);
            }

                if (Input.GetKey(GameDataManager.Instance.keys["MoveRight"]))
                {
                    dirPressed = new Vector2(1.0f, dirPressed.y);
                }

                    if (Input.GetKey(GameDataManager.Instance.keys["MoveLeft"]))
                    {
                        dirPressed = new Vector2(-1.0f, dirPressed.y);
                    }

            //force = new Vector2(dirPressed.x * Time.deltaTime * speed + force.x, dirPressed.y * Time.deltaTime * speed + force.y);

            //force.x = Mathf.Clamp(force.x, -torqueResistance, torqueResistance);
            //force.y = Mathf.Clamp(force.y, -torqueResistance, torqueResistance);
            //dir = force.normalized;
            dir = new Vector2(dirPressed.x * Time.deltaTime * speed, dirPressed.y * Time.deltaTime * speed).normalized;
            thrownDir = dir;

            SpriteAnimation();

            rigidbdy.velocity = dir * speed;
        }
        else
        {
            rigidbdy.velocity = Vector2.zero;
            dir = Vector3.zero;
        }
    }

    public void SpriteAnimation()
    {
        if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
        {
            animator.speed = defaultSpeed;
            if (dir.x > 0)
            {
                animator.SetBool("right", true);
                animator.SetBool("left", false);
                animator.SetBool("up", false);
                animator.SetBool("down", false);
                animatorState = "playerRight";
            }
            else
            {
                animator.SetBool("right", false);
                animator.SetBool("left", true);
                animator.SetBool("up", false);
                animator.SetBool("down", false);
                animatorState = "playerLeft";
            }
        }
        else if (Mathf.Abs(dir.y) > Mathf.Abs(dir.x))
        {
            animator.speed = defaultSpeed;
            if (dir.y > 0)
            {
                animator.SetBool("right", false);
                animator.SetBool("left", false);
                animator.SetBool("up", true);
                animator.SetBool("down", false);
                animatorState = "playerUp";

            }
            else
            {
                animator.SetBool("right", false);
                animator.SetBool("left", false);
                animator.SetBool("up", false);
                animator.SetBool("down", true);
                animatorState = "playerDown";

            }
        }
    }

    public void Move(Vector3 dir) // Overloaded for virtual joystick movement
    {
        if (GameDataManager.Instance.doMovement)
        {
            this.dir = dir;
            if (!dir.Equals(Vector3.zero))
                thrownDir = dir;

            SpriteAnimation();

            rigidbdy.velocity = dir * speed;
        }
        else
        {
            rigidbdy.velocity = Vector2.zero;
        }
    }

    public void Interact()
    {
        DialogueManager manager = FindObjectOfType<DialogueManager>();
        if(!manager.HasDialogueEnded())
        {
            manager.AttemptSkipDialogue();
            return; // return?
        }
        
        GameObject[] go = GameObject.FindGameObjectsWithTag("Interactable");
       
        for (int i = 0; i < go.Length; ++i)
        {
            Interact interactEntity = go[i].GetComponent<Interact>();

            if (Vector2.SqrMagnitude((Vector2)transform.position - (Vector2)interactEntity.transform.position) <= interactEntity.interactRadius * interactEntity.interactRadius)
            {
                SetFocus(interactEntity);
                return;
            }
        }
    }

    public void ScrollItem()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) //switch usable item
            GameDataManager.Instance.playerData.usableItemInventory.SwapCurrentItem(1);
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            GameDataManager.Instance.playerData.usableItemInventory.SwapCurrentItem(-1);
    }
    public void ScrollItem(bool direction) // Overload for ontouch controls
    {
        if (direction)
            GameDataManager.Instance.playerData.usableItemInventory.SwapCurrentItem(1);
        else
            GameDataManager.Instance.playerData.usableItemInventory.SwapCurrentItem(-1);
    }

    public void Action()
    {
        if (!hiding)
            UseItem(transform.position, thrownDir);
    }

    public void Visor()
    {
        GameDataManager.Instance.playerData.visor.Activate();
        AudioManager.instance.Play("VisorOnOff");
    }

    void SetFocus(Interact newInteractEntity)
    {
        if (focusedEntity != null) //if theres an entity, remove it
            RemoveFocus();

        focusedEntity = newInteractEntity;
        focusedEntity.isSelected(transform);
    }

    void RemoveFocus()
    {
        focusedEntity.isUnselected();

        focusedEntity = null;

    }

    void UseItem(Vector3 position, Vector3 dir)
    {
        List<ItemUsable> items = GameDataManager.Instance.playerData.usableItemInventory.items;
        int current = GameDataManager.Instance.playerData.usableItemInventory.currentItem;

        if (current >= items.Count)
            return;

        if (items[current].startCoolDown) //if obj still in cool down mode
            return;

        GameObject obj = Instantiate(items[current].prefab);
        obj.transform.position = new Vector3(transform.position.x, transform.position.y, -5.0f);
        obj.GetComponent<ItemAction>().BeingUsed(dir);

        if (!items[current].reusable)
            GameDataManager.Instance.playerData.usableItemInventory.RemoveCurrentItem();
        else
            items[current].startCoolDown = true;
    } 

    void UpdateObjects()
    {
        List<ItemUsable> items = GameDataManager.Instance.playerData.usableItemInventory.items;
        for (int i =0; i < items.Count; ++i)
        {
            if (!items[i].reusable || !items[i].startCoolDown)
                continue;

            items[i].currTimer += Time.deltaTime;
            if (items[i].currTimer >= items[i].coolDownTimer)
            {
                items[i].startCoolDown = false;
                items[i].currTimer = 0.0f;
            }
        }
    }
}
