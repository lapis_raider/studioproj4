﻿using UnityEngine;

public class FollowRotation : MonoBehaviour {

    Transform rotation;
    Vector3 direction = new Vector3(0, 1, 0);

    public PlayerMovement player;
    public float smooth = 5f;

	// Use this for initialization
	void Start ()
    {
        rotation = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!player.dir.Equals(Vector3.zero))
        {
            direction = player.dir;
        }
        
        Quaternion target = Quaternion.LookRotation(direction, transform.up);
        Vector3 euler = Quaternion.Slerp(rotation.rotation, target, Time.deltaTime * smooth).eulerAngles;
        rotation.localEulerAngles = euler;
    }
}
