﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

    Transform cameraTransform;
    public PlayerMovement player;
    public float smoothValue;

    [System.Serializable]
    public struct CameraMinMax
    {
        public Vector2 min;
        public Vector2 max;
    }
    public CameraMinMax[] cameraClamps;

    [HideInInspector]
    public bool followWoman = false;
    public float womanZSize = 2.5f;
    public float playerZsize = 6.45f;
    GameObject smallWoman = null;
	
	[HideInInspector]
    public GameObject objectFollow = null;
    [HideInInspector]
    public bool followingObject = false;
    [HideInInspector]
    public float objectZSize = 0;
	
    float lerpTime;

    // Use this for initialization
    void Start () {
        cameraTransform = GetComponent<Transform>();
        smallWoman = GameObject.FindGameObjectWithTag("SmallWoman");
	}

    void LateUpdate()
    {
        if(smallWoman == null)
            smallWoman = GameObject.FindGameObjectWithTag("SmallWoman");
        Vector3 lerpPos;
        if (!followingObject)
        {
            if (followWoman)
                lerpPos = smallWoman.transform.position;
            else
                lerpPos = player.GetComponent<Transform>().localPosition;
           
            lerpPos.z = cameraTransform.position.z;

            cameraTransform.position = Vector3.Lerp(cameraTransform.position, lerpPos, smoothValue);

            if (!followWoman)
            {
                cameraTransform.position = new Vector3(Mathf.Clamp(cameraTransform.position.x, cameraClamps[(int)GameDataManager.Instance.playerData.level].min.x, cameraClamps[(int)GameDataManager.Instance.playerData.level].max.x),
                                                   Mathf.Clamp(cameraTransform.position.y, cameraClamps[(int)GameDataManager.Instance.playerData.level].min.y, cameraClamps[(int)GameDataManager.Instance.playerData.level].max.y),
                                                   cameraTransform.position.z);
                if (Camera.main.orthographicSize != playerZsize)
                    Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, playerZsize, smoothValue);
            }
            else if (followWoman)
                Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, womanZSize, smoothValue);
        }
        else
        {
            lerpPos = objectFollow.GetComponent<Transform>().position;
            lerpPos.z = cameraTransform.position.z;
            cameraTransform.position = Vector3.Lerp(cameraTransform.position, lerpPos, smoothValue);
            Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, objectZSize, smoothValue);
        }
    }
    
}
