﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour {

    public float viewRadius;
    [Range(0,360)]
    public float viewAngle;

    public LayerMask targetMask; //obj layer
    public LayerMask obstacleMask; //obstacle layer

    public List<Transform> visibleTargets = new List<Transform>();

    private void Start()
    {
        StartCoroutine("FindTargets", 0.2f);
    }

    IEnumerator FindTargets(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }


    void FindVisibleTargets()
    {
        if (GameDataManager.Instance.playerData.visor.visorActive)
            return;

        visibleTargets.Clear(); //clear the target

        Collider2D[] targetsInViewRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, targetMask);

        for (int i=0; i < targetsInViewRadius.Length; ++i)
        {
            targetsInViewRadius[i].gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Transform target = targetsInViewRadius[i].transform; //get target info

            Vector3 dirToTarget = (target.position - transform.position).normalized; //get dir from player to dir

            if (Vector2.Angle(transform.forward, dirToTarget) < viewAngle / 2) //check dot product angle is it within the angle or not
            {
                float distToTarget = Vector3.Distance(target.position, transform.position);

                if (!Physics2D.Raycast(transform.position, new Vector2(dirToTarget.x, dirToTarget.y), distToTarget, obstacleMask))//no obstacle in the way, can see target
                {
                    visibleTargets.Add(target); //add the targets we see
                    targetsInViewRadius[i].gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    continue;
                }
            }
        }
    }

    void UpdateVisibleTarget()
    {

        //visibleTargets[0].gameObject.SetActive(true);
    }

    public Vector3 DirectionFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        angleInDegrees -= Mathf.Atan2(transform.forward.y, transform.forward.x) * Mathf.Rad2Deg - 90;
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0);
    }
}
