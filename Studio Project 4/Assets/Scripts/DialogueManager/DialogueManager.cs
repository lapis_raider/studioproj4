﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    //The text header of the dialogue
    private Text textHeader;
    //The text description of the dialogue
    private Text textDescription;
    //Not used anymore.
    //private Image dialogueImage;

    //The animator to animate the dialogue
    private Animator animator;
        
    public float pauseTimer;

    //The text speed
    [SerializeField]
    private float textSpeed;

    public float defaultSpeed;

    public bool hasReachedEnd;

    //Queue of sentences.
    private Queue<string> sentences;

    //The dialogue object(Name:ChatBorder)
    public GameObject dialogueObject;

    //Has the dialogue ended?
    bool endedDialogue = true;
    
    string sentence;

    // Use this for initialization
    void Awake()
    {
        textSpeed = defaultSpeed;
        animator = dialogueObject.GetComponent<Animator>();
        sentences = new Queue<string>();

        //Get the component
        Text[] textComponents;
        textComponents = dialogueObject.GetComponentsInChildren<Text>();

        foreach(Text text in textComponents)
        {
            if(text.tag == "TextHeader")
            {
                textHeader = text;
            }
            else if (text.tag == "TextDescription")
            {
                textDescription = text;
            }
        }
    }

    private void Update()
    {
        ////Change with interact button

        //if (Input.GetKeyDown(KeyCode.E))
        if ((Input.GetKeyDown(GameDataManager.Instance.keys["Interact"])))
        {
            AttemptSkipDialogue();
        }

        if (!endedDialogue)
        {

        //#if UNITY_ANDROID
        //                //Touch touch = Input.GetTouch(0);

        //                // if (touch.phase == TouchPhase.Ended)
        //                // {
        //                //      AttemptSkipDialogue();
        //                // }
     
        //#endif

            ////Quite bad but okay? I can probably do something about it later
            //if (Input.GetMouseButtonDown(0))
            //{
            //    AttemptSkipDialogue();
            //}
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().enabled = false;
        GameDataManager.Instance.doMovement = false;
        endedDialogue = false;
        //Debug.Log("Dialogue started with header: " + dialogue.textHeader);

        animator.SetBool("IsOpen", true);
        ButlerInfo.butlerStop = true;
        BigmanInfo.BigManStop = true;
        MouseInfo.mouseStop = true;
        //Set the image too
        //dialogueImage.sprite = dialogue.dialogueIcon;
        textHeader.text = dialogue.textHeader;
        textDescription.text = "";

        //Clear any sentences that might have been there
        sentences.Clear();

        //Add all the sentences in the dialogue given to the queue
        foreach (string sentence in dialogue.sentences)
        { 
            sentences.Enqueue(sentence);
        }
        //Show the first sentence first?
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        hasReachedEnd = false;
        //If the queue for the sentences are empty then
        if (sentences.Count <= 0)
        {
            EndDialogue();
            return;
        }

        //Else there is a queue so
        sentence = sentences.Dequeue();
       //Debug.Log(sentence);
        //Stop the coroutine for safety
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));       
    }

    IEnumerator TypeSentence (string sentence)
    {
        textDescription.text = "";
        foreach(char letter in sentence.ToCharArray())
        {
            textDescription.text += letter;
            if (textDescription.text == sentence)
            {
                hasReachedEnd = true;
            }
            if (!GameDataManager.Instance.isPaused)
            {
                if (textSpeed > 0)
                {
                    yield return new WaitForSeconds(textSpeed);
                }
                else
                {
                    yield return null;
                }
            }
        }
    }

    void EndDialogue()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().enabled = true;
        //Debug.Log("End of conversation in text instance");
        animator.SetBool("IsOpen", false);
        endedDialogue = true;
        ButlerInfo.butlerStop = false;
        BigmanInfo.BigManStop = false;
        MouseInfo.mouseStop = false;
        if (!GameObject.FindWithTag("Player").GetComponent<PlayerMovement>().hiding)
            GameDataManager.Instance.doMovement = true;
    }

    public bool HasDialogueEnded()
    {
        return endedDialogue;
    }

    public void AttemptSkipDialogue()
    {
        if (!hasReachedEnd)
        {
            textDescription.text = sentence;
            hasReachedEnd = true;
            StopAllCoroutines();
        }
        else
        {
            DisplayNextSentence();
        }
    }

}
