﻿using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

    public Dialogue dialogue;

    public void TriggerDialogue()
    {
        if (FindObjectOfType<DialogueManager>().HasDialogueEnded())
            FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}
