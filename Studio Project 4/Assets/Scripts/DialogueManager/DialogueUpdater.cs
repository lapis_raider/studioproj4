﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueUpdater : MonoBehaviour {

    DialogueManager dialogueManager;
    Animator animator;
    
	// Use this for initialization
	void Start () {
       dialogueManager = GetComponent<DialogueManager>();
       animator = dialogueManager.dialogueObject.GetComponent<Animator>();
	}	
}
