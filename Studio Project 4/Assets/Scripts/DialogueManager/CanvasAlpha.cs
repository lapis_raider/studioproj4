﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasAlpha : MonoBehaviour {

    CanvasGroup canvasGroup;
    public bool fadeIn;
    public float alphaStart;

    public float minThreshold;
    public float maxThreshold;

    private bool alphaSwap = false;

	// Use this for initialization
	void Start () {
        canvasGroup = GetComponent<CanvasGroup>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if(alphaStart > maxThreshold)
        {
            alphaSwap = true;
        }
        else if(alphaStart < minThreshold)
        {
            alphaSwap = false;
        }

        alphaStart = alphaSwap ? alphaStart - Time.deltaTime : alphaStart + Time.deltaTime;


        //Set the canvas group alpha
        canvasGroup.alpha = alphaStart;

    }
}
