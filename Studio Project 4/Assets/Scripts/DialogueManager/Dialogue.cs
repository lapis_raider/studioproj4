﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[System.Serializable]
//public class Dialogue
//{
//    public string textHeader;

//    [TextArea(3, 10)]
//    public string[] sentences;
//}

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogues/Dialogue")]
public class Dialogue : ScriptableObject
{
    //Every dialogue needs a textHeader;
    public string textHeader;

    //The text area for the editor?
    [TextArea(3, 10)]
    public string[] sentences;

   // The sprite icon of the dialogue
    public Sprite dialogueIcon;


}