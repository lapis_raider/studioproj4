﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class MainMenu : MonoBehaviour {

    //The animator to animate the camera
    Animator cameraAnimator;

    [SerializeField]
    private OptionsMenu optionsMenu;

    [Header("Sub Panels")]
    [Tooltip("The subPanel")]
    public GameObject selectionPanel;
    [Tooltip("The subPanel2")]
    public GameObject selectionPanel2;


    [Header("Game Scene To Load")]
    [Tooltip("The game scene to load when newGameButton is clicked")]
    public string sceneName = "";
    [Tooltip("The instructions scene to load when instructionsButton is clicked")]
    public string instructionSceneName = "";

    [Header("MainMenu")]
    [Tooltip("New Game Button")]
    public GameObject playButton;
    [Tooltip("Settings Button")]
    public GameObject settingsButton;
    [Tooltip("Quit Button")]
    public GameObject quitButton;
    [Tooltip("When the play button is pressed, this determines the fade speed of other options.")]
    public float fadeSpeed;

    [Header("SubMenu")]
    [Tooltip("New Game Button")]
    public GameObject newGameButton;
    [Tooltip("Tutorial Button")]
    public GameObject tutorialButton;
    [Tooltip("When the play button is pressed, this determines the fade of the new and tutorial buttons.")]
    public float subFadeSpeed;

    [Header("Max and Min Alpha")]
    [Tooltip("The min alpha that each button will lerp back to.")]
    public float minAlpha = 0.5f;
    [Tooltip("The max alpha that each button will lerp back to.")]
    public float maxAlpha = 1.0f;

    //Private variables to make the thing more intuitive
    //BRIEF: Checks if the play button has been pressed.
    public bool isPlay = false;

    //Start method
    void Start()
    {
        cameraAnimator = transform.GetComponent<Animator>();
    }

    void Update()
    {
        //The fade function of other variables.
        IsPlayPressed();

        if(Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene("MusicMinigame"); 
        }
    }

    //Function to start game
    public void StartGame()
    {
        isPlay = !isPlay;
        //Set some stuff to active
        newGameButton.gameObject.SetActive(isPlay);
        tutorialButton.gameObject.SetActive(isPlay);
        selectionPanel.GetComponent<SpriteRenderer>().enabled = isPlay;
        selectionPanel2.GetComponent<SpriteRenderer>().enabled = isPlay;
    }

    //Function to close the play buttons
    public void DisableStartGame()
    {
        newGameButton.gameObject.SetActive(false);
        tutorialButton.gameObject.SetActive(false);
        selectionPanel.GetComponent<SpriteRenderer>().enabled = false;
        selectionPanel2.GetComponent<SpriteRenderer>().enabled = false;
        isPlay = false;
    }

    public void LoadGame()
    {
        if (sceneName != "")
        {
           //Debug.Log("Attempting to load game scene!");
            try
            {
                SceneManager.LoadScene(sceneName);
            }
            catch (Exception exception)
            {
                //Debug.Log(exception);
            }
        }
    }

    public void LoadInstructionsScene()
    {
        if (instructionSceneName != "")
        {
            //Debug.Log("Attempting to load instructions scene!");
            try
            {
                SceneManager.LoadScene(instructionSceneName, LoadSceneMode.Single);
            }
            catch (Exception exception)
            {
                //Debug.Log(exception);
            }
        }
    }

    public void MoveCameraSettings()
    {
    
        cameraAnimator.SetBool("DoAnim", true);
        DisableStartGame();
        optionsMenu.settingsChanged = false;
    }

    public void MoveCameraMain()
    {
        cameraAnimator.SetBool("DoAnim", false);
        optionsMenu.settingsChanged = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void IsPlayPressed()
    {
        CanvasGroup cg = new CanvasGroup();
        if (isPlay)
        {       
            //Settings Button
            cg = settingsButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, minAlpha, fadeSpeed * Time.deltaTime);
            //Quit Button
            cg = quitButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, minAlpha, fadeSpeed * Time.deltaTime);
            //New Game Button
            cg = newGameButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, subFadeSpeed * Time.deltaTime);
            //Highscore Button
            cg = tutorialButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, subFadeSpeed * Time.deltaTime);
        }
        else
        {
            //Settings Button
            cg = settingsButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, fadeSpeed * Time.deltaTime);
         
            //Quit Button
            cg = quitButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, fadeSpeed * Time.deltaTime);

            //New Game Button
            cg = newGameButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, minAlpha, subFadeSpeed * Time.deltaTime);
            //Highscore Button
            cg = tutorialButton.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, minAlpha, subFadeSpeed * Time.deltaTime);
        }
    }

    public void PlaySound(string soundName)
    {
        AudioManager.instance.Play(soundName);
    }
}
