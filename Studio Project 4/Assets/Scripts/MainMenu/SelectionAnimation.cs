﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectionAnimation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    [SerializeField]
    private Image image;

    private RectTransform rt;

    [SerializeField]
    private float targetWidth;

    [SerializeField]
    private float speed;

    private bool doAnim = false;

    public bool stay = false;

    public GameObject[] others;

	// Use this for initialization
	void Start () {
        rt = image.GetComponent<RectTransform>();
	}

    void Update()
    {
        Color testColor = image.color;
        testColor.a = 0.25f;

        if (stay)
        {
            testColor.a = 0.59f;
            image.color = testColor;
            rt.sizeDelta = new Vector3(Mathf.Lerp(rt.sizeDelta.x, targetWidth, speed * Time.deltaTime), rt.sizeDelta.y);
            return;
        }
        image.color = testColor;
        //Set color


        if (doAnim)
        {
            rt.sizeDelta = new Vector3(Mathf.Lerp(rt.sizeDelta.x, targetWidth, speed * Time.deltaTime), rt.sizeDelta.y);
        }
        else
        {
            rt.sizeDelta = new Vector3(Mathf.Lerp(rt.sizeDelta.x, 0, speed * Time.deltaTime), rt.sizeDelta.y);
        }

    }
	
   public void OnPointerEnter(PointerEventData eventData)
    {
        doAnim = true;
    }

  public  void OnPointerExit(PointerEventData eventData)
    {
        doAnim = false;
    }


    public void Enabled()
    {
        stay = true;
        foreach(GameObject objects in others)
        {
            objects.GetComponent<SelectionAnimation>().stay = false;
        }
    }

}
