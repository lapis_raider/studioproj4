﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionScript : MonoBehaviour {

    SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    //Used for animation functions
    public void SetActive()
    {
        spriteRenderer.enabled = true;
    }

    //Used for animation functions
    public void SetInActive()
    {
       // spriteRenderer.enabled = false;
    }
}
