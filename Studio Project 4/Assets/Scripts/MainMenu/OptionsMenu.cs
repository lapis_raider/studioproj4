﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//APPLIED ON OPTION SCREEN CANVAS
public class OptionsMenu : MonoBehaviour {

    private WindowManager windowManager;
    private AudioManager soundManager;

    [SerializeField]
    private KeybindsData keybindManager;

    //These are the gameObjects that will store the stuff
    // [Tooltip("These are the attributes that store the game panel")]
    [Header("Game Panel")]
    [Tooltip("The button to trigger the game menu")]
    public GameObject gameButton;
    [Tooltip("The game panel")]
    public GameObject gamePanel;

    //    [Tooltip("These are the attributes of the audio menu")]
    [Header("Audio Panel")]
    [Tooltip("The game object for the music slider")]
    public GameObject musicSlider;
    [Tooltip("The game object for the sfx slider")]
    public GameObject sfxSlider;
    [Tooltip("The game object for the master slider")]
    public GameObject masterSlider;
    [Tooltip("The button to trigger the audio menu")]
    public GameObject audioButton;
    [Tooltip("The game panel")]
    public GameObject audioPanel;

    [Header("Keybindings Panel")]
    [Tooltip("The game object for the keybindings panel")]
    public GameObject keybindingsPanel;
    [Tooltip("The button to trigger the keybindings menu")]
    public GameObject keybindingsButton;
    //Then we will change the other stuff

    [Header("Highlight Effects")]
    [Tooltip("The effect for the game selection")]
    public GameObject gameEffect;
    [Tooltip("The effect for the audio selection")]
    public GameObject audioEffect;
    [Tooltip("The effect for the keybindings selection")]
    public GameObject keybindEffect;

    [Header("Unsaved changes")]
    [Tooltip("The text for unsaved changes")]
    public GameObject unsavedChanges;

    [Header("Keybinding Menu")]
    [Tooltip("The movement effect")]
    public GameObject movementEffect;
    [Tooltip("The interaction effect")]
    public GameObject interactionEffect;
    [Tooltip("The misc effect")]
    public GameObject miscEffect;
    [Tooltip("The movement panel")]
    public GameObject movementPanel;
    [Tooltip("The interaction panel")]
    public GameObject interactionPanel;
    [Tooltip("The misc panel")]
    public GameObject miscPanel;

    public GameObject settingsHover;

    //Slides
    public float musicSliderValue = 0.5f;
    public float sfxSliderValue = 0.5f;
    public float masterSliderValue = 0.5f;

    [Header("Panel Fade Speeds")]
    [Tooltip("The fade speeds")]
    public float fadeSpeed;
    [Tooltip("The min alpha")]
    public float minAlpha = 0.5f;
    [Tooltip("The max alpha")]
    public float maxAlpha = 1.0f;

    //Private variables
    private bool isGame = true;
    private bool isAudio = false;
    private bool isKeyBind = false;

    //Saved values
    private string savedResolution;

    [HideInInspector]
    //Public bool
    public bool settingsChanged = false;

    void Start()
    {
        windowManager = GetComponent<WindowManager>();
        soundManager = FindObjectOfType<AudioManager>();
        musicSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("musicVolume",0.5f);
        sfxSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("sfxVolume",0.5f);
        masterSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("masterVolume",0.5f);

#if UNITY_ANDROID

        //Move the audio button up to the game button
        audioButton.GetComponent<RectTransform>().position = gameButton.GetComponent<RectTransform>().position;
        audioEffect.GetComponent<RectTransform>().position = gameEffect.GetComponent<RectTransform>().position;

        //Set the things we dont need to false
        gameButton.SetActive(false);
        gameEffect.SetActive(false);

        keybindingsButton.SetActive(false);
        keybindEffect.SetActive(false);
        settingsHover.SetActive(false);

        //Set the panels to false
        gamePanel.SetActive(false);
        keybindingsPanel.SetActive(false);

        audioPanel.SetActive(true);

        audioEffect.SetActive(true);
        
#endif
    }

    //Generic update function
    void Update()
    {
        EnableUnsavedChanges(settingsChanged);
        musicSliderValue = musicSlider.GetComponent<Slider>().value;
        sfxSliderValue = sfxSlider.GetComponent<Slider>().value;
        masterSliderValue = masterSlider.GetComponent<Slider>().value;

        UpdateMasterSlider();
        UpdateSFXSlider();
        UpdateMusicSlider();

        CanvasGroup cg = new CanvasGroup();
        if (isGame)
        {
            cg = gamePanel.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, fadeSpeed * Time.deltaTime);
        }
        else if (isAudio)
        {
            cg = audioPanel.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, fadeSpeed * Time.deltaTime);
        }
        else if (isKeyBind)
        {
            cg = keybindingsPanel.GetComponent<CanvasGroup>();
            cg.alpha = Mathf.Lerp(cg.alpha, maxAlpha, fadeSpeed * Time.deltaTime);
        }
    }

    public void EnableGameSettings()
    {
        audioPanel.GetComponent<CanvasGroup>().alpha = minAlpha;
        keybindingsPanel.GetComponent<CanvasGroup>().alpha = minAlpha;
        isGame = true;
        isAudio = true;
        //Setting of game stuff.
        gameEffect.SetActive(true);
        gamePanel.SetActive(true);

        //Setting of audio stuff.
        audioPanel.SetActive(false);
        audioEffect.SetActive(false);

        //Setting of keybind stuff.
        keybindingsPanel.SetActive(false);
       // keybindEffect.SetActive(false);
    }

    public void EnableAudioSettings()
    {
        gamePanel.GetComponent<CanvasGroup>().alpha = minAlpha;
        keybindingsPanel.GetComponent<CanvasGroup>().alpha = minAlpha;
        isGame = false;
        isAudio = true;
        //Setting of game stuff.
        gameEffect.SetActive(false);
        gamePanel.SetActive(false);

        //Setting of audio stuff.
        audioPanel.SetActive(true);
        audioEffect.SetActive(true);

        //Setting of keybind stuff.
        keybindingsPanel.SetActive(false);
       // keybindEffect.SetActive(false);

    }

    public void EnableKeybindingSettings()
    {
        gamePanel.GetComponent<CanvasGroup>().alpha = minAlpha;
        audioPanel.GetComponent<CanvasGroup>().alpha = minAlpha;
        isGame = false;
        isAudio = false;
        isKeyBind = true;

        //Setting of game stuff.
  //      gameEffect.SetActive(false);
        gamePanel.SetActive(false);

        //Setting of audio stuff.
        audioPanel.SetActive(false);
        //audioEffect.SetActive(false);

        //Setting of keybind stuff.
        keybindingsPanel.SetActive(true);
       // keybindEffect.SetActive(true);

    }

    public void EnableSubMovementMenu()
    {
        //Do stuff for the movement panels
        movementPanel.SetActive(true);
       // movementEffect.SetActive(true);

        //Do stuff for the interaction panels
        interactionPanel.SetActive(false);
      //  interactionEffect.SetActive(false);

        //Do stuff for the misc panels
        miscPanel.SetActive(false);
       // miscEffect.SetActive(false);

    }
    public void EnableSubInteractionMenu()
    {
        //Do stuff for the movement panels
        movementPanel.SetActive(false);
      //  movementEffect.SetActive(false);

        //Do stuff for the interaction panels
        interactionPanel.SetActive(true);
     //   interactionEffect.SetActive(true);

        //Do stuff for the misc panels
        miscPanel.SetActive(false);
       // miscEffect.SetActive(false);
    }
    public void EnableSubMiscMenu()
    {
        //Do stuff for the movement panels
        movementPanel.SetActive(false);
       // movementEffect.SetActive(false);

        //Do stuff for the interaction panels
        interactionPanel.SetActive(false);
      //  interactionEffect.SetActive(false);

        //Do stuff for the misc panels
        miscPanel.SetActive(true);
      //  miscEffect.SetActive(true);
    }


    private void EnableUnsavedChanges(bool enabled)
    {
        unsavedChanges.SetActive(enabled);
    }

    public void SetQualitySetting(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    //Master volume settings
    public void UpdateMasterSlider()
    {
        if (masterSliderValue < sfxSliderValue)
        {
            sfxSlider.GetComponent<Slider>().value = Mathf.Clamp(sfxSlider.GetComponent<Slider>().value, 0, masterSliderValue);
            UpdateMusicSlider();
        }
        if (masterSliderValue < musicSliderValue)
        {
            musicSlider.GetComponent<Slider>().value = Mathf.Clamp(musicSlider.GetComponent<Slider>().value, 0, masterSliderValue);
            UpdateSFXSlider();
        }
        soundManager.masterVolume = masterSliderValue;
        PlayerPrefs.SetFloat("masterVolume", masterSliderValue);
        //settingsChanged = true;
    }

    //Slider settings
    public void UpdateMusicSlider()
    {
        if(musicSliderValue > masterSliderValue)
        {
            musicSlider.GetComponent<Slider>().value = Mathf.Clamp(musicSlider.GetComponent<Slider>().value, 0, masterSliderValue);
        }
        soundManager.musicVolume = musicSliderValue;
        PlayerPrefs.SetFloat("musicVolume", musicSliderValue);

        //settingsChanged = true;
    }
    //Sfx Slider settings
    public void UpdateSFXSlider()
    {
        if (sfxSliderValue > masterSliderValue)
        {
            sfxSlider.GetComponent<Slider>().value = Mathf.Clamp(sfxSlider.GetComponent<Slider>().value, 0, masterSliderValue);
        }
        soundManager.soundEffectVolume = sfxSliderValue;
        PlayerPrefs.SetFloat("sfxVolume", sfxSliderValue);

        //settingsChanged = true;
    }

    public void DefaultSettings()
    {
        //Revert the keybinds
        windowManager.RevertSettings();
        //Revert the other variables

        //Initialise Default Values
        musicSlider.GetComponent<Slider>().value = 0.5f;
        sfxSlider.GetComponent<Slider>().value = 0.5f;
        masterSlider.GetComponent<Slider>().value = 0.5f;

        settingsChanged = true;
    }

    public void RevertSettings()
    {
        //Revert based on what it is
        if (isGame)
        {
            windowManager.RevertSettings();
        }
        else if (isAudio)
        {
            //Initialise Default Values
            musicSlider.GetComponent<Slider>().value = 0.5f;
            sfxSlider.GetComponent<Slider>().value = 0.5f;
            masterSlider.GetComponent<Slider>().value = 0.5f;
        }
        else if (isKeyBind)
        {
            keybindManager.RevertKeybindings();
        }

        settingsChanged = true;
    }

    public void SaveAudioSettings()
    {
        PlayerPrefs.SetFloat("masterVolume", masterSliderValue);
        PlayerPrefs.SetFloat("musicVolume", musicSliderValue);
        PlayerPrefs.SetFloat("sfxVolume", sfxSliderValue);
    }
}
