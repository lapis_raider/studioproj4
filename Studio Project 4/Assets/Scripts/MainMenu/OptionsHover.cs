﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OptionsHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    [Header("Text to affect")]
    [Tooltip("Settings Text")]
    public Text settingsText;

    [Header("Description")]
    [TextArea(3,10)]
    public string textToShow;

    public void OnPointerEnter(PointerEventData eventData)
    {
        settingsText.text = textToShow;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        settingsText.text = "";
    }
}
