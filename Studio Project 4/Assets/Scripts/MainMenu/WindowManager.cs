﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour {

    private OptionsMenu optionsMenu;

    #region Attributes

    #region Player Pref Key Constants

    private const string RESOLUTION_PREF_KEY = "resolution";
    private const string RENDERMODE_PREF_KEY = "screenMode";

    #endregion

    #region Resolution
    [SerializeField]
    private Text resolutionText;

    private Resolution[] resolutions;

    private int currentResolutionIndex = 0;
    #endregion

    #endregion

    private bool isFullScreen;
    [SerializeField]
    private Text renderModeText;

    // Use this for initialization
    void Start () {
        optionsMenu = GetComponent<OptionsMenu>();
        resolutions = Screen.resolutions;
        currentResolutionIndex = PlayerPrefs.GetInt(RESOLUTION_PREF_KEY, resolutions.Length / 2);
        LoadScreenMode(PlayerPrefs.GetInt(RENDERMODE_PREF_KEY, 1));
        SetResolutionText(resolutions[currentResolutionIndex]);
	}

    #region ResolutionCycling

    private void SetResolutionText(Resolution resolution)
    {
        resolutionText.text = resolution.width + "x" + resolution.height;

    }

    public void SetNextResolution()
    {
        currentResolutionIndex = GetNextWrappedIndex(resolutions, currentResolutionIndex);
        SetResolutionText(resolutions[currentResolutionIndex]);
        optionsMenu.settingsChanged = true;
    }

    public void SetPreviousResolution()
    {
        currentResolutionIndex = GetPreviousWrappedIndex(resolutions, currentResolutionIndex);
        SetResolutionText(resolutions[currentResolutionIndex]);
        optionsMenu.settingsChanged = true;
    }

    #endregion

    #region ApplyResolution
    private void SetAndApplyResolution(int newResolutionIndex)
    {
        currentResolutionIndex = newResolutionIndex;
        ApplyCurrentResolution();
    }

    private void ApplyCurrentResolution()
    {
        ApplyResolution(resolutions[currentResolutionIndex]);
    }

    private void ApplyResolution(Resolution resolution)
    {
        SetResolutionText(resolution);

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);

        PlayerPrefs.SetInt(RESOLUTION_PREF_KEY, currentResolutionIndex);
        
    }
    #endregion

    #region Misc Helpers

    #region Index Wrap Helpers
    private int GetNextWrappedIndex<T>(IList<T>collection, int currentIndex)
    {
        if (collection.Count < 1) return 0;
        
        return (currentIndex +1) % collection.Count;
    }

    private int GetPreviousWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1) return 0;
        if ((currentIndex - 1) < 0) return collection.Count - 1;

        return (currentIndex - 1) % collection.Count;

    }

    #endregion
    #endregion

    public void ChangeScreenMode()
    {
        isFullScreen = !isFullScreen;
        if(isFullScreen)
        {
            renderModeText.text = "Fullscreen";
        }
        else
        {
            renderModeText.text = "Windowed";
        }
        optionsMenu.settingsChanged = true;
    }

    private void LoadScreenMode(int renderMode)
    {
        if(renderMode ==0)
        {
            renderModeText.text = "Windowed";
            isFullScreen = false;
        }
        else
        {
            renderModeText.text = "Fullscreen";
            isFullScreen = true;
        }
    }

    public void ApplyChanges()
    {
        SetAndApplyResolution(currentResolutionIndex);
        Screen.fullScreen = isFullScreen;

        optionsMenu.settingsChanged = false;
    }

    public void RevertSettings()
    {
        isFullScreen = true;
        currentResolutionIndex = PlayerPrefs.GetInt(RESOLUTION_PREF_KEY, resolutions.Length / 2);
        LoadScreenMode(PlayerPrefs.GetInt(RENDERMODE_PREF_KEY, 1));
        SetResolutionText(resolutions[currentResolutionIndex]);

        optionsMenu.settingsChanged = true;

    }
}
