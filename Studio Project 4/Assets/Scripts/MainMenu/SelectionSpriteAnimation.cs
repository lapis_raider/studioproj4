﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionSpriteAnimation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    private Animator animator;

    [SerializeField]
    private GameObject selection;

    [SerializeField] // debugging in inspector
    private bool playAnimation = false;

    private bool isEnabled = false;

    private bool stay = false;

	// Use this for initialization
	void Start () {
        animator = selection.GetComponent<Animator>();
	}

    void Update()
    {
        if(stay)
        {
            animator.SetBool("isOpen", isEnabled);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        playAnimation = true;
        animator.SetBool("isOpen", playAnimation);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        playAnimation = false;
        animator.SetBool("isOpen", playAnimation);
    }

    public void Enabled()
    {
        isEnabled = !isEnabled;
        stay = isEnabled;
        animator.SetBool("isOpen", isEnabled);
    }


}
