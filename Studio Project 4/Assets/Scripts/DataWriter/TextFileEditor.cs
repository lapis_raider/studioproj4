﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class TextFileEditor : MonoBehaviour {

    //Directly editable by the editor
   
    //For other calls, if not its by using editor
    public void CreateTextFile(string theFileName = "", string theCreateFileText="")
    {
        if(theFileName == "")
        {
            //Then we write to another errorFile
            string errorPath = Application.dataPath + "/Resources/" + "errorLog.txt";
            if(!File.Exists(errorPath))
            {
                File.WriteAllText(errorPath, "[" + System.DateTime.Now.ToString() + "]"     + " The fileName for creating file was empty.\n");
            }
            return;
        }
     
        //Specify the path of the file
        string path = Application.dataPath + "/Resources/" + theFileName + ".txt";
        //Create file if it doesnt exist
        if(!File.Exists(path))
        {
            if (theCreateFileText != "")
            {
                File.WriteAllText(path, theCreateFileText + "\n");
            }
        }
    }

    public void WriteText(string theFileName, string contentToWrite, bool isAppend)
    {
        //Specify the path of the file
        string path = Application.dataPath + "/Resources/" + theFileName + ".txt";
        //Then we write to another errorFile
        string errorPath = Application.dataPath + "/Resources/" + "errorLog.txt";
        if (!File.Exists(path))
        {
            File.AppendAllText(errorPath, "["+ System.DateTime.Now.ToString() + "] Create file of name <" + theFileName + "> first, before writing.\n");
            return;
        }

        //Else we either append or
        if (isAppend)
        {
            File.AppendAllText(path, contentToWrite);
        }
        else
        {
            File.WriteAllText(path, contentToWrite);
        }
    }
}
