﻿using UnityEngine;

public class RankCheck : MonoBehaviour {

    int numOfClues = 0;
    Sprite[] sprites;
    SpriteRenderer spriteRenderer;
    public GameObject SRank;
    public GameObject[] rank;
	// Use this for initialization
	void Awake () {
        AudioManager.instance.StopAllSounds();
	}

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        sprites = Resources.LoadAll<Sprite>("Images/WinLose");
        if (GameDataManager.Instance.playerData.playerSanity <= 0) //game over
        {
            rank[6].SetActive(true);
            AudioManager.instance.Play("LoseScreen");
        }
        else
        {
            for (int i = 0; i < GameDataManager.Instance.playerData.clues.Length; ++i)
            {
                if (GameDataManager.Instance.playerData.clues[i] == true)
                    ++numOfClues;
            }
            switch (numOfClues)
            {
                case 11:
                    {
                        SRank.SetActive(true);
                        gameObject.SetActive(false);
                        AudioManager.instance.Play("Srank");
                        AudioManager.instance.Play("WinBg");
                    }
                    break;
                case 10:
                    {
                        spriteRenderer.sprite = sprites[8]; //rank A
                        rank[0].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 9:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[1].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 8:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[1].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 7:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[2].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 6:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[2].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 5:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[3].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 4:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[3].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 3:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[4].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 2:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[5].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 1:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[5].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
                case 0:
                    {
                        spriteRenderer.sprite = sprites[8];
                        rank[5].SetActive(true);
                        AudioManager.instance.Play("Tried");
                    }
                    break;
            }
        }
    }
}
