﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class EnterScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("called TRIGGER");
        if (collision.CompareTag("Player"))
        {

            SceneManager.LoadScene("WinLoseScene");
        }
    }
}
