﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanityDiamond : MonoBehaviour {

    [SerializeField]
    private GameObject quad;

    public bool triggered = false;

    private bool increasing = false;
    private bool decreasing = false;

    //Min scale
    [SerializeField]
    private Vector3 minScale;
    //Max scale
    [SerializeField]
    private Vector3 maxScale;
    [SerializeField]
    private Vector3 defaultScale;

    RectTransform rt;

    private void Start()
    {
        rt = GetComponent<RectTransform>();
        defaultScale = rt.localScale;
      
    }

    void Update()
    {
        if(triggered)
        {
            if(!increasing)
            {
                rt.localScale = Vector3.Lerp(rt.localScale, maxScale, 5 * Time.deltaTime);
            }

            if(Vector3.Distance(rt.localScale,maxScale) <0.1f)
            {
                increasing = true;
            }

            if(increasing && !decreasing)
            {
                rt.localScale = Vector3.Lerp(rt.localScale, minScale, 5 * Time.deltaTime);

                if (Vector3.Distance(rt.localScale, minScale) < 0.1f)
                {
                    decreasing = true;
                }
            }

        }
        else
        {
            increasing = false;
            decreasing = false;
            rt.localScale = Vector3.Lerp(rt.localScale, defaultScale, 5 * Time.deltaTime);
        }
    }


}
