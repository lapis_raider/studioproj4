﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanityBarUI : MonoBehaviour {

    [Header("The sanityBar")]
    public GameObject sanityBar;

    [Header("The Sanity Bar Masks")]
    //Layer masks
    public GameObject sanityBarMasked;
    public GameObject sanityBarLayerMask;
    public GameObject sanityBarLayerMask2;

    [Header("The Sanity Diamond Masks")]
    //Diamond masks
    public GameObject sanityBarDiamondMask;
    public GameObject sanityBarDiamondMask2;
    public GameObject sanityBarDiamondMask3;

    [Header("The subsequent diamonds")]
    public GameObject diamond1;
    public GameObject diamond2;
    public GameObject diamond3;

    [Header("Lerp speed")]
    public float lerpSpeed;

    //So its easier 
    private float sanity = 0;
    private float maxSanity = 0;

    private float maxWidth = 0;

    private float triggerDiamond1, triggerDiamond2, triggerDiamond3;

    private float valueToLerpTo = 0;

    private void Start()
    {
        maxSanity = GameDataManager.Instance.playerData.playerFullSanity;
        maxWidth = sanityBar.GetComponent<RectTransform>().rect.width * 2;
        triggerDiamond1 = maxWidth * 0.33f;
        triggerDiamond2 = maxWidth * 0.66f;
        triggerDiamond3 = maxWidth * 0.99f /*1.0f*/;

        sanityBarLayerMask.GetComponent<RectTransform>().position = sanityBarMasked.GetComponent<RectTransform>().position;
        sanityBarLayerMask2.GetComponent<RectTransform>().position = sanityBarMasked.GetComponent<RectTransform>().position;
        sanityBarDiamondMask.GetComponent<RectTransform>().position = sanityBarMasked.GetComponent<RectTransform>().position;
        sanityBarDiamondMask2.GetComponent<RectTransform>().position = sanityBarMasked.GetComponent<RectTransform>().position;
        sanityBarDiamondMask3.GetComponent<RectTransform>().position = sanityBarMasked.GetComponent<RectTransform>().position;

        //Set of coloring...

        //Yellow
        sanityBarMasked.GetComponent<Image>().color = new Color(0.749f, 0.714f, 0.102f);
        sanityBarDiamondMask.GetComponent<Image>().color = new Color(0.749f, 0.714f, 0.102f);

        //Orange rgb(153, 63, 13)
        sanityBarLayerMask.GetComponent<Image>().color = new Color(0.894f, 0.38f, 0.094f);
        sanityBarDiamondMask2.GetComponent<Image>().color = new Color(0.894f, 0.38f, 0.094f);

        //Red RGB rgb(109, 31, 12)
        sanityBarLayerMask2.GetComponent<Image>().color = new Color(0.886f, 0.118f, 0.118f);
        sanityBarDiamondMask3.GetComponent<Image>().color = new Color(0.886f, 0.118f, 0.118f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameDataManager.Instance.playerData.PlayerSanityDecrease(20);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            GameDataManager.Instance.playerData.PlayerSanityDecrease(-20);
        }

        sanity = GameDataManager.Instance.playerData.playerSanity;
        valueToLerpTo = CalculateValueToLerpTo();
        //Debug.Log(valueToLerpTo);
        MoveBar();
        TriggerDiamonds();
    }

    void TriggerDiamonds()
    {
        //lmao
        {
            diamond1.GetComponent<SanityDiamond>().triggered = false;
            diamond2.GetComponent<SanityDiamond>().triggered = false;
            diamond3.GetComponent<SanityDiamond>().triggered = false;
        }

        RectTransform rt = sanityBarMasked.GetComponent<RectTransform>();

        if (rt.sizeDelta.x >= triggerDiamond1)
        {
            diamond1.GetComponent<SanityDiamond>().triggered = true;
        }
        if (rt.sizeDelta.x >= triggerDiamond2)
        {
            diamond2.GetComponent<SanityDiamond>().triggered = true;
        }
        if (rt.sizeDelta.x >= triggerDiamond3)
        {
            diamond3.GetComponent<SanityDiamond>().triggered = true;
        }
    }

    float CalculateValueToLerpTo()
    {
        float sanityRatio = 0;

        sanityRatio = ((maxSanity - sanity) / maxSanity) * maxWidth;

        return sanityRatio;

    }

    void MoveBar()
    {
        RectTransform rt = sanityBarMasked.GetComponent<RectTransform>();
        if(rt.sizeDelta.x != valueToLerpTo)
        {
            rt.sizeDelta = new Vector3( Mathf.Lerp(rt.sizeDelta.x, valueToLerpTo, lerpSpeed * Time.deltaTime), rt.sizeDelta.y, 0);
        }

        if(Mathf.Abs(valueToLerpTo - rt.sizeDelta.x) < 1f)
        {
            rt.sizeDelta = new Vector3(valueToLerpTo, rt.sizeDelta.y, 0);
        }

        sanityBarLayerMask.GetComponent<RectTransform>().sizeDelta = new Vector3(rt.sizeDelta.x, sanityBarLayerMask.GetComponent<RectTransform>().sizeDelta.y);
        sanityBarLayerMask2.GetComponent<RectTransform>().sizeDelta = new Vector3(rt.sizeDelta.x, sanityBarLayerMask2.GetComponent<RectTransform>().sizeDelta.y);
        sanityBarDiamondMask.GetComponent<RectTransform>().sizeDelta = new Vector3(rt.sizeDelta.x, sanityBarDiamondMask.GetComponent<RectTransform>().sizeDelta.y);
        sanityBarDiamondMask2.GetComponent<RectTransform>().sizeDelta = new Vector3(rt.sizeDelta.x, sanityBarDiamondMask2.GetComponent<RectTransform>().sizeDelta.y);
        sanityBarDiamondMask3.GetComponent<RectTransform>().sizeDelta = new Vector3(rt.sizeDelta.x, sanityBarDiamondMask3.GetComponent<RectTransform>().sizeDelta.y);

    }

    

}
