﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnButler : MonoBehaviour {
    public float delay = 0;
    public GameObject[] butlers;
    bool spawned = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < butlers.Length; ++i)
        {
            if (!butlers[i].activeSelf)
            {
                spawned = false;
                break;
            }
        }
        if (!spawned)
            delay -= Time.deltaTime;
        if(delay <= 0)
        {
            for (int i = 0; i < butlers.Length; ++i)
            {
                if(!butlers[i].activeSelf)
                    butlers[i].SetActive(true);
            }
            spawned = true;
            delay = 10;
        }
	}
}
