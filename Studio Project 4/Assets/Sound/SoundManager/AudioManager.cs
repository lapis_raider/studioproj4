using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

	public static AudioManager instance;

	public AudioMixerGroup mixerGroup;

	public Sound[] sounds;

    //[HideInInspector]
    public float masterVolume = 1;
    //[HideInInspector]
    public float soundEffectVolume = 1;
   // [HideInInspector]
    public float musicVolume = 1;

	void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}

		foreach (Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.loop = s.loop;

			s.source.outputAudioMixerGroup = mixerGroup;
		}
        Play("background"); 
	}

	public void Play(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return;
		}

		s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f));
		s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f));

		s.source.Play();
    }
    public void Stop(string sound)
    {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        s.source.Stop();
    }
    public void StopAllSounds()
    {
        foreach (Sound s in sounds)
        {
            s.source.Stop();
        }
    }
    public Sound GetSound(string soundName)
    {
        Sound s = Array.Find(sounds, item => item.name == soundName);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return null;
        }
        return s;
    }
    private void Update()
    {
        foreach (Sound s in sounds)
        {
            s.source.clip = s.clip;
            s.source.loop = s.loop;
            if (s.SoundEffect)
            {
                if(masterVolume < soundEffectVolume)
                    s.source.volume = Mathf.Clamp(s.volume, 0, masterVolume);
                else
                    s.source.volume = Mathf.Clamp(s.volume, 0, soundEffectVolume);
            }
            if(s.Music)
            {
                if (masterVolume < musicVolume)
                    s.source.volume = Mathf.Clamp(s.volume, 0, masterVolume);
                else
                    s.source.volume = Mathf.Clamp(s.volume, 0, musicVolume);
            }
            
            s.source.outputAudioMixerGroup = mixerGroup;
        }
    }
}
